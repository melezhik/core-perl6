#!/bin/bash

RED=
GREEN=
YELLOW=
GREY=
NC=
REDBOLD=
SKIPPED=
PROCESSOR=
CONFIGFILE=
LOGFIRSTSTAGE=0
SKIPPEDSTAGES=()
COVERAGESTATS=()
COVERAGE=0
ITERATOR=0
LINESZ=64
DATE=`date '+%Y-%m-%d_%H-%M-%S'`
SUCCESSTOKEN=`md5sum $0 | awk '{ print $1 }'`
GITVER='undef';
FILES_REPORT='[]'
WWW=./www
PRECOMP=./lib/.precomp
TESTLOG=./testreport.${DATE}.log
COVERALLSENDPOINT='https://coveralls.io/api/v1/jobs'
ORIGIN='git@gitlab.com:pheix-pool/core-perl6.git'
MULTIPARTDELIM="-----`echo ${DATE} | md5sum | cut -f1 -d' '`"
GITBRANCH=${CI_COMMIT_BRANCH}

while getopts "cg:v:s:p:f:l" opt; do
    case $opt in
        g)
            if [ -d "${OPTARG}" ]
            then
                CURRDIR=`pwd`;
                cd ${OPTARG}
                GITVER=`git log -1 | egrep -i -m 1 -o '([0-9]+\.[0-9]+\.[0-9]+)'`;
                cd ${CURRDIR}
                echo -e "Git root directory: ${GREEN}${OPTARG}${NC}, last commit version: ${GREEN}${GITVER}${NC}"
            fi
        ;;
        v)
            if [ ! -z "${OPTARG}" ]
            then
                CURRVER=${OPTARG}
                echo -e "Current commit version: ${GREEN}${CURRVER}${NC}"
            fi
        ;;
        c)
            RED='\e[0;31m'
            GREEN='\e[0;32m'
            YELLOW='\e[0;33m'
            SKIPBG='\e[7;37m'
            NC='\e[0m'
            REDBOLD='\e[1;31m'
            echo -e "Colors in output are ${GREEN}switch on${NC}!"
        ;;
        s)
            IFSBACK="$IFS"
            IFS=', ' read -r -a SKIPPEDSTAGES <<< "${OPTARG}"
            IFS=$IFSBACK
            echo -e "Got ${#SKIPPEDSTAGES[@]} stages to forced skip: ${OPTARG}"
        ;;
        p)
            if [ ! -z "${OPTARG}" ]
            then
                PROCESSOR=${OPTARG}
            fi
        ;;
        f)
            if [ ! -z "${OPTARG}" ]
            then
                if [ -f "${OPTARG}" ]; then
                    CONFIGFILE=${OPTARG}
                fi
            fi
        ;;
        l)
            LOGFIRSTSTAGE=1
        ;;
        *)
            echo "Usage: $(basename $0) [-c] [-v VERSION] [-g GITVERSION] [-s STAGES] [-p PROCESSOR]"
            exit 255
        ;;
    esac
done

if [ $? -ne 0 ]
then
    echo -e "[ ${REDBOLD}PANIC:${NC}${RED} command line args processing failure${NC} ]"
    exit 2
fi

if [ ! -z "${PROCESSOR}" ]; then
    if   [ "${PROCESSOR}" == 'yq' ]; then
        if [ -z "${CONFIGFILE}" ]; then
            TESTCONFIG=run-tests.conf.yml
        else
            TESTCONFIG=${CONFIGFILE}
        fi
    elif [ "${PROCESSOR}" == 'jq' ]; then
        if [ -z "${CONFIGFILE}" ]; then
            TESTCONFIG=run-tests.conf.json
        else
            TESTCONFIG=${CONFIGFILE}
        fi
    else
        echo -e "[ ${REDBOLD}PANIC:${NC}${RED} unknown processor ${PROCESSOR}${NC} ]"
        exit 13
    fi
else
    PROCESSOR=jq
    TESTCONFIG=run-tests.conf.json
fi

echo -e "Config processor ${GREEN}${PROCESSOR}${NC} is used"

check_output () {
    local OUTPUT="$1"
    local STAGE="$2"
    local SUCCESSFUL=`echo $OUTPUT | grep "$SUCCESSTOKEN"`

    if [ ! -z "$SUCCESSFUL" ]; then
        SKIPPED=`echo $OUTPUT | grep 'SKIP'`
        if [ $STAGE -gt 1 ] || [ ${LOGFIRSTSTAGE} -eq 1 ]; then
            echo -e "----------- STAGE no.${STAGE} -----------\n${OUTPUT}" | sed -e "s/${SUCCESSTOKEN}//g" >> $TESTLOG
        fi
        return 0
    else
        echo -e "------- ERROR AT STAGE no.${STAGE} -------\n${OUTPUT}" >> $TESTLOG
        echo -e "[ ${REDBOLD}PANIC:${NC}${RED} success token missed${NC} ]"
        return 1
    fi
}

stage_is_skipped () {
    local STAGE="$1"
    local SCRIPT="$2"
    local ISSKIPPED=0

    if [ -z "${SCRIPT}" ]; then
        return 1
    fi

    for SKIPSTAGE in "${SKIPPEDSTAGES[@]}"; do
        if [ "$SKIPSTAGE" = "$STAGE" ]; then
            ISSKIPPED=1
            SKIPPED="# SKIP: stage no.$STAGE is skipped with command line arg"
            printf "${SKIPBG}%02d.${NC} %-${LINESZ}b" $STAGE "Skipping tests for ${YELLOW}${SCRIPT}${NC}"
            echo -e "----------- STAGE no.${STAGE} -----------\n${SKIPPED}\n" >> $TESTLOG
        fi
    done
    return $ISSKIPPED
}

stage_env () {
    local TYPE="$1"
    local STAGE="$2"
    local SUBSTAGE="$3"
    local STAGEENV=()
    local SUBSTAGEENV=()
    local SETUP="environment"

    if [ "${TYPE}" == "unset" ]; then
        local SETUP="cleanup"
    fi

    if [ -z "${STAGE}" ] || [ ${STAGE} -lt 0 ]; then
        return 0
    fi

    local IFSBACK="$IFS"

    if [ "${PROCESSOR}" == "jq" ]; then
        if [ ! -z ${SUBSTAGE} ]  && [ ${SUBSTAGE} -ge 0 ]; then
            IFS=$'\r\n' GLOBIGNORE='*' command eval 'SUBSTAGEENV=($(cat '${TESTCONFIG}' | jq -r "select(.stages['${STAGE}'].substages['${SUBSTAGE}']) | select(.stages['${STAGE}'].substages['${SUBSTAGE}'].'${SETUP}'[0]) | .stages['${STAGE}'].substages['${SUBSTAGE}'].'${SETUP}'[]"))'
        else
            IFS=$'\r\n' GLOBIGNORE='*' command eval 'STAGEENV=($(cat '${TESTCONFIG}' | jq -r "select(.stages['${STAGE}'].'${SETUP}'[0]) | .stages['${STAGE}'].'${SETUP}'[]"))'
        fi
    elif [ "${PROCESSOR}" == "yq" ]; then
        if [ ! -z ${SUBSTAGE} ]  && [ ${SUBSTAGE} -ge 0 ]; then
            IFS=$'\r\n' GLOBIGNORE='*' command eval 'SUBSTAGEENV=($(cat '${TESTCONFIG}' | yq ".stages['${STAGE}'].substages['${SUBSTAGE}'].'${SETUP}'[]"))'
        else
            IFS=$'\r\n' GLOBIGNORE='*' command eval 'STAGEENV=($(cat '${TESTCONFIG}' | yq ".stages['${STAGE}'].'${SETUP}'[]"))'
        fi
    else
        echo -e "[ ${REDBOLD}PANIC:${NC}${RED} unknown processor ${PROCESSOR}${NC} ]"
        exit 13
    fi

    IFS=$IFSBACK

    if [ ! -z "${STAGEENV[0]}" ]; then
        for COMMAND in "${STAGEENV[@]}"; do
            $COMMAND
        done
    fi

    if [ ! -z "${SUBSTAGEENV[0]}" ]; then
        for COMMAND in "${SUBSTAGEENV[@]}"; do
            $COMMAND
        done
    fi

    return 0
}

get_stage_command () {
    local STAGE="$1"

    if [ -z "${STAGE}" ] || [ ${STAGE} -lt 0 ]; then
        echo -e "[ ${REDBOLD}PANIC:${NC}${RED} cannot get command for undefined stage${NC} ]"
        exit 1;
    fi

    local SUBSTAGE="$2"
    local STAGECOMM=
    local SUBSTLINE=
    local ARGS=()

    if [ "${PROCESSOR}" == "jq" ]; then
        if [ ! -z ${SUBSTAGE} ]  && [ ${SUBSTAGE} -ge 0 ]; then
            ARGS=(`cat ${TESTCONFIG} | jq -jr "select(.stages[${STAGE}].substages[${SUBSTAGE}]) | select(.stages[${STAGE}].substages[${SUBSTAGE}].args[0]) | .stages[${STAGE}].substages[${SUBSTAGE}].args[] | values, \" \"" | sed 's/ $//g'`)
        else
            ARGS=(`cat ${TESTCONFIG} | jq -jr "select(.stages[${STAGE}].args[0]) | .stages[${STAGE}].args[] | values, \" \"" | sed 's/ $//g'`)
        fi

        for ARG in "${ARGS[@]}"; do
            SUBSTLINE="${SUBSTLINE} | sub(\"%${ARG}%\";\"${!ARG}\")"
        done

        if [ ! -z ${SUBSTAGE} ]  && [ ${SUBSTAGE} -ge 0 ]; then
            STAGECOMM=`cat ${TESTCONFIG} | jq -r "select(.stages[${STAGE}].substages[${SUBSTAGE}]) | .stages[${STAGE}].substages[${SUBSTAGE}].test ${SUBSTLINE}"`
        else
            STAGECOMM=`cat ${TESTCONFIG} | jq -r ".stages[${STAGE}].test ${SUBSTLINE}"`
        fi
    elif [ "${PROCESSOR}" == "yq" ]; then
        if [ ! -z ${SUBSTAGE} ]  && [ ${SUBSTAGE} -ge 0 ]; then
            ARGS=(`cat ${TESTCONFIG} | yq ".stages[${STAGE}].substages[${SUBSTAGE}].args[] | select(. != null)" | tr '\n' ' ' | sed 's/ $//g'`)
        else
            ARGS=(`cat ${TESTCONFIG} | yq ".stages[${STAGE}].args[] | select(. != null)" | tr '\n' ' ' | sed 's/ $//g'`)
        fi

        for ARG in "${ARGS[@]}"; do
            export ${ARG}=${!ARG}
        done

        if [ ! -z ${SUBSTAGE} ]  && [ ${SUBSTAGE} -ge 0 ]; then
            STAGECOMM=`cat ${TESTCONFIG} | yq ".stages[${STAGE}].substages[${SUBSTAGE}].test | envsubst"`
        else
            STAGECOMM=`cat ${TESTCONFIG} | yq ".stages[${STAGE}].test | envsubst"`
        fi

        for ARG in "${ARGS[@]}"; do
            unset ${ARG}
        done
    else
        return 1
    fi

    echo ${STAGECOMM}
}

process_sustages () {
    local STAGE="$1"
    local SUBSTAGES="$2"
    local SUBSTAGECOMM=
    local SUBSTAGEMESS=
    local SUBSTAGESCRIPT=

    for (( SUBSTAGE=1; SUBSTAGE<=$SUBSTAGES; SUBSTAGE++ )); do
        SUBSTAGECOMM=$(get_stage_command $STAGE $((SUBSTAGES-1)))
        SUBSTAGESCRIPT=`cut -f1 -d' ' <<< ${STAGECOMM/raku[[:space:]]/}`

        stage_env "set" $STAGE $((SUBSTAGE-1))
        check_output "`${SUBSTAGECOMM} && echo ${SUCCESSTOKEN}`" $((STAGE+1))

        if [ $? -eq 0 ]
        then
            stage_env "unset" $STAGE $((SUBSTAGE-1))
        else
            stage_env "unset" $STAGE $((SUBSTAGE-1))
            return 1
        fi
    done

    return 0
}

failure_exit () {
    local STAGE="$1"

    echo -e "[ ${RED}error at stage ${STAGE}${NC} ]"
    unset PHEIXTESTENGINE
    exit 3;
}

multipart_data () {
    local DATA=`echo ${1} | jq .`
    local NL=$'\r\n'

    printf %s%s%s \
        "--${MULTIPARTDELIM}${NL}Content-Disposition: form-data; name=\"json_file\"; filename=\"coverage.json\"${NL}Content-Type: application/octet-stream${NL}${NL}" \
        "${DATA}" \
        "${NL}--${MULTIPARTDELIM}--${NL}"
}

export PHEIXTESTENGINE=1

if [ ! -f "${TESTCONFIG}" ]; then
    echo -e "${REDBOLD}PANIC:${NC} configuration file ${YELLOW}${TESTCONFIG}${NC} is not existed"
    exit 1;
fi

if [ -d "${PRECOMP}" ]; then
    rm -rf ${PRECOMP}
else
    echo -e "Skip delete of ${YELLOW}${PRECOMP}${NC} folder: not existed"
fi

TESTLEN=`cat ${TESTCONFIG} | ${PROCESSOR} '.stages | length'`

for (( STAGE=1; STAGE<=$TESTLEN; STAGE++ )); do
    COVERAGESTATS[$STAGE]=0
    STAGECOMM=$(get_stage_command $((STAGE-1)))
    STAGESCRIPT=`shopt -s extglob && cut -f1 -d' ' <<< ${STAGECOMM/+(raku|perl|perl6)[[:space:]]/}`

    if stage_is_skipped $STAGE $STAGESCRIPT -eq 0
    then
        SUBSTAGES=`cat ${TESTCONFIG} | ${PROCESSOR} ".stages[$((STAGE-1))].substages | length"`

        printf "%02d. %-${LINESZ}b" $STAGE "Running ${YELLOW}${STAGESCRIPT}${NC}"
        stage_env "set" $((STAGE-1))
        check_output "`${STAGECOMM} && echo ${SUCCESSTOKEN}`" $STAGE

        if [ $? -eq 0 ]; then
            if [ ! -z ${SUBSTAGES} ] && [ $SUBSTAGES -ge 0 ]; then
                process_sustages $((STAGE-1)) $SUBSTAGES
            fi
        else
            stage_env "unset" $((STAGE-1))
            failure_exit $STAGE
        fi
    fi

    if [ $? -eq 0 ]; then
        stage_env "unset" $((STAGE-1))

        if [ -z "$SKIPPED" ]; then
            COVERAGESTATS[$STAGE]=100
            let "ITERATOR = ($ITERATOR + 1)"
            let "COVERAGE = ($ITERATOR)*100/($TESTLEN)"
            echo -e "[ ${GREEN}$COVERAGE% covered${NC} ]"
        else
            echo -e "[ ${RED}SKIP${NC} ]"
        fi
    else
        stage_env "unset" $((STAGE-1))
        failure_exit $STAGE
    fi
done

if [ ! -z "${COVERALLSTOKEN}" ]; then
    if [ ! -z "${CI_JOB_ID}" ]; then
        for (( STAGE=1; STAGE<=$TESTLEN; STAGE++ )); do
            STAGECOMM=$(get_stage_command $((STAGE-1)))
            STAGESCRIPT=`cut -f1 -d' ' <<< ${STAGECOMM/raku[[:space:]]/}`

            TESTDIGEST=`echo "${STAGESCRIPT}${DATE}" | md5sum | cut -f1 -d' '`
            COVERAGE="[${COVERAGESTATS[STAGE]}]"

            FILES_REPORT=`jq --compact-output ".[.| length] |= . + {name: \"${STAGESCRIPT}\", source_digest: \"${TESTDIGEST}\", coverage: $COVERAGE}" <<< ${FILES_REPORT}`
        done

        GITHEAD=`git log -1 --pretty=format:'{id: "%H", author_name: "%aN", author_email: "%aE", committer_name: "%cN", committer_email: "%cE", message: "%f"}'`

        if [ -z "${GITBRANCH}" ]; then
            GITBRANCH=`git branch --show-current`
        fi

        COVERALLS_JSON=$( jq --compact-output -n \
            --arg token "$COVERALLSTOKEN" \
            --arg id "$CI_JOB_ID" \
            --arg branch "$GITBRANCH" \
            --arg url "$ORIGIN" \
            "{repo_token: \$token, service_job_id: \$id, service_name: \"pheix-test-suite\", source_files: ${FILES_REPORT}, git: { head: ${GITHEAD}, branch: \$branch, remotes: [ {name: \"origin\", url: \$url} ] } }" )

        COVERALLS_RESP=$(multipart_data "${COVERALLS_JSON}" | curl -s -k -X POST ${COVERALLSENDPOINT} -H "content-type: multipart/form-data; boundary=${MULTIPARTDELIM}" --data-binary @-)

        if [[ $COVERALLS_RESP == *"error"* ]]; then
            echo $COVERALLS_RESP | jq .
        else
            echo $COVERALLS_RESP | jq -r .url
        fi
    else
        echo -e "Skip send report to ${YELLOW}coveralls.io${NC}: CI/CD identifier is missed"
    fi
else
    echo -e "Skip send report to ${YELLOW}coveralls.io${NC}: repository token is missed"
fi

unset PHEIXTESTENGINE
