#!/bin/bash

ME=`basename "$0"`
RAKUVER=`raku --version | tr '\n' ' '`
ZEFVER=`zef --version`
WORKDIR=$PWD
EXITCODE=0
TIMEOUT=1
WAITTIMEOUT=30;
JOBTRACE=job-trace.log
SCHASHPATH=smart-contract.hash
BLDPATH=/builds/pheix
SCHASH=
ETHHTTPNODEINFO=
ETHHTTPSNODEINFO=
COLORTRIGGER=
STGSKIP=
ETHNODEINFO=
PHEIXCONFIGFILE=conf/config.json
LOCALENDPOINTURL=http://localhost:8540

# Parsing command line parameters

while getopts "c" opt
do
	case $opt in
	c)
        COLORTRIGGER="-c"
	;;
	*)
		# echo "no reasonable options found"
	;;
	esac
done

if [ $? -ne 0 ]
then
	echo "***ERR[$ME]: command line args processing failure"
	exit 3
fi

# Checking CI job id

if [ -z $CI_JOB_ID ] || [ $CI_JOB_ID -eq 0 ]; then
    echo "***FAILURE[$ME]: no CI job identificator is provided, aborting..."
    exit 1;
fi

# Checking ethereum nodes

if [ ! -z "$GOERLILOCALNODE" ] && [ $GOERLILOCALNODE -eq 1 ]; then
    LOCALENDPOINTURL=`jq -r '.module.configuration.settings.storage.group.goerli_local_storage | .prtl + .host + ":" + .port + .qstr' ${WORKDIR}/${PHEIXCONFIGFILE}`
    STATUS=`curl -s -k -o /dev/null -w "%{http_code}\n" $LOCALENDPOINTURL`

    while [ -z "$ETHNODEINFO" ] && [ $WAITTIMEOUT -gt 0 ] && [ $STATUS -eq 200 ]; do
        ETHNODEINFO=`curl -s -k --data '{"method":"web3_clientVersion","params":[],"id":1,"jsonrpc":"2.0"}' -H "Content-Type: application/json" -X POST ${LOCALENDPOINTURL} | jq '.result'`
        if [ ! -z "$ETHNODEINFO" ]; then
            echo "***INF[$ME]: ethereum node $ETHNODEINFO"
        fi
        WAITTIMEOUT=$[ $WAITTIMEOUT - 1 ]
        sleep $TIMEOUT
    done
else
    while [ -z "$ETHNODEINFO" ] && [ $WAITTIMEOUT -gt 0 ]; do
        ETHNODEINFO=`curl -s -k --data '{"method":"web3_clientVersion","params":[],"id":1,"jsonrpc":"2.0"}' -H "Content-Type: application/json" -X POST ${LOCALENDPOINTURL} | jq '.result'`
        if [ ! -z "$ETHNODEINFO" ]; then
            echo "***INF[$ME]: ethereum node $ETHNODEINFO"

            if [ ! -z "$PUBLICTESTNET" ] && [ $PUBLICTESTNET == "goerli" -o $PUBLICTESTNET == "ropsten" -o $PUBLICTESTNET == "rinkeby" ]; then
                ENDPOINTURL=`jq -r ".module.configuration.settings.storage.group.${PUBLICTESTNET}_storage | .prtl + .host + \":\" + .port + .qstr" ${WORKDIR}/${PHEIXCONFIGFILE}`
                ENDPOINTRESPONSE=`curl -s -k --data '{"method":"web3_clientVersion","params":[],"id":1,"jsonrpc":"2.0"}' -H "Content-Type: application/json" -X POST ${ENDPOINTURL} | jq '.result'`

                if [ ! -z "$ENDPOINTRESPONSE" ]; then
                    echo "***INF[$ME]: remote endpoint $ENDPOINTRESPONSE"
                else
                    echo "***FAILURE[$ME]: remote endpoint at ${ENDPOINTURL} is down, aborting..."
                    exit 2;
                fi
            fi
        fi
        WAITTIMEOUT=$[ $WAITTIMEOUT - 1 ]
        sleep $TIMEOUT
    done
fi

if [ -z "$ETHNODEINFO" ]; then
    echo "***WARN[$ME]: ethereum node at ${LOCALENDPOINTURL} is down"

    if [ ! -z "$PUBLICTESTNET" ] && [ $PUBLICTESTNET == "goerli" -o $PUBLICTESTNET == "ropsten" -o $PUBLICTESTNET == "rinkeby" ]; then
        echo "***FAILURE[$ME]: local signer failure, aborting..."
        exit 3;
    fi
fi

# Checking smart contract hash

if [ -f "$SCHASHPATH" ]; then
    SCHASH=`cat $SCHASHPATH`
    if [ -z "$SCHASH" ]; then
        echo "***FAILURE[$ME]: smart contract hash is blank"
    else
        export SCHASH=$SCHASH
        echo "***INF[$ME]: smart contract hash <$SCHASH>"
    fi
else
    echo "***INF[$ME]: no smart contract hash found"
fi

# Printing Raku details

echo "***INF[$ME]: ${RAKUVER}"
echo "***INF[$ME]: zef ${ZEFVER}"

# Updating zef utility

if [ ! -z "$ZEFUPDATE" ] && [ $ZEFUPDATE -eq 1 ]; then
    zef update
    zef --debug --depsonly install git://github.com/ugexe/zef.git
    zef upgrade zef
    zef upgrade --force-test Router::Right
    zef upgrade LZW::Revolunet
    zef upgrade Net::Ethereum
else
    echo "***INF[$ME]: skip zef, Router::Right, LZW::Revolunet and Net::Ethereum update"
fi

# Updating HTTP::UserAgent module

if [ ! -z "$PHEIXUPDATEUA" ] && [ $PHEIXUPDATEUA -eq 1 ]; then
    zef uninstall HTTP::UserAgent
    mkdir -p $BLDPATH && cd "$_"
    git clone https://gitlab.com/pheix/http-useragent.git && cd ./http-useragent
    echo "HTTP::UserAgent latest commit: $(git rev-parse --short HEAD)"
    zef install .
    export NETWORK_TESTING=1
    prove -ve 'raku -Ilib'
    unset NETWORK_TESTING
else
    echo "***INF[$ME]: skip HTTP::UserAgent update"
fi

# Setting stages to skip

if [ ! -z "$STAGESTOSKIP" ]; then
    STGSKIP="-s ${STAGESTOSKIP//[[:blank:]]/}"
fi

# Running core tests

if [ ! -z "$PHEIXWITHTROVE" ] && [ $PHEIXWITHTROVE -eq 1 ]; then
	if [ ! -z "$STAGESTOSKIP" ]; then
	    STGSKIP="--s=${STAGESTOSKIP//[[:blank:]]/}"
	fi

	cd $WORKDIR && zef --verbose install . && trove-cli ${COLORTRIGGER} ${STGSKIP} --p=yq --f=`pwd`/run-tests.conf.yml --o=git@gitlab.com:pheix-pool/core-perl6.git
else
	cd $WORKDIR && zef --verbose install . && bash run-tests.bash ${COLORTRIGGER} ${STGSKIP}
fi

if [ $? -ne 0 ]; then
    EXITCODE=4
fi

# Uploading artefacts

if [ ! -z $CI_JOB_ID -a $CI_JOB_ID -gt 0 ]; then
    bash collect-badges.bash
    sleep $WAITTIMEOUT
    wget -nv -O _${JOBTRACE} https://gitlab.com/pheix-pool/core-perl6/-/jobs/$CI_JOB_ID/raw
    cat _${JOBTRACE} | perl -pe 's/\e\[?.*?[\@-~]//g' | tr -s '\r' '\n' | sed -n "/gitlab-runner/,/100% covered/p" > $JOBTRACE
    rm -f _${JOBTRACE}
fi

# Cleanup

if [ -f "$SCHASHPATH" ]; then
    rm -f ${SCHASHPATH}
    if [ -z "$SCHASH" ]; then
        unset SCHASH
    fi
fi

# Exit

echo "***INF[$ME]: job finished with \$EXITCODE=$EXITCODE"
exit $EXITCODE
