#!/usr/bin/env raku

# run for single file: raku ./conf/_pages/convert-to-filechain.raku --path=./conf/_pages/index.txt --mode=rand

use v6;

use MIME::Base64;
use LZW::Revolunet;

enum Mode <full rand skip>;

sub MAIN (Str:D :$path!, Mode:D :$mode = (full), Str :$basepath, Str :$outpath)
{
    my $frame   = 1024;
    my $ratio   = 0;
    my $lzwobj  = LZW::Revolunet.new(:dictsize(97000));
    my $mimeobj = MIME::Base64.new;
    my $opath   = $outpath && $outpath.IO.e && $outpath.IO.d ?? $outpath !! './conf/system';
    my @files;

    die 'invalid file is given' unless $path ne q{} && $path.IO.e && ($path.IO.f || $path.IO.d);

    if ($path.IO.d) {
        @files = dir($path, test => { "$path/$_".IO.f }).map({~($_)});
    }
    else {
        @files.push($path);
    }

    for @files -> $file {
        my $bp = q{};
        my $filechain = "# id;data;compression\n";

        my $filecontents = $file.IO.slurp;
        $filecontents ~~ s:g/^^ <[\s]>+ // unless $mode eq 'full';

        my $strbuf = $filecontents.split(q{});

        my $flen         = $strbuf.elems;
        my $blocks       = ($flen / $frame).ceiling;
        my @skip_indexes = get_skip_indexes(:mode($mode), :blocks($blocks));

        note 'File ', $file, ' elems = ', $strbuf.elems, ' (', $blocks, ' blocks)';
        note 'Skipping encoding on indexes: ', join(q{,}, @skip_indexes) if @skip_indexes;

        for ^$blocks -> $index {
            my $comp    = 0;
            my $lim_bot = $index * $frame;
            my $lim_top = ($index + 1) * $frame - 1;

            $lim_top = $flen - 1 if $lim_top >= $flen;

            my $work_buf = $strbuf[$lim_bot...$lim_top];
            my $encoded  = q{};

            if !@skip_indexes || @skip_indexes.map({$_ if $index == $_}).elems == 0 {
                my $wfs = $mode eq 'rand' ??
                    prepare_plain(:data($work_buf.join(q{}))) !!
                        $work_buf.join(q{});

                $encoded = $mimeobj.encode-str(
                    $lzwobj.compress(
                        :s($lzwobj.encode_utf8(:s($wfs))),
                        :ratio($ratio)
                    ),
                    :oneline
                );

                my $decoded = $lzwobj.decode_utf8(
                    :s($lzwobj.decompress(:s($mimeobj.decode-str($encoded))))
                );

                if $decoded ne $wfs {
                    die "not equal (at index $index):" ~ $decoded;
                }

                note sprintf("%03d: %d...%d [encoded]", $index + 1, $lim_bot, $lim_top);

                $comp = 1;
            }
            else {
                $encoded = prepare_plain(:data($work_buf.join(q{})));

                note sprintf("%03d: %d...%d [plain]", $index + 1, $lim_bot, $lim_top);
            }

            $filechain ~= ($index, $encoded, $comp).join(q{|}) ~ "\n";
        }

        if ($basepath && $basepath ne q{}) {
            my $clean_basepath = $basepath.IO.cleanup.Str;
            $bp = $file.IO.cleanup.dirname;

            $bp ~~ s/$clean_basepath//;
        }

        my $bname = $opath ~ $bp ~ q{/} ~ $file.IO.extension('tnk').basename;

        if spurt $bname, $filechain {
            note "data is saved ", $bname;
        }
        else {
            die "save data failure";
        }
    }
}

sub get_skip_indexes(Mode:D :$mode, UInt:D :$blocks) returns Array {
    return [] if $mode eq 'full';

    srand(((now % 1) * 10000000).Int);

    my UInt $rb = $mode eq 'skip' ?? $blocks !! ($blocks + 1).rand.Int || 1;
    my @indexes;

    note "Indexes num: ", $rb;

    while (@indexes.elems != $rb) {
        my $index = $blocks.rand.Int;
        if @indexes.map({$_ if $_ == $index}).elems == 0 {
            @indexes.push($index);
        }
    }

    note "Indexes values: ", @indexes.sort.Array.gist;

    @indexes.sort.Array;
}

sub prepare_plain(Str:D :$data!) {
    my %subst =
        nl => q{},
        vl => '&VerticalLine;'
    ;

    my $prepared_data = $data;

    if $data ne q{} {
        $prepared_data ~~ s:g/^^ <[\s]>+ $$//;
        $prepared_data ~~ s:g/<[\r\n]>+//;
        $prepared_data ~~ s:g/<[|]>+/%subst<vl>/;
    }

    $prepared_data;
}
