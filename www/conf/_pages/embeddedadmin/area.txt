<pheix-content>
    <h1 class="h3 mb-3 font-weight-normal">Authenticated session <span id="sesstime" class="_phx-opacity-5">for { (state.sesstime - state.timeout) } seconds</span></h1>
    <div class="row px-3">
        <div class="col-12 pheix-row-nav-tabs">
            <div class="row">
                <div class="col-10 px-0">
                    <ul id="adminNav" class="nav nav-tabs">
                       <li class="active"><a data-toggle="tab" id="selecthome" href="#home">Home</a></li>
                       <li><a data-toggle="tab" id="selecttraffic" href="#traffic">Traffic</a></li>
                       <li><a data-toggle="tab" id="selectlogs" href="#logs">Raw</a></li>
                       <li><a data-toggle="tab" id="selectextensions" href="#extensions">Extensions</a></li>
                       <li><a href="javascript:window.PheixAuth.refresh()"><i class="fas fa-sync"></i></a></li>
                    </ul>
                </div>
                <div class="col-2 text-right px-0">
                    <ul id="adminExitNav" class="nav nav-tabs justify-content-end">
                        <li><a href="javascript:window.PheixAuth.exit()"><i class="fas fa-sign-out-alt"></i></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="mt-3 tab-content">
       <div id="home" class="tab-pane fade in active">
          <h2 class="h4 mb-3 font-weight-normal">Session details</h1>
          <p class="mb-0">The session will expire in <span class="_phx-tmrg1 pheix-blog-tag-14 p-2 d-inline-flex pheix-textline-overflow">{ (state.sesstime - state.timeout) } seconds</span></p>
          <p class="mb-0">The session is authenticated on Ethereum node for account <span class="_phx-tmrg1 pheix-blog-tag-08 p-2 d-inline-flex pheix-textline-overflow">{ state.addr }</span></p>
          <p class="mb-0">Secure private key is <span class="_phx-tmrg1 pheix-blog-tag-13 p-2 d-inline-flex pheix-textline-overflow">{ state.pkey }</span></p>
          <p id="ASC" class="mb-0">ASC token: <span class="_phx-tmrg1 pheix-blog-tag-12 p-2 d-inline-flex pheix-textline-overflow">{ state.tx }</span></p>
          <p class="mb-0">Cookie: <span class="_phx-tmrg1 pheix-blog-tag-03 p-2 d-inline-flex pheix-textline-overflow">{ state.pheixauth }</span></p>
          <p class="mb-0">To be extended alert: <span class="_phx-tmrg1 pheix-blog-tag-05 p-2 d-inline-flex pheix-textline-overflow">{ state.tryextend }</span></p>
          <hr class="_pheix-divider">
       </div>
       <div id="traffic" class="tab-pane fade">
          <h2 class="h4 mb-3 font-weight-normal">Traffic</h1>
          <canvas class="pheix-chart-canvas" id="pheixChart" width="400" height="400"></canvas>
       </div>
       <div id="logs" class="tab-pane fade">
          <h2 class="h4 mb-3 font-weight-normal">Raw decoded logs data</h1>
          <ul>
              <li class="mt-3" each={ record in this.props.pheixlogs }>
                  <span class="_phx-bold _phx-opacity-5 _phx-inline-blck mr-2">{ record.date }</span>
                  <div class="pheix-chart-canvas p-2"><code>{ b64DecodeUnicode(record.dump) }</code></div>
              </li>
          </ul>
       </div>
       <div id="extensions" class="tab-pane fade">
          <div id="extensions-content"></div>
       </div>
    </div>
    <div class="modal fade" id="admin_message_modal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
               <div class="modal-body" id="admin_message_modal_body"></div>
            </div>
        </div>
    </div>

    <script>
    export default {
      b64DecodeUnicode(str) {
          return decodeURIComponent(atob(str).split('').map(function(c) {
              return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2);
          }).join(''));
      },
      onBeforeMount(props, state) {
        this.state = {
          addr: props.addr,
          pkey: props.pkey,
          tx: props.tx,
          pheixauth: props.pheixauth ? props.pheixauth : "😵",
          sesstime: props.session,
          timeout: 0,
          tryextend: props.tryextend ? "True" : "False",
          activetab: 'home',
        }
      },
      onMounted() {
        var targettab = 'home';

        if (this.props.jspayload) {
          if (this.props.jspayload.targettab) {
              targettab = this.props.jspayload.targettab;
          }
        }

        if (this.props.extensions) {
          if (this.props.extensions.length > 0) {
            this.props.extensions.forEach(function (extension, index) {
              var extdata = Object.values(extension)[0];

              jQuery('#extensions-content').append('<div id="container-'+extdata.id+'"><'+extdata.id+'></'+extdata.id+'></div>');

              riot.inject(riot.compileFromString(extdata.component).code, extdata.id, './'+extdata.id+'.riot');
              riot.mount(extdata.id, extdata.tparams);

              console.log('extension ' + extdata.name + ' is mounted, id=' + extdata.id);
            });
          }
          else {
            jQuery('#extensions,#selectextensions').addClass('d-none');
          }
        }
        else {
           jQuery('#extensions,#selectextensions').addClass('d-none');
        }

        jQuery('#adminNav a[href="#' + targettab + '"]').tab('show');

        const footer = (tooltipItems) => {
            var countries = this.props.stats.countries;

            if (countries && countries.length) {
                return window.PheixAuth.getEmojiFlags(countries[tooltipItems[0].dataIndex]);
            }
        };

        const ctx = document.getElementById('pheixChart').getContext('2d');
        const myChart = new Chart(ctx, {
            type: 'line',
            data: {
                labels: this.props.stats.dates,
                datasets: [
                  {
                    label: 'Hosts',
                    data: this.props.stats.hosts,
                    fill: false,
                    borderColor: 'rgb(75, 192, 192)',
                    backgroundColor: 'rgba(75, 192, 192, 0.3)',
                    tension: 0.1
                  },
                  {
                    label: 'Visitors',
                    data: this.props.stats.visits,
                    fill: false,
                    borderColor: 'rgb(153, 51, 255)',
                    backgroundColor: 'rgba(153, 51, 255, 0.3)',
                    tension: 0.1
                  },
                ]
            },
            options: {
                scales: {
                    y: {
                        beginAtZero: true
                    }
                },
                plugins: {
                    tooltip: {
                        callbacks: {
                            footer: footer
                        }
                    }
                }
            }
        });

        if (window.PheixSession) {
            if (window.PheixSession.interval) {
                clearInterval(window.PheixSession.interval);

                console.log('previous interval for countdown is deleted');
            }
        }

        var interval_id = setInterval(() => {
            if (jQuery('#sesstime').length) {
              var updated_timeout = this.state.timeout + 1;

              if (this.state.timeout >= this.state.sesstime && !window.PheixAPI.isLoading) {
                  updated_timeout = this.state.timeout;
                  window.location.replace('/admin');
              }

              if (updated_timeout % 10 == 0) {
                  window.PheixAuth.validate_in_background(this);
              }
              else {
                  if (this.state.tryextend === "True") {
                      window.PheixAuth.extend_in_background(this);

                      this.update({tryextend: "Pending"});
                  }
              }

              if (updated_timeout <= this.state.sesstime) {
                  this.update({timeout: updated_timeout});
              }
            }
        }, 1000);

        window.PheixSession = {'interval': interval_id};
      }
    }
    </script>
</pheix-content>
