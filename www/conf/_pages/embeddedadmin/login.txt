<pheix-content>
    <div>
       <form class="_phx-cntr _pheix-admin-form-signin" onsubmit="{ doLogin }">
          <i class="_pheix-admin-form-signin-login-ico fas fa-sign-in-alt"></i>
          <h1 class="h3 mb-3 font-weight-normal">{ props.header }</h1>
          <label for="inputEmail" class="sr-only">Email address</label>
          <input id="ethereumAddress" type="ethaddr" pattern="^0x[a-fA-F0-9]\{40}" class="_pheix-admin-form-signin-control _pheix-admin-ethaddr" placeholder="Ethereum address" required autofocus>
          <label for="inputPassword" class="sr-only">Password</label>
          <input id="userPassword" type="password" class="_pheix-admin-form-signin-control _pheix-admin-password" placeholder="Password" required>
          <button class="btn btn-lg btn-primary btn-block" type="submit">Log in</button>
       </form>
    </div>

    <script>
        export default {
            doLogin(e) {
                window.PheixAuth.login(this.props.tmpl_sesstoken);
            }
        }
    </script>
</pheix-content>
