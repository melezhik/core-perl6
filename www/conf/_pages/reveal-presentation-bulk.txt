<html>
    <title>Querying the Ethereum blockchain nodes with Raku</title>
    <meta charset=utf-8>
    <head>
        <link rel="icon" href="data:;base64,iVBORw0KGgo=">
        <link href=https://fonts.googleapis.com/css?family=Source+Code+Pro&display=swap rel=stylesheet>
        <link href=https://cdnjs.cloudflare.com/ajax/libs/reveal.js/3.6.0/css/reveal.min.css rel=stylesheet>
        <link href=https://cdnjs.cloudflare.com/ajax/libs/reveal.js/3.6.0/css/theme/white.css rel=stylesheet>
    </head>
    <body class=pheix-presentation-bg>
        <div class=container>
            <div class=reveal>
                <div class=slides>
                    <section id=slide0></section>
                    <section id=slide1></section>
                    <section id=slide2></section>
                </div>
            </div>
        </div>
        <script src=https://code.jquery.com/jquery-3.4.1.min.js></script>
        <script src=https://cdnjs.cloudflare.com/ajax/libs/reveal.js/3.6.0/js/reveal.min.js></script>
        <script>
        function addListenerMulti(el, s, fn) {
            s.split(' ').forEach(e => el.addEventListener(e, fn, false));
        };
        Reveal.initialize();
        addListenerMulti(Reveal, 'slidechanged ready', function (event) {
            if ($('#slide' + event.indexh).is(':empty')) {
                console.log('Querying blockchain for slide ' + event.indexh);
                $('#slide' + event.indexh).html("slide " + event.indexh);
            }
        });
        </script>
    </body>
</html>
