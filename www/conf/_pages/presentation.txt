<html>
    <title>Pheix: Raku-based CMS with data storing on blockchain</title>
    <meta charset="utf-8">
    <head>
        <link rel="icon" href="data:;base64,iVBORw0KGgo=">
        <link href="https://fonts.googleapis.com/css?family=Source+Code+Pro&display=swap" rel="stylesheet">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/reveal.js/3.6.0/css/reveal.min.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/reveal.js/3.6.0/css/theme/white.css">
        <link href="resources/skins/akin/css/pheix-presentation.css?ts={{tmpl_timestamp}}" rel="stylesheet">
        <!--<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/reveal.js/3.6.0/css/theme/night.min.css">-->
    </head>
    <body class="pheix-presentation-bg">
        <div class="pheix-presentation-container">
        <div class="reveal">
            <div class="slides">
                <section class="pheix-presentation-slide1">
                    <div class="pheix-presentation-slide1-header"><strong>Pheix:</strong> Raku-based CMS<br>with data storing on blockchain</div>
                    <div class="pheix-presentation-slide1-descrs-container">
                        <div class="pheix-presentation-slide1-descrs">
                            <div class="pheix-presentation-slide1-descr-1">What is CMS?</div>
                            <div class="pheix-presentation-slide1-descr-2">Are there<br><strong>Raku-based</strong><br>systems?</div>
                            <div class="pheix-presentation-slide1-descr-3">Storing<br>content on<br>Blockchain</div>
                            <div class="pheix-presentation-slide1-descr-4">POA<br><strong>Ethereum</strong><br>Networks</div>
                            <div class="pheix-presentation-slide1-descr-5"><strong>Pheix —</strong><br>Raku CMS</div>
                            <div class="pheix-presentation-slide1-descr-6">Ethereum<br>and <strong>Raku</strong></div>
                            <div class="pheix-presentation-slide1-descr-7">Boost Raku<br>dApp</div>
                            <div class="pheix-presentation-slide1-descr-8">Hybrid<br>data model</div>
                        </div>
                    </div>
                    <div class="pheix-presentation-slide1-author-container">
                        <div class="pheix-presentation-slide1-author"><a href="https://narkhov.pro/" target="_blank">@knarkhov</a></div>
                    </div>
                </section>
                <section class="pheix-presentation-slide">
                    <div class="pheix-presentation-slide-header pheix-presentation-slide-bg-1">What is CMS?</div>
                    <div class="pheix-presentation-slide-body-container">
                        <div class="pheix-presentation-slide-body">
                            <p>Web content management system (CMS) allows non-technical users to make changes to an existing website with little or no training. CMS is a Web-site maintenance tool for non-technical administrators.</p>
                            <p><strong>A few specific CMS features:</strong></p>
                            <ol class="pheix-presentation-bigger-font">
                                <li>Presentation and administration layers;</li>
                                <li>Web page generation via templates;</li>
                                <li>Scalability and expandability via modules;</li>
                                <li>WYSIWYG editing tools;</li>
                                <li>Workflow management (access levels, roles).</li>
                            </ol>
                        </div>
                    </div>
                </section>
                <section class="pheix-presentation-slide">
                    <div class="pheix-presentation-slide-header pheix-presentation-slide-bg-1">Raku/Perl6 content management systems and frameworks</div>
                    <div class="pheix-presentation-slide-body-container">
                        <div class="pheix-presentation-slide-body">
                            <p>A few well-known Raku/Perl6-based content management systems and frameworks:</p>
                            <ol class="pheix-presentation-bigger-font">
                                <li><strong><a href="https://github.com/scmorrison/uzu" target="_blank">Uzu</a></strong> — static site generator with built-in web server, file modification watcher, live reload, themes, multi-page support;</li>
                                <li><strong><a href="https://github.com/davepagurek/Cantilever" target="_blank">Cantilever</a></strong> — CMS for Raku with built-in web server and flat-file database;</li>
                                <li><strong><a href="https://github.com/viklund/november" target="_blank">November</a></strong> — A wiki engine written in Raku, the oldest from known P6 content management systems;</li>
                                <li><strong><a href="https://github.com/croservices/cro-webapp" target="_blank">Cro</a></strong> — a set of libraries for building reactive Raku-driven distributed systems, Cro is the most promising framework for web applications now;</li>
                                <li><strong><a href="https://github.com/Bailador/Bailador" target="_blank">Bailador</a></strong> — light-weight route-based web application framework for Raku with integrated web-server.</li>
                            </ol>
                        </div>
                    </div>
                </section>
                <section class="pheix-presentation-slide4">
                    <div class="pheix-presentation-slide4-header"><strong>Content on blockchain</strong></div>
                    <div class="pheix-presentation-slide1-descrs-container">
                        <div class="pheix-presentation-slide1-descrs">
                            <div class="pheix-presentation-slide4-descr pheix-presentation-slide4-descr-1"><span>Benefits</span></div>
                            <div class="pheix-presentation-slide4-descr pheix-presentation-slide4-descr-2"><span>Features</span></div>
                            <div class="pheix-presentation-slide4-descr pheix-presentation-slide4-descr-3"><span>CMS as dApp</span></div>
                        </div>
                    </div>
                </section>
                <section class="pheix-presentation-slide">
                    <div class="pheix-presentation-slide-header pheix-presentation-slide-bg-1">Content on blockchain: benefits</div>
                    <div class="pheix-presentation-slide-body-container">
                        <div class="pheix-presentation-slide-body">
                            <p><strong>General benefits of blockchain data storage:</strong></p>
                            <ul class="pheix-presentation-bigger-font">
                                <li>Improved data privacy and security: data in a blockchain network is highly secure and tamper-evident;</li>
                                <li>Increases data reliability and speed: data in the blockchain is broken down and stored in the nodes within the network;</li>
                                <li>Cost reduction in cloud computing;</li>
                                <li>High availability and accuracy of data;</li>
                                <li>Storage is decentralized.</li>
                            </ul>
                        </div>
                    </div>
                </section>
                <section class="pheix-presentation-slide">
                    <div class="pheix-presentation-slide-header pheix-presentation-slide-bg-1">Content on blockchain: features and restrictions</div>
                    <div class="pheix-presentation-slide-body-container">
                        <div class="pheix-presentation-slide-body">
                            <p><strong>Features:</strong></p>
                            <ul class="pheix-presentation-bigger-font">
                                <li>Tracking all changes for any dataset stored in the blockchain: e.g. if you have record with user Foo details, you get full changes history out-of-box;</li>
                                <li>Account management: decentralized application is distributed over nodes (private network) and every node has built-in account: you could use this to restrict access in your app (create binding node→functionality).</li>
                            </ul>
                            <p><strong>Restrictions:</strong></p>
                            <ul class="pheix-presentation-bigger-font">
                                <li>You should use the blockchain with embedded smart contracts only (Ethereum, Hyperledger, TON);</li>
                                <li>It is preferred to store text data.</li>
                            </ul>
                        </div>
                    </div>
                </section>
                <section class="pheix-presentation-slide">
                    <div class="pheix-presentation-slide-header pheix-presentation-slide-bg-1">CMS as decentralized application (dApp)</div>
                    <div class="pheix-presentation-slide-body-container">
                        <div class="pheix-presentation-slide-body">
                            <p><strong>Why CMS is well decentralized application:</strong></p>
                            <ul class="pheix-presentation-bigger-font">
                                <li>Presentation and administration layers should be deployed on independent nodes: presentation on public server, administration on private workstation;</li>
                                <li>Administration layer (content workflow) is presented by access-restricted nodes: user from nodeA has create permissions, user from nodeB has edit permissions, user from nodeC has publish permissions;</li>
                                <li>Users from administrative nodes have roles, ensured by private transactions; e.g. different users from nodeA with create permissions could have additional roles: creating articles, news, announces.</li>
                            </ul>
                        </div>
                    </div>
                </section>
                <section class="pheix-presentation-slide8">
                    <div class="pheix-presentation-slide1-descrs-container">
                        <div class="pheix-presentation-slide1-descrs">
                            <div class="pheix-presentation-slide8-descr pheix-presentation-slide8-descr-1">Private<br><strong>Proof-of-Authority</strong> network</div>
                            <div class="pheix-presentation-slide8-descr pheix-presentation-slide8-descr-2"><strong>Create-Read-Update-Delete</strong><br>smart contract</div>
                        </div>
                    </div>
                </section>
                <section class="pheix-presentation-slide">
                    <div class="pheix-presentation-slide-header pheix-presentation-slide-bg-1">Private Proof-of-Authority network</div>
                    <div class="pheix-presentation-slide-body-container">
                        <div class="pheix-presentation-slide-body">
                            <p>Proof-of-Authority (PoA) does not depend on nodes solving arbitrarily difficult mathematical problems (like Proof-of-Work, PoW), but instead uses a set of «authorities» - nodes that are explicitly allowed to create new blocks and secure the blockchain. The chain has to be signed off by the majority of authorities, in which case it becomes a part of the permanent record. This makes it easier to maintain a private chain and keep the block issuers accountable.</p>
                            <p><strong>Parity</strong> supports a Proof-of-Authority consensus engine to be used with Ethereum Virtual Machine (EVM) based chains.</p>
                        </div>
                    </div>
                </section>
                <section class="pheix-presentation-slide">
                    <div class="pheix-presentation-slide-header pheix-presentation-slide-bg-1">CRUD smart contract</div>
                    <div class="pheix-presentation-slide-body-container">
                        <div class="pheix-presentation-slide-body">
                            <p>A contract in the sense of <strong>Solidity</strong> is a collection of code (its functions) and data (its state) that resides at a specific address on the Ethereum blockchain.</p>
                            <p>To implement four basic functions of persistent storage (<strong>create, read, update and delete</strong>) we should implement the next methods in our contract:</p>
                            <ul class="pheix-presentation-bigger-font">
                                <li><tt>new_table(string tabname)</tt>;</li>
                                <li><tt>drop_table(string tabname)</tt>;</li>
                                <li><tt>set(string memory tabname, uint rowid, string memory rowdata);</tt></li>
                                <li><tt>insert(string memory tabname, string memory rowdata, uint id);</tt></li>
                                <li><tt>select(string memory tabname, uint rowid);</tt></li>
                                <li><tt>remove(string memory tabname, uint rowid).</tt></li>
                            </ul>
                        </div>
                    </div>
                </section>
                <section class="pheix-presentation-slide11">
                    <div class="pheix-presentation-slide1-descrs-container">
                        <div class="pheix-presentation-slide1-descrs">
                            <div class="pheix-presentation-slide11-descr pheix-presentation-slide11-descr-1">Architecture</div>
                            <div class="pheix-presentation-slide11-descr pheix-presentation-slide11-descr-2">Features</div>
                            <div class="pheix-presentation-slide11-descr pheix-presentation-slide11-descr-3">Router::Right</div>
                            <div class="pheix-presentation-slide11-descr pheix-presentation-slide11-descr-4">Dependencies</div>
                        </div>
                    </div>
                    <div class="pheix-presentation-slide11-text"><strong>Pheix</strong> — Artistic 2.0 license compliant, Raku driven content management system. It is extremely lightweight, fast & scalable.</div>
                </section>
                <section class="pheix-presentation-slide12">
                    <div class="pheix-presentation-slide12-text">Pheix: architecture</div>
                </section>
                <section class="pheix-presentation-slide">
                    <div class="pheix-presentation-slide-header pheix-presentation-slide-bg-1">Pheix: features</div>
                    <div class="pheix-presentation-slide-body-container">
                        <div class="pheix-presentation-slide-body">
                            <p><strong>Pheix strengths:</strong></p>
                            <ul class="pheix-presentation-bigger-font">
                                <li>Object-oriented architecture;</li>
                                <li>Light weight and scalable;</li>
                                <li>Modules with basic functionality for complex projects;</li>
                                <li>Search engine optimization features;</li>
                                <li>Extension components development with open API;</li>
                                <li>Fast cycle from install to start up;</li>
                                <li>Multi-language support;</li>
                                <li>Out-of-the-box integration with Ethereum blockchain and Redis.</li>
                            </ul>
                            <p><strong>Why Pheix? Why not!</strong></p>
                        </div>
                    </div>
                </section>
                <section class="pheix-presentation-slide">
                    <div class="pheix-presentation-slide-header pheix-presentation-slide-bg-1">Pheix: URL routing via Router::Right</div>
                    <div class="pheix-presentation-slide-body-container">
                        <div class="pheix-presentation-slide-body">
                            <p>We use external module <strong>Router::Right</strong> for URL routing. <strong>Router::Right</strong> is Raku-based, framework-agnostic URL routing engine for web applications.</p>
                            <p>This module was ported from Perl5 implementation:<br><a href="https://github.com/mla/router-right" target="_blank">https://github.com/mla/router-right</a></p>
                            <p>Module repo: <a href="https://gitlab.com/pheix/router-right-perl6" target="_blank">https://gitlab.com/pheix/router-right-perl6</a></p>
                            <p><strong>Router::Right</strong> is well-documented, you could find docs, examples and best-practices in wiki: <a href="https://gitlab.com/pheix/router-right-perl6/wikis/home" target="_blank">https://gitlab.com/pheix/router-right-perl6/wikis/home</a></p>
                        </div>
                    </div>
                </section>
                <section class="pheix-presentation-slide">
                    <div class="pheix-presentation-slide-header pheix-presentation-slide-bg-1">Pheix: dependencies</div>
                    <div class="pheix-presentation-slide-body-container">
                        <div class="pheix-presentation-slide-body">
                            <p><strong>Pheix is dependable on:</strong></p>
                            <ul class="pheix-presentation-bigger-font">
                                <li><a href="https://github.com/timo/json_fast" target="_blank">JSON::Fast</a>:ver&lt;0.9.12&gt;</li>
                                <li><a href="https://github.com/sergot/http-useragent" target="_blank">HTTP::UserAgent</a>:ver&lt;1.1.49&gt;:authgithub:sergot</li>
                                <li><a href="https://gitlab.com/pheix/net-ethereum-perl6" target="_blank">Net::Ethereum</a>:ver&lt;0.0.95&gt;</li>
                                <li><a href="https://gitlab.com/pheix/router-right-perl6/" target="_blank">Router::Right</a>:ver&lt;0.0.44&gt;</li>
                                <li><a href="https://gitlab.com/pheix/lzw-revolunet-perl6/" target="_blank">LZW::Revolunet</a>:ver&lt;0.1.2&gt;</li>
                                <li><a href="https://github.com/sergot/openssl" target="_blank">OpenSSL</a>:ver&lt;0.1.23&gt;:authgithub:sergot</li>
                                <li><a href="https://github.com/raku-community-modules/MIME-Base64" target="_blank">MIME::Base64</a>:ver&lt;1.2.1&gt;:authgithub:retupmoca</li>
                                <li><a href="https://github.com/softmoth/p6-Template-Mustache" target="_blank">Template::Mustache</a>:ver&lt;1.0.1&gt;:authgithub:softmoth</li>
                                <li><a href="https://modules.perl6.org/dist/MagickWand:cpan:AZAWAWI" target="_blank">MagickWand</a>:ver&lt;0.1.0&gt;:authgithub:azawawi</li>
                                <li><a href="https://modules.perl6.org/dist/LibraryCheck:cpan:JSTOWE" target="_blank">LibraryCheck</a>:ver&lt;0.0.9&gt;:authgithub:jonathanstowe:api&lt;1.0&gt;</li>
                                <li><a href="https://github.com/bbkr/GeoIP2" target="_blank">GeoIP2</a>:ver&lt;1.1.0&gt;</li>
                            </ul>
                        </div>
                    </div>
                </section>
                <section class="pheix-presentation-slide16">
                    <div class="pheix-presentation-slide1-descrs-container">
                        <div class="pheix-presentation-slide1-descrs">
                            <div class="pheix-presentation-slide16-descr pheix-presentation-slide16-descr-1"><strong>1. </strong>How-To</div>
                            <div class="pheix-presentation-slide16-descr pheix-presentation-slide16-descr-2"><strong>2. </strong>Net::Ethereum</div>
                            <div class="pheix-presentation-slide16-descr pheix-presentation-slide16-descr-3"><strong>3. </strong>LZW Compression</div>
                        </div>
                    </div>
                </section>
                <section class="pheix-presentation-slide17">
                    <div class="pheix-presentation-slide-header pheix-presentation-slide-bg-1">Ethereum and Raku: dApp levels</div>
                    <div class="pheix-presentation-slide1-descrs-container">
                        <div class="pheix-presentation-slide1-descrs">
                            <div class="pheix-presentation-slide17-descr pheix-presentation-slide17-descr-1">Raku Application</div>
                            <div class="pheix-presentation-slide17-descr pheix-presentation-slide17-descr-2">Net::Ethereum API</div>
                            <div class="pheix-presentation-slide17-descr pheix-presentation-slide17-descr-3">Parity Client RPC</div>
                            <div class="pheix-presentation-slide17-descr pheix-presentation-slide17-descr-4">ETH blockchain</div>
                        </div>
                    </div>
                </section>
                <section class="pheix-presentation-slide">
                    <div class="pheix-presentation-slide-header pheix-presentation-slide-bg-1">Net::Ethereum</div>
                    <div class="pheix-presentation-slide-body-container">
                        <div class="pheix-presentation-slide-body">
                            <p><strong>Net::Ethereum</strong> module is a Raku interface for interacting with the Ethereum blockchain and ecosystem via JSON RPC API.<br>Module is available from: <a href="https://gitlab.com/pheix/net-ethereum-perl6" target="_blank">https://gitlab.com/pheix/net-ethereum-perl6</a></p>
                            <p>This module was ported from Perl5 <strong>Net::Ethereum</strong> with some changes, but original documentation is still actual, so feel free to use p5 docs with Raku <strong>Net::Ethereum</strong>: <a href="https://metacpan.org/pod/Net::Ethereum" target="_blank">https://metacpan.org/pod/Net::Ethereum</a></p>
                            <p>You should install Ethereum client (Geth, Parity, etc..) and Solidity compiler (optional) before using <strong>Net::Ethereum</strong>. You can work without Solidity with precompiled smart contracts.</p>
                        </div>
                    </div>
                </section>
                <section class="pheix-presentation-slide">
                    <div class="pheix-presentation-slide-header pheix-presentation-slide-bg-1">Lempel-Ziv-Welch (LZW) algorithm</div>
                    <div class="pheix-presentation-slide-body-container">
                        <div class="pheix-presentation-slide-body">
                            <p>When you're working with Ethereum blockchain you should permanently think about <strong>cost</strong> of your transactions (TXs). TX cost is estimated in <strong>gas</strong>.</p>
                            <p>TX cost depends on smart contract implementation: storage operations and dynamic arrays are expensive. Also TX cost depends on data amount to store on blockchain.</p>
                            <p>In private networks (PoA) you have unlimited gas amount, so cost of transaction is not a critical value. But amount of data is the thing, you should still think about. Big data decreases TX speed and utilize disk space, so it's a good idea to compress data before commit — text data can be compressed by LZW.</p>
                            <p>Pheix uses <a href="https://gitlab.com/pheix/lzw-revolunet-perl6/" target="_blank">LZW::Revolunet</a> module for LZW compression.</p>
                        </div>
                    </div>
                </section>
                <section class="pheix-presentation-slide20">
                    <div class="pheix-presentation-slide1-descrs-container">
                        <div class="pheix-presentation-slide1-descrs">
                            <div class="pheix-presentation-slide20-descr pheix-presentation-slide20-descr-1">Caching tools</div>
                            <div class="pheix-presentation-slide20-descr pheix-presentation-slide20-descr-2">Be asynchronous</div>
                            <div class="pheix-presentation-slide20-descr pheix-presentation-slide20-descr-3">Lazy techniques</div>
                        </div>
                    </div>
                </section>
                <section class="pheix-presentation-slide">
                    <div class="pheix-presentation-slide-header pheix-presentation-slide-bg-1">Caching tools</div>
                    <div class="pheix-presentation-slide-body-container">
                        <div class="pheix-presentation-slide-body">
                            <p>Common approach to increase speed of dApp is to minimize a number of TXs. First technique is to put caching tools to top data-storing layer and increase reading speed. Syncing with blockchain should be done after cache expiration. Cache expiration period should be the configurable value.</p>
                            <p>Most known Raku caching modules are:</p>
                            <ul class="pheix-presentation-bigger-font">
                                <li><strong>Cache::Memcached</strong> — Raku client for memcached a distributed caching daemon, <a href="https://github.com/cosimo/perl6-cache-memcached" target="_blank">https://github.com/cosimo/perl6-cache-memcached</a>;</li>
                                <li><strong>Redis</strong> — Raku client for Redis server, <a href="https://github.com/cofyc/perl6-redis" target="_blank">https://github.com/cofyc/perl6-redis</a>;</li>
                                <li><strong>Propius</strong> — Raku memory cache with loader and eviction by time, <a href="https://github.com/atroxaper/p6-Propius" target="_blank">https://github.com/atroxaper/p6-Propius</a>.</li>
                            </ul>
                        </div>
                    </div>
                </section>
                <section class="pheix-presentation-slide">
                    <div class="pheix-presentation-slide-header pheix-presentation-slide-bg-1">Asynchronous write</div>
                    <div class="pheix-presentation-slide-body-container">
                        <div class="pheix-presentation-slide-body">
                            <p>It's usual practice to avoid TXs mining wait in dApp. You have got TX hash immediately after TX sending, so you should analyze it asynchronously. By default Parity Ethereum PoA node mines new block every 15 seconds, that's why syncing with mining results is terrible, slowpoke technique.</p>
                            <p>Pheix provides the next solution: we've got TX hash, and immediately return a page with JS asynchronous Ajax code (callback). This code will raise pop-up notify message with TX results ASAP.</p>
                            <p>This solution requires the back-end functionality, that provides checking TX status by hash from front-end.</p>
                        </div>
                    </div>
                </section>
                <section class="pheix-presentation-slide">
                    <div class="pheix-presentation-slide-header pheix-presentation-slide-bg-1">Lazy loading and pipelines</div>
                    <div class="pheix-presentation-slide-body-container">
                        <div class="pheix-presentation-slide-body">
                            <p>Lazy loading is an efficient pattern to save resources and increase response time while reading data from blockchain. In Pheix we store text data in 4KB frames (e.g. 100KB text is stored in 25 frames), so when we need to output the text preview, we do not read whole text — we're reading just first frame. Also we show <strong>&lt;MORE&gt;</strong> hyperlinks with async JS callbacks everywhere it's not conflicting with UI/UX.</p>
                            <p>Frames are useful while store data on blockchain. When we post 100KB text — we proceed pipeline of 25 4KB frames with async technique, discussed on previous slide. Pipeline provides ability to output progress bar or equal widget, that show process status.</p>
                        </div>
                    </div>
                </section>
                <section class="pheix-presentation-slide24">
                    <div class="pheix-presentation-slide-header pheix-presentation-slide-bg-1 pheix-presentation-slide24-header">Pheix: hybrid model of data storing</div>
                    <div class="pheix-presentation-slide1-descrs-container">
                        <div class="pheix-presentation-slide1-descrs">
                            <div class="pheix-presentation-slide24-descr pheix-presentation-slide24-descr-1">Regular<br>relational<br>database</div>
                            <div class="pheix-presentation-slide24-descr pheix-presentation-slide24-descr-2">Ethereum<br>blockchain</div>
                            <div class="pheix-presentation-slide24-descr pheix-presentation-slide24-descr-3">Flat-file<br>database</div>
                            <div class="pheix-presentation-slide24-descr pheix-presentation-slide24-descr-4">PHEIX</div>
                        </div>
                    </div>
                </section>
                <section class="pheix-presentation-slide">
                    <div class="pheix-presentation-slide-header pheix-presentation-slide-bg-1">Regular relational database</div>
                    <div class="pheix-presentation-slide-body-container">
                        <div class="pheix-presentation-slide-body">
                            <p>Pheix has database independent API layer with basic data access methods (CRUD). So, it's possible to connect any regular relational database, supported by Raku <strong>DBIsh</strong> module: <a href="https://github.com/perl6/DBIish" target="_blank">https://github.com/perl6/DBIish</a>. Hybrid model of data storing supposes that you optimize your dApp in the next directions:</p>
                            <ul class="pheix-presentation-bigger-font">
                                <li>classify your data — tamper-proof data, tamper-sensitive data, temporary data, cache, etc...;</li>
                                <li>store different types of data in different storages.</li>
                            </ul>
                            <p>Regular relational databases are good for no-critical data: if it will be tamper, nothing critical happens.</p>
                        </div>
                    </div>
                </section>
                <section class="pheix-presentation-slide">
                    <div class="pheix-presentation-slide-header pheix-presentation-slide-bg-1">Flat-file database</div>
                    <div class="pheix-presentation-slide-body-container">
                        <div class="pheix-presentation-slide-body">
                            <p>Flat-file database is similar to relational database in terms of data integrity and tamper protection. Ok, relational DBs have built-in user accounts, permissions, etc... But in sense of data tamper they are vulnerable.</p>
                            <p>Flat-files are efficient quick-start solution on low-resources devices or servers with restricted install permissions. Flat-files databases could be syncing with popular clouds out-of-the-box and have advantages such as: simple dumping, cross server moving and backuping.</p>
                            <p>Flat-file database is simple to load to blockchain with trivial JS parsers.</p>
                        </div>
                    </div>
                </section>
                <section class="pheix-presentation-slide">
                    <div class="pheix-presentation-slide-header pheix-presentation-slide-bg-1">Ethereum blockchain</div>
                    <div class="pheix-presentation-slide-body-container">
                        <div class="pheix-presentation-slide-body">
                            <p>Pheix uses blockchain as a database (ledger) that is shared across a private PoA network. This ledger is encrypted such that only authorized parties can access the data. Since the data is shared, the records cannot be tampered. Thus, the data will not be held by a single entity.</p>
                            <p>By decentralizing data storage, we improve the security of the data. Any attack or outage at a single point will not have a devastating effect because other nodes in other locations will continue to function.</p>
                            <p>Blockchain is good for tamper-proof data: vote results, ratings, test marks, bug tracks, changes history and others.</p>
                        </div>
                    </div>
                </section>
                <section class="pheix-presentation-slide28">
                    <div class="pheix-presentation-slide1-descrs-container">
                        <div class="pheix-presentation-slide1-descrs">
                            <div class="pheix-presentation-slide28-descr pheix-presentation-slide28-descr-1">Late α-release <strong>06 Apr, 2020</strong></div>
                            <div class="pheix-presentation-slide28-descr pheix-presentation-slide28-descr-2">Public β-release</div>
                            <div class="pheix-presentation-slide28-descr pheix-presentation-slide28-descr-2d"><strong>19 February 2021</strong></div>
                            <div class="pheix-presentation-slide28-descr pheix-presentation-slide28-descr-3">Private β-release <strong>20 Jun, 2020</strong></div>
                        </div>
                    </div>
                    <div class="pheix-presentation-slide28-roadmap-descrs">
                        <div class="pheix-presentation-slide28-roadmap-descr-1">
                            <strong>Private beta release:</strong>
                            <ul>
                                <li>blockchain support;</li>
                                <li>implementation of database independent API layer;</li>
                                <li>unit and regressive tests.</li>
                            </ul>
                        </div>
                        <div class="pheix-presentation-slide28-roadmap-descr-2">
                            <strong>Public beta release:</strong>
                            <ul>
                                <li>Migrate from Apache+CGI;</li>
                                <li>Basic implementation of administration layer;</li>
                                <li>Module scaling features;</li>
                                <li>Porting Weblog module from P5 Pheix;</li>
                                <li>Run Raku Pheix site with demo access.</li>
                            </ul>
                        </div>
                    </div>
                </section>
                <section class="pheix-presentation-slide29">
                    <div class="pheix-presentation-slide29-credits">
                        <p>
                            <strong>konstantin@narkhov.pro</strong><br>
                            <a href="https://narkhov.pro" target="_blank">https://narkhov.pro</a><br>
                            <a href="https://gitlab.com/pheix" target="_blank">https://gitlab.com/pheix</a><br>
                            <a href="https://www.linkedin.com/in/knarkhov/" target="_blank">https://www.linkedin.com/in/knarkhov/</a><br>
                        </p>
                        <p class="pheix-presentation-slide29-credits-donate">Support Pheix: <a href="https://pheix.org/#donate" target="_blank">https://pheix.org/#donate</a></p>
                    </div>
                </section>
                </div>
            </div>
        </div>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/reveal.js/3.6.0/js/reveal.min.js"></script>
        <script>
            Reveal.initialize();
        </script>
    </body>
</html>
