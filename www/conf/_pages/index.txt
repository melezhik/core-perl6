<pheix-content>
    <p>I can not ever imagine the war against the neighborhoods, brothers and mostly relatives.</p>
    <p><strong>Looks like apocalypse.</strong></p>
    <p>Covering by hypocritical speeches about peace and help, they bomb the territories never asked for any kind of support. They are blocking any news opposite to the offical ones, they ban anyone who says: this is attack, this is invasion, this is the war. But <strong>THIS IS WAR</strong>, maybe the last war ever. Stop it!</p>
    <p>I want the developers all over the world to claim — <strong>STOP THIS WAR</strong>: add the comment to your code, add the badge to your repository, add anti-war pitch to your doc or white paper.</p>
    <a href="#"><img src="resources/skins/akin/img/index/no-war-2022.jpg" class="img-fluid"></a>
    <hr>
    <h1 class="_phx-hdr-1">{ props.tmpl_pageheader }</h1>
    <p class="_phx-vpad15">Pheix – <a href="https://opensource.org/licenses/Artistic-2.0" target="_blank">Artistic 2.0 license</a> compliant, <a href="https://raku.org/" target="_blank">#Rakulang</a> driven content management system with data storing on <a href="https://ethereum.org/" target="_blank">#Ethereum</a> blockchain. It is extremely lightweight, fast &amp; scalable.</p>
    <hr>
    <div id="pheixIndexCarousel" class="carousel slide">
        <ol class="carousel-indicators">
            <li data-target="#pheixIndexCarousel" data-slide-to="0" class="active"></li>
            <!--<li data-target="#pheixIndexCarousel" data-slide-to="1"></li>-->
            <li data-target="#pheixIndexCarousel" data-slide-to="2"></li>
            <li data-target="#pheixIndexCarousel" data-slide-to="3"></li>
        </ol>
        <div class="carousel-inner">
            <div class="carousel-item active">
                <a href="/embedded/fosdem22-raku-auth"><img class="d-block w-100" src="resources/skins/akin/img/index/slider/lowres/slide-06.jpg" alt="Decentralized authentication at FOSDEM 2022"></a>
            </div>
            <!--<div class="carousel-item">
                <a href="/embedded/the-raku-conference-2021"><img class="d-block w-100" src="resources/skins/akin/img/index/slider/lowres/slide-01.jpg" alt="The Raku Conference 2021"></a>
            </div>-->
            <div class="carousel-item">
                <a href="/embedded/pheix-is-beta-now"><img class="d-block w-100" src="resources/skins/akin/img/index/slider/lowres/slide-02.jpg" alt="Pheix CMS public β-release"></a>
            </div>
            <div class="carousel-item">
                <a href="https://gitlab.com/pheix-pool/core-perl6" target="_blank"><img class="d-block w-100" src="resources/skins/akin/img/index/slider/lowres/slide-03.jpg" alt="Pheix CMS repository"></a>
            </div>
        </div>
        <a class="carousel-control-prev" href="#pheixIndexCarousel" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#pheixIndexCarousel" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
        </a>
    </div>
    <hr>
    <h2 class="_phx-hdr-2">Important</h2>
    <p>{ props.tmpl_paragraph }
    <p>
    <p>Pheix is distributed via <a href="https://gitlab.com/pheix-pool/core-perl6" target="_blank">GitLab</a> &ndash; feel free to make merge requests!</p>
    <hr>
    <a id="email"></a>
    <h2 class="_phx-hdr-2">Get in touch</h2>
    <p>Via email: dev<span class="_phx-opacity-8 text-secondary">[at-sign]</span>pheix.org</p>
    <hr>
    <h2 class="_phx-hdr-2">Changelog</h2>
    <p>Full changes <a href="changelog.txt" target="_blank">history</a>.</p>
    <hr>
    <h2 class="_phx-hdr-2">Server summary</h2>
    <p>{ props.tmpl_rakudo_ver }</p>
    <hr>
    <h2 class="_phx-hdr-2">Environment variables</h2>
    <div class="pheix-quote text-muted _phx-fnt10">
        <span class="d-block" each={ env_var in props.tmpl_env_vars }>{ env_var }</span>
    </div>
    <hr>
    <h2 class="_phx-hdr-2">Support Pheix project!</h2>
    <p>
        <span class="_phx-inline-blck _phx-wdth-60">PayPal:</span><a class="pheix-wallet" href="https://www.paypal.me/pheix/15" target="_blank">$15 or more</a><br>
        <span class="_phx-inline-blck _phx-wdth-60">BTC:</span><span class="pheix-wallet">bc1qpdrmyh46zjmt3vqzn00vfs6d54u8d7uw7acx3d</span><br>
        <span class="_phx-inline-blck _phx-wdth-60">ETH:</span><span class="pheix-wallet">0x369643FD63ecd439A9d735a9c9E5603898097354</span><br>
        <span class="_phx-inline-blck _phx-wdth-60">ZEC:</span><span class="pheix-wallet">t1fyqYrZAfLyUr3xpqCGJasDUbk5Rpx3fqL</span>
    </p>
    <div class="d-none">
        <img id="pheix-captcha" onload="{ loadCaptcha }" src="resources/skins/akin/img/captcha-bulk.png" alt="sample hidden captcha">
    </div>
    <hr>

    <script>
    export default {
        onBeforeMount(props, state) {
            this.state = {
                captchaIsLoaded: false,
                sessToken: this.props.tmpl_sesstoken,
                enCaptcha: this.props.tmpl_encaptcha,
            }
        },
        onMounted() {
            if (jQuery(".carousel")[0]){
                console.log('responsive component is trying to start carousel');
                jQuery('.carousel').carousel('cycle');
            }
        },
        loadCaptcha(e) {
            if (this.state.captchaIsLoaded) {
                return 0;
            }
            else {
                window.PheixApi.load_captcha(this.state.enCaptcha, this.state.sessToken);
                this.update({captchaIsLoaded: true});
            }

            return 1;
        }
    }
    </script>
</pheix-content>
