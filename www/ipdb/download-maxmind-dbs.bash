#!/usr/bash

LICENCE=$1

if [ -z "$LICENCE" ]; then
	echo -e "USAGE: bash download-maxmind-dbs.bash license-hash"
	echo -e "More details at: https://blog.maxmind.com/2019/12/18/significant-changes-to-accessing-and-using-geolite2-databases/"
	exit -1
fi

RED='\e[0;31m'
GREEN='\e[0;32m'
YELLOW='\e[0;33m'
NC='\e[0m'
GEOLITECITYEDIT='GeoLite2-City'
GEOLITECNTREDIT='GeoLite2-Country'
GEOLITEDATE='20191231'
GEOLITECITY="https://download.maxmind.com/app/geoip_download?edition_id=$GEOLITECITYEDIT&date=$GEOLITEDATE&license_key=$LICENCE&suffix=tar.gz"
GEOLITECNTR="https://download.maxmind.com/app/geoip_download?edition_id=$GEOLITECNTREDIT&date=$GEOLITEDATE&license_key=$LICENCE&suffix=tar.gz"

find . ! -name 'download-maxmind-dbs.bash' -type f -exec rm -f {} + 1> /dev/null 2>&1
if [ $? -eq 0 ]
then
    find . ! -name 'download-maxmind-dbs.bash' -o -name '.' -o -name '..' -type d -exec rm -fr {} + 1> /dev/null 2>&1
	if [ $? -eq 0 ]; then
		echo -e "[ ${GREEN}directory clean successful${NC} ]"
	else
		echo -e "Errors while ${YELLOW}cleaning${NC} subdirectories"
	fi
else
	echo -e "Errors while ${YELLOW}cleaning${NC} files"
fi

if ls ./*.mmdb 1> /dev/null 2>&1; then
	rm -rf ./*.mmdb
	echo -e "[ ${GREEN}old ./*.mmdb files are removed${NC} ]"
else
	echo -e "Skip delete of ${YELLOW}./*.mmdb${NC}: not existed"
fi

curl -fsSL $GEOLITECNTR -o $GEOLITECNTREDIT.tar.gz

if [ $? -eq 0 ]
then
	tar xzf $GEOLITECNTREDIT.tar.gz -C ./
	rm -f $GEOLITECNTREDIT.tar.gz
	ln -s $GEOLITECNTREDIT'_'$GEOLITEDATE/$GEOLITECNTREDIT.mmdb $GEOLITECNTREDIT.mmdb
	echo -e "[ ${GREEN}$GEOLITECNTREDIT database is processed${NC} ]"
else
	echo -e "[ ${RED}$GEOLITECNTREDIT database download error${STAGE}${NC} ]"
	exit -1;
fi

curl -fsSL $GEOLITECITY -o $GEOLITECITYEDIT.tar.gz

if [ $? -eq 0 ]
then
	tar xzf $GEOLITECITYEDIT.tar.gz -C ./
	rm -f $GEOLITECITYEDIT.tar.gz
	ln -s $GEOLITECITYEDIT'_'$GEOLITEDATE/GeoLite2-City.mmdb GeoLite2-City.mmdb
	echo -e "[ ${GREEN}$GEOLITECITYEDIT database is processed${NC} ]"
else
	echo -e "[ ${RED}$GEOLITECITYEDIT database download error${STAGE}${NC} ]"
	exit -1;
fi

echo -e "[ ${GREEN}OK${NC} ] $GEOLITECITYEDIT and $GEOLITECNTREDIT databases are updated"

#geoipupdate -v -d .

#if [ $? -eq 0 ]
#then
#	echo -e "[ ${GREEN}$GEOLITECITYEDIT and $GEOLITECNTREDIT databases are proceed${NC} ]"
#else
#	echo -e "[ ${RED}$GEOLITECITYEDIT and $GEOLITECNTREDIT databases update via geoipupdate error${STAGE}${NC} ]"
#	exit -1;
#fi
