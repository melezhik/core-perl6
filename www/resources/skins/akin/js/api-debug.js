function sendRequest() {
    jQuery("#response").val('working...')

    var data = jQuery("#request").val();

    if (data) {
        var target = jQuery("#apitarget").val() === '2' ? '/logger' : '/api';

        jsonobj = JSON.parse(data);
        jsonobj.route = encodeURIComponent(jsonobj.route);
        
        var jqxhr  = jQuery.post(target, JSON.stringify(jsonobj), function(response) {
            console.log(response);
            jQuery("#response").val(response)
        })
        .fail(function() {
            //console.log('API request is failed');
            jQuery("#response").val('API request to ' + target + ' is failed')
        })
        .always(function() {
            console.log('API request to ' + target + ' is finished');
        });
    }
    else {
        //console.log('empty request');
        jQuery("#response").val('empty request')
    }
}

jQuery("#api-debug-container").append(
    "<div class=\"row m-0 p-0\">" +
    "<div class=\"col-md-6 m-0 p-0 ml-md-0 pl-md-0 pr-md-2\">" +
    "<textarea id=\"request\" class=\"form-control\" rows=\"15\"></textarea></div>" +
    "<div class=\"col-md-6 m-0 p-0\">" +
    "<textarea id=\"response\" class=\"form-control\" rows=\"15\" readonly></textarea></div>" +
    "</div>"
);

jQuery("#api-debug-container").append(
    "<div class=\"form-inline mt-2\">" +
    "<select class=\"custom-select my-1 mr-sm-2\" id=\"apitarget\"><option value=\"1\" selected>Content API</option><option value=\"2\">Logger API</option></select>" +
    "<input type=\"button\" class=\"btn btn-primary my-1\" onclick=\"sendRequest()\" value=\"Submit\">" +
    "</div>"
);
