unit class Pheix::Model::JSON;

use JSON::Fast;

has %!setup;
has Str $.path      = 'conf';
has Str $.addonpath = 'conf/addons';
has Str $.jsonfn    = 'config.json';
has Str $.root      = 'module';
has Str $!lf        = "\x[0A]";

method as-json(Any :$data) {
    return to-json($data, :pretty);
}

method !write_conf_file(Str :$fname, :%config) returns Bool {
    my Bool $ret = False;
    if $fname && $fname.IO.e {
        my $fh = $fname.IO.open(:w);
        if $fh.lock {
            if $fh.spurt((to-json(%config, :pretty)) ~ $!lf ) {
                $ret = $fh.unlock;
            }
        }
        $fh.close if $fh;
    }

    return $ret;
}

method get_json_fn(Str $addon) returns Str {
    my Str $fn = Nil;
    if $addon {
        given $addon {
            when 'Pheix' {
                $fn = $!path ~ q{/} ~ $!jsonfn;
            }
            default {
                $fn = $!addonpath ~ q{/} ~ $addon ~ q{/} ~ $!jsonfn;
            }
        }
        if $fn {
            if !($fn.IO.e) {
                $fn = Nil;
            }
        }
    }

    return $fn;
}

method set_entire_config(
    Str :$addon,
    :%setup
) returns Pheix::Model::JSON {
    if !%setup.keys.elems {
        my $fn = self.get_json_fn($addon);

        if $fn {
            %!setup = from-json($fn.IO.slurp);
        }
    }
    else {
        %!setup = %setup;
    }

    return self;
}

method get_entire_config(Str :$addon, Bool :$nocache = False) returns Hash {
    my %config;
    my $fn = self.get_json_fn($addon);

    if $fn && $nocache {
        %config = from-json($fn.IO.slurp);
    }
    else {
        %config = %!setup;
    }

    return %config;
}

method get_conf_value(
    Str $addon,
    Str $conf,
    Bool :$nocache = False
) returns Cool {
    my Cool $value = Nil;

    if $conf {
        my %setup =
            %!setup{$!root}:exists && !$nocache ??
                %!setup !!
                    self.get_entire_config(:addon($addon), :nocache(True));

        if %setup {
            if %setup{$!root}{$conf}<value>:exists {
                $value = %setup{$!root}{$conf}<value> ~~ Str ??
                    self.check_int(%setup{$!root}{$conf}<value>)
                        !! %setup{$!root}{$conf}<value>
            }
        }
    }

    return $value;
}

method get_setting(
    Str  $addon,
    Str  $setting,
    Str  $key,
    Bool :$nocache = False,
    Str  :$conf
) returns Cool {
    my Cool $value  = Nil;
    my Str $cmember = $conf // 'settings';

    if $setting and $key {
        my %setup =
            %!setup{$!root}:exists && !$nocache ??
                %!setup !!
                    self.get_entire_config(:addon($addon), :nocache(True));

        if %setup {
            my %settings = %setup{$!root}<configuration>{$cmember};

            if %settings{$setting}{$key}:exists {
                $value = %settings{$setting}{$key} ~~ Str ??
                    self.check_int(%settings{$setting}{$key}) !!
                        %settings{$setting}{$key}
            }
        }
    }

    return $value;
}

method get_group_setting(
    Str  $addon,
    Str  $group,
    Str  $setting,
    Str  $key,
    Bool :$nocache = False,
    Str  :$conf
) returns Cool {
    my Cool $value  = Nil;
    my Str $cmember = $conf // 'settings';

    if $group and $setting and $key {
        my %setup =
            %!setup{$!root}:exists && !$nocache ??
                %!setup !!
                    self.get_entire_config(:addon($addon), :nocache(True));

        if %setup {
            my %settings = %setup{$!root}<configuration>{$cmember};

            if %settings{$group}<group>{$setting}{$key}:exists {
                $value = %settings{$group}<group>{$setting}{$key} ~~ Str ??
                    self.check_int(%settings{$group}<group>{$setting}{$key}) !!
                        %settings{$group}<group>{$setting}{$key};
            }
        }
    }

    return $value;
}

method get_all_settings_for_group_member(
    Str $addon,
    Str $group,
    Str $setting,
    Str :$conf,
    Bool :$nocache = False
) returns Hash {
    my %rc;
    my Str $cmember = $conf // 'settings';

    if $group and $setting {
        my %setup =
            %!setup{$!root}:exists && !$nocache ??
                %!setup !!
                    self.get_entire_config(:addon($addon), :nocache(True));

        if %setup {
            my %settings = %setup{$!root}<configuration>{$cmember};

            if %settings{$group}<group>{$setting}:exists {
                %rc = %settings{$group}<group>{$setting};
            }
        }
    }

    return %rc;
}

method is_setting_group(Str $addon, Str $group) returns Int {
    my Int $ret = 0;
    if $group {
        my %setup = self.get_entire_config(:addon($addon));

        if %setup {
            my %settings = %setup{$!root}<configuration><settings>;

            if %settings{$group}<group>:exists {
                $ret = %settings{$group}<group>.elems;
            }
        }
    }

    return $ret;
}

method check_int(Str $_v) returns Any {
    return ($_v ~~ m/^ <[\d]>+ $/ ?? $_v.Int !! $_v);
}

method set_group_setting(
    Str $addon,
    Str $group,
    Str $setting,
    Str $key,
    Cool $value,
    Bool :$temporary = False
) returns Bool {
    my Bool $ret = False;
    if $group and $setting and $key {
        my $fn    = self.get_json_fn( $addon );
        my %setup = self.get_entire_config(:addon($addon));

        if %setup && $fn {
            my %settings = %setup{$!root}<configuration><settings>;

            if %settings{$group}<group>{$setting}{$key}:exists {

                %settings{$group}<group>{$setting}{$key} = $value ~~ Str ??
                    self.check_int($value) !!
                        $value;

                %setup{$!root}<configuration><settings> = %settings;

                if $temporary {
                    self.set_entire_config(:addon($addon), :setup(%setup));

                    $ret = True;
                }
                else {
                    $ret = self!write_conf_file(:fname($fn), :config(%setup));
                }
            }
        }
    }

    return $ret;
}

method set_setting(
    Str $addon,
    Str $setting,
    Str $key,
    Cool $value,
    Bool :$temporary = False
) returns Bool {
    my Bool $ret = False;
    if $setting and $key {
        my $fn    = self.get_json_fn($addon);
        my %setup = self.get_entire_config(:addon($addon));

        if %setup && $fn {
            my %settings = %setup{$!root}<configuration><settings>;

            if %settings{$setting}{$key}:exists {

                %settings{$setting}{$key} = $value ~~ Str ??
                    self.check_int($value) !!
                        $value;

                %setup{$!root}<configuration><settings> = %settings;

                if $temporary {
                    self.set_entire_config(:addon($addon), :setup(%setup));

                    $ret = True;
                }
                else {
                    $ret = self!write_conf_file(:fname($fn), :config(%setup));
                }
            }
        }
    }

    return $ret;
}

method set_conf_value(
    Str $addon,
    Str $conf,
    Cool $value,
    Bool :$temporary = False
) returns Bool {
    my Bool $ret = False;
    if $conf {
        my $fn    = self.get_json_fn($addon);
        my %setup = self.get_entire_config(:addon($addon));

        if %setup && $fn {
            if %setup{$!root}{$conf}<value>:exists {
                %setup{$!root}{$conf}<value> = $value ~~ Str ??
                    self.check_int($value) !!
                        $value;

                if $temporary {
                    self.set_entire_config(:addon($addon), :setup(%setup));

                    $ret = True;
                }
                else {
                    $ret = self!write_conf_file(:fname($fn), :config(%setup));
                }
            }
        }
    }

    return $ret;
}
