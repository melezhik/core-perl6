use Pheix::Model::JSON;
use Pheix::Model::Resources::En;

class Pheix::Model::Resources {
    has Bool $.test   = False;
    has Str  $.parent is rw;
    has Str  $.locale =
        Pheix::Model::JSON.new.get_setting('Pheix', 'locale', 'value') // Str;

    method check_methods( Str :$class ) returns Bool {
        (require ::($class)) andthen Nil;

        my @a = Pheix::Model::Resources::En.new.^methods.sort({.gist});
        my @b = ::($class).new.^methods.sort({.gist});

        (@a == @b) ?? True !! False;
    }

    method init returns Mu {
        $!parent = sprintf("%s\:\:%s", self.^name, $!locale) if $!locale;

        try {
            if !self.check_methods(:class($!parent)) {
                $!parent = 'Pheix::Model::Resources::En';
            }
            CATCH {
                default {
                    $!parent = 'Pheix::Model::Resources::En';
                }
            }
        }

        (require ::($!parent)) andthen Nil;
        
        my $c = Metamodel::ClassHOW.new_type(:name('Pheix::Model::Resources'));

        $c.HOW.add_parent($c, ::($!parent).new);
        $c.HOW.compose($c);
        $c.new(
            test   => $!test,
            parent => $!parent,
            locale => $!locale,
        );
    }
}
