unit class Pheix::Model::Database::Blockchain::SendTx;

has Any  $.targetobj is default(Nil);
has Any  $.signerobj is default(Nil);
has Bool $.debug is default(False);

method send_signed_tx(
    Str  :$method!,
    Hash :$data,
    UInt :$txgas,
    Bool :$waittx
) returns Str {
    my Str $tx;

    if $!signerobj {
        my $mdata = $!signerobj.ethobj.marshal($method, $data);
        my $raw   = self.sign(:marshalledtx($mdata), :gasqty($txgas), :waittx($waittx));

        $tx = self.send(:rawtx($raw));

        $!targetobj.sgnlog.tail ~= sprintf("|%s|%12s", $tx, $method) if $!targetobj.sgnlog.elems > 0;
        sprintf("**SGN: %s", $!targetobj.sgnlog.tail).say if $!debug;
    }
    else {
        $tx = $!targetobj.ethobj.sendTransaction(
            :account($!targetobj.ethacc),
            :scid($!targetobj.scaddr),
            :fname($method),
            :fparams($data),
            :gas($txgas || $!targetobj.gasqty),
        );
    }

    sprintf("**SGN: sent %s tx %s", $!signerobj ?? 'signed' !! 'generic', $tx)
        .say if $!debug;

    $tx;
}

method selfcheck returns Bool {
    $!targetobj.dbswitch == 1 && $!signerobj.dbswitch == 1 ??
        True !!
            False;
}

method sign(
    Str :$marshalledtx!,
    Str :$blocktag = 'pending',
    UInt :$gasqty,
    Bool :$waittx
) returns Str {
    my Str $noncedetails;
    sprintf("**SGN: try to sign tx for %s", $!targetobj.table).say if $!debug;

    # get target gas price
    my UInt $gp = $!targetobj.ethobj.eth_gasPrice;

    # get target nonce
    my UInt $nc = $!targetobj.ethobj.eth_getTransactionCount(
        :data($!targetobj.ethacc),
        :tag($blocktag)
    ) if $waittx || $!targetobj.nonce == 0;

    # unlock account
    sprintf("**SGN: try to unlock %s addr at %s", $!signerobj.ethacc, $!signerobj.apiurl).say if $!debug;

    $!signerobj.ethobj.personal_unlockAccount(
        :account($!signerobj.ethacc),
        :password($!signerobj.ethobj.unlockpwd),
    );

    if $nc.defined && $nc > $!targetobj.nonce {
        sprintf("**SGN: got nonce %u (local=%u), block=%s", $nc, $!targetobj.nonce, ~$blocktag).say if $!debug;

        $!targetobj.nonce = $nc;
    }
    else {
        sprintf("**SGN: gen nonce %u (%s)", $!targetobj.nonce, $waittx.Str).say if $!debug;

        $nc = $nc.defined && $nc == 0 ?? 0 !! (++$!targetobj.nonce);
    }

    # output nonce state
    sprintf("**SGN: nonce local=%u, nonce remote=%u", $!targetobj.nonce, $nc).say if $!debug;

    # sign the transaction
    my %sign = $!signerobj.ethobj.eth_signTransaction(
        :from($!targetobj.ethacc),
        :to($!targetobj.scaddr),
        :gas($gasqty // $!targetobj.gasqty),
        :gasprice($gp),
        :nonce($nc),
        :data($marshalledtx)
    );

    # logging
    $!targetobj.sgnlog.push([
        sprintf("%06d", $nc),
        sprintf("%10d", $!targetobj.cmpobj.lzw.get_bytes(:s($marshalledtx))),
        sprintf("%10d", $gasqty // $!targetobj.gasqty),
    ].join(q{|}));

    (%sign<raw>:exists && %sign<raw> ~~ m:i/^ 0x<xdigit>+ $/) ??
        %sign<raw> !!
            Str;
}

method send(Str :$rawtx!) returns Str {
    if $!debug {
        sprintf("**SGN: try to send signed tx to %s", $!targetobj.apiurl).say;
        sprintf("**SGN: raw transaction data\n%s", $rawtx).say;
    }

    $!targetobj.ethobj.eth_sendRawTransaction(:data($rawtx));
}
