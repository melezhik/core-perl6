unit class Pheix::Model::Database::Compression;

use Pheix::Datepack;
use LZW::Revolunet;
use Compress::Bzip2;

has Bool $.test          = False;
has uint $.dictsize      = 97000;
has LZW::Revolunet $.lzw = LZW::Revolunet.new(:dictsize($!dictsize));

method compress(Str :$data!) returns Str {
    my Str $ret = q{};
    if $!lzw && $data {
        my $encoded = $!lzw.encode_utf8(:s($data));

        try {
            if $!test {
                X::AdHoc.new(:payload('crash test')).throw;
            }
            else {
                $ret = $!lzw.compress(:s($encoded));
            }

            CATCH {
                default {
                    if not $!test {
                        $*ERR.say: .message;

                        for .backtrace.reverse {
                            $*ERR.say: "  in block {.subname} at {.file} line {.line}";
                        }
                    }

                    my $logf = self.dumper(:unixtime(397556573), :data($data));
                    my $logb = self.dumper(:unixtime(397556573), :data($data), :bin);

                    my $dumpmsg = sprintf("compress failure, dumps at\n\t%s\n\t%s", $logf, $logb);

                    X::AdHoc.new(:payload($dumpmsg)).throw;
                }
            }
        }
    }

    $ret;
}

method decompress(Str :$data!) returns Str {
    my Str $ret = q{};

    if $!lzw && $data {
        my $decmp = $!lzw.decompress(:s($data));

        $ret = $!lzw.decode_utf8(:s($decmp));
    }

    $ret;
}

method get_bytes(Str :$data) returns uint {
    my uint $ret = 0;

    if $!lzw && $data {
        $ret = $!lzw.get_bytes(:s($data));
    }

    $ret;
}

method get_ratio returns int {
    my int $ret = 0;

    if $!lzw {
        $ret = $!lzw.get_ratio;
    }

    $ret;
}

method dumper(int :$unixtime, Str :$data, Bool :$bin, Bool :$dry) returns Str {
    my Str $fname;
    my $dateobj = Pheix::Datepack.new(:unixtime($unixtime));

    if $bin {
        $fname = $dateobj.get_date_filename(:extension('bin'));

        if !$dry {
            my $databuf = Buf[uint8].new($data.encode);
            my $fhandle = $fname.IO.open(:w, :bin);

            $fhandle.write(compressToBlob($databuf));
            $fhandle.close;
        }
    }
    else {
        $fname = $dateobj.get_date_filename;

        spurt $fname, $data if !$dry;
    }

    $fname;
}
