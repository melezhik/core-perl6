# November
# ========
#
# November is Copyright (C) 2008-2009, the November contributors (as listed in
# CONTRIBUTING).
#
# LICENSE INFORMATION
# -------------------
#
# November is free software; you can redistribute it and/or modify it under the
# terms of the Artistic License 2.0.  (Note that, unlike the Artistic License
# 1.0, version 2.0 is GPL compatible by itself, hence there is no benefit to
# having an Artistic 2.0 / GPL disjunction.)
#
# CONTRIBUTING
# ------------
#
# We'll hand out commit bits liberally. If you want to contribute, create an
# account on github.org and send your github handle to Johan
# (johan.viklund@gmail.com).  Patches are ok too, if you prefer those.  See
# docs/COMMITTERS if these thoughts intrigue you.
#
# CONTACT
# -------
#
# Google group: november-wiki@googlegroups.com
# IRC channel: #november-wiki over at irc.freenode.org
# Github: https://github.com/viklund/november

unit class Pheix::Addons::November::CGI;

has Bool $!test;
has Bool $!unescape;
has %.params;
has %.cookie;
has @.keywords;
has $!crlf = "\x[0D]\x[0A]";

submethod BUILD(Bool :$nounesc, Bool :$test?) {
    $!unescape = $nounesc ?? False !! True;
    $!test     = $test || False;
    $!crlf = "\x[0D]\x[0A]";
    self.parse_params(%*ENV<QUERY_STRING> // '');
    if (%*ENV<REQUEST_METHOD> // '') eq 'POST' {
        my $input = $*IN.slurp if !$!test;
        self.parse_params($input || Str);
    }
}

method send_response($contents, %opts?) {
    print "Content-Type: text/html; charset=utf-8$!crlf";
    if %opts && %opts<cookie> {
        print "Set-Cookie: {%opts<cookie>}; path=/;$!crlf";
    }
    print "$!crlf";
    print $contents;
}

method redirect($uri, %opts?) {
    my $status = '302 Moved' || %opts<status>;
    print "Status: $status$!crlf";
    print "Location: $uri";
    print "$!crlf$!crlf";
}

method parse_params(Str $string) {
    if ( $string.defined && $string ~~ / '&' | ';' | '=' / ) {
        my @param_values = $string.split(/ '&' | ';' /);
        for @param_values -> $param_value {
            my @kvs = $param_value.split("=");
            self.add_param(
                @kvs[0],
                $!unescape ?? unescape(@kvs[1]) !! @kvs[1],
            );
        }
    }
    else {
        self.parse_keywords($string || Str);
    }
}

method parse_keywords (Str $string is copy) {
    if $string.defined {
        my $kws = $!unescape == 1 ?? unescape($string) !! $string;
        @!keywords = $kws.split(/ \s+ /);
    }
}

our sub unescape($string is copy) {
    $string .= subst('+', ' ', :g);
    while $string ~~ / ( [ '%' <[0..9A..F]>**2 ]+ ) / {
        $string .= subst( ~$0,
        percent_hack_start( decode_urlencoded_utf8( ~$0 ) ) );
    }
    return percent_hack_end( $string );
}

sub percent_hack_start($str is copy) {
    if $str ~~ '%' {
        $str = '___PERCENT_HACK___';
    }
    return $str;
}

sub percent_hack_end($str) {
    return $str.subst('___PERCENT_HACK___', '%', :g);
}

sub decode_urlencoded_utf8($str) {
    my $r = '';
    my @chars = map { :16($_) }, $str.split('%').grep({$^w});
    while @chars {
        my $bytes = 1;
        my $mask  = 0xFF;
        given @chars[0] {
            when { $^c +& 0xF0 == 0xF0 } { $bytes = 4; $mask = 0x07 }
            when { $^c +& 0xE0 == 0xE0 } { $bytes = 3; $mask = 0x0F }
            when { $^c +& 0xC0 == 0xC0 } { $bytes = 2; $mask = 0x1F }
        }
        my @shift = (^$bytes).reverse.map({6 * $_});
        my @mask  = $mask, Slip(0x3F xx $bytes-1);
        $r ~= chr( [+] @chars.splice(0,$bytes) »+&« @mask »+<« @shift );
    }
    return $r;
}

method add_param ( Str $key, $value ) {
    if %.params{$key} :exists {
        if %.params{$key} ~~ Str | Int {
            my $old_param = %.params{$key};
            %!params{$key} = [ $old_param, $value ];
        }
        elsif %.params{$key} ~~ Array {
            %!params{$key}.push( $value );
        }
    }
    else {
        %!params{$key} = $value;
    }
}

method param ($key) {
   return %.params{$key};
}
