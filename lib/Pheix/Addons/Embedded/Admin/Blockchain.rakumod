unit class Pheix::Addons::Embedded::Admin::Blockchain;

use Pheix::Model::Database::Access;

has UInt $.txwaisec   = 1;
has UInt $.sealperiod = 5;
has UInt $.sesstime   = 300;
has UInt $.sessseed   = (1_000_000..1_000_000_000).rand.Int;
has      %!gateways   = {asc => 'auth-smart-contract', agw => 'auth-node'};

method auth_on_blockchain(
    Str :$addr!,
    Str :$pwd!,
        :$sharedobj
) returns Hash {
    my %ret =
        status  => False,
        pkey    => q{},
        addr    => '0x' ~ q{0} x 40,
        tx      => '0x' ~ q{0} x 64;

    my $agw = self!configure_auth_gateway;

    if $agw && $agw.dbswitch == 1 && $addr ~~ m:i/^ 0x<xdigit>**40 $/ {
        try {
            $agw.chainobj.ethobj.personal_sign(
                :message($agw.chainobj.ethobj.string2hex(self.^name)),
                :account($addr),
                :password($pwd)
            );
        }

        if $! {
            %ret<message> = $!.message;
        }
        else {
            my Pheix::Model::Database::Access $asc;

            my $catch_details = self!catch_contract(:agw($agw), :sharedobj($sharedobj));

            if $catch_details && $catch_details<scaddr> && $catch_details<txhash> {
                %ret = self.validate_on_blockchain(:token($catch_details<txhash>));
            }

            if !%ret<status> {
                $asc = Pheix::Model::Database::Access.new(
                    :table(%!gateways<asc>),
                    :fields([]),
                    :constrpars({_sealperiod => $!sealperiod, _delta => $!sesstime, _seedmod => $!sessseed}),
                    :txwaitsec($!txwaisec),
                );

                if $asc && $asc.dbswitch == 1 {
                    my %h = $asc.
                            chainobj.
                            ethobj.
                            contract_method_call('getPkey', {});

                    if %h<publickey> && %h<publickey> ~~ m:i/^ 0x<xdigit>**64 $/ {
                        %ret<status>  = True;
                        %ret<addr>    = $addr;
                        %ret<pkey>    = %h<publickey>;
                        %ret<tx>      = $asc.chainobj.sctx;
                        %ret<session> = $!sesstime;
                    }
                }
            }
        }
    }

    return %ret;
}

method validate_on_blockchain(Str :$token!) returns Hash {
    my %validate = status => False;

    if (my $agw = self!configure_auth_gateway(:token($token))) {
        my $upd = self!session_details(:trx($token), :agw($agw));
        my %h   = $agw.chainobj.ethobj.contract_method_call('getPkey', {});

        %h<publickey>:delete if %h<publickey> ~~ m:i/^ 0x<[0]>**64 $/;

        if %h<publickey> && %h<publickey> ~~ m:i/^ 0x<xdigit>**64 $/ && $upd<delta> > 0 {
            %validate =
                status  => True,
                addr    => $agw.chainobj.ethacc,
                expired => $upd<expired>,
                session => $upd<delta>,
                pkey    => %h<publickey>,
                tx      => $token
            ;
        }
    }

    return %validate;
}

method extend_on_blockchain(Str :$token!) returns Hash {
    my %extend = status => False;

    if (my $agw = self!configure_auth_gateway(:token($token))) {
        my %updatetrx = $agw.chainobj.write_blockchain(
            :method('updateState'),
            :waittx(True)
        );

        %updatetrx<txhash> = $token unless %updatetrx<txhash>:exists;

        my $updetails = self!session_details(:trx(%updatetrx<txhash>), :agw($agw));

        my %h = $agw.chainobj.ethobj.contract_method_call('getPkey', {});

        %h<publickey>:delete if %h<publickey> ~~ m:i/^ 0x<[0]>**64 $/;

        if %h<publickey> && %h<publickey> ~~ m:i/^ 0x<xdigit>**64 $/ && $updetails<delta> > 0 {
            %extend =
                status  => True,
                addr    => $agw.chainobj.ethacc,
                expired => $updetails<expired>,
                session => $updetails<delta>,
                pkey    => %h<publickey>,
                tx      => %updatetrx<txhash>
            ;
        }
    }

    return %extend;
}

method close_on_blockchain(Str :$token!) returns Bool {
    my %extend = status => False;

    if (my $agw = self!configure_auth_gateway(:token($token))) {
        my %updatetrx = $agw.chainobj.write_blockchain(
            :method('doExit'),
            :waittx(True)
        );

        return True
            if %updatetrx<txhash>:exists && %updatetrx<txhash> !~~ m:i/^ 0x<[0]>**64 $/;
    }

    return False;
}


method !session_details(
    Str :$trx!,
    Pheix::Model::Database::Access :$agw!
) returns Hash {
    return {} unless $agw && $agw.dbswitch == 1;

    my $ret  = {expired => False};
    my $logs = $agw.chainobj.get_logs(:address($agw.chainobj.scaddr));
    my %trx  = $agw.chainobj.ethobj.eth_getTransactionReceipt($trx);

    if $logs && $logs.elems > 0 {
        if (($logs.head)<transactionHash>:exists) && (%trx<blockNumber>:exists) {
            my $gentrx  = $agw.chainobj.ethobj.eth_getTransactionReceipt(($logs.head)<transactionHash>);

            $ret<genbn> = $gentrx<blockNumber>.Int;
            $ret<trxbn> = %trx<blockNumber>.Int;
            $ret<block> = $ret<trxbn> > $ret<genbn> ?? $ret<trxbn> !! $ret<genbn>;

            my $prevblck  = $agw.chainobj.ethobj.eth_getBlockByNumber($ret<block>);
            my $lastblck = $agw.chainobj.ethobj.eth_getBlockByNumber('latest');

            $ret<delta> = $!sesstime - ($lastblck<timestamp>.Int - $prevblck<timestamp>.Int);

            if $ret<delta> > 0 && $ret<delta> <= 20 {
                $ret<expired> = True;
            }
        }
    }

    return $ret;
}

method !configure_auth_gateway(Str :$token) returns Pheix::Model::Database::Access {
    my $agw = Pheix::Model::Database::Access.new(
        :table(%!gateways<agw>),
        :fields([]),
        :txwaitsec($!txwaisec),
    );

    return unless $agw && $agw.dbswitch == 1;

    $agw.chainobj.evsign = 'PheixAuthCode(uint256)';

    if $token && $token ~~ m:i/^ 0x<xdigit>**64 $/ {
        my %trx = $agw.chainobj.ethobj.eth_getTransactionReceipt($token);

        return unless %trx<from> ~~ m:i/^ 0x<xdigit>**40 $/;

        $agw.chainobj.ethacc = %trx<from>;

        my Str $scadr;

        if %trx<to> && %trx<to> ~~ m:i/^ 0x<xdigit>**40 $/ {
            $scadr = %trx<to>;
        }
        elsif %trx<contractAddress> && %trx<contractAddress> ~~ m:i/^ 0x<xdigit>**40 $/ {
            $scadr = %trx<contractAddress>;
        }
        else {
            $scadr = '0x' ~ q{0} x 40;
        }

        return unless $scadr !~~ m:i/^ 0x<[0]>**40 $/;

        $agw.chainobj.scaddr = $scadr;
        $agw.chainobj.ethobj.contract_id = $scadr;
    }

    return $agw;
}

method !catch_contract(
    Pheix::Model::Database::Access :$agw!,
                                   :$sharedobj
) returns Hash {
    my $ret = {};
    my $logs;

    return $ret unless $agw && $agw.dbswitch == 1;

    my $lastblock  = $agw.chainobj.ethobj.eth_getBlockByNumber('latest');

    my $startblock = (($lastblock<number> // 0) - $!sesstime/$!sealperiod).Int;

    $startblock = 0 unless $startblock > 0;

    $logs = $agw.chainobj.get_logs(:from($startblock), :topics([sprintf("0x%064x", 2)]));
    $logs = $agw.chainobj.get_logs(:from($startblock), :topics([sprintf("0x%064x", 1)])) unless $logs && $logs.elems > 0;

    $sharedobj<dbugobj>.log(
        :entry(sprintf("catch contract feature on logon for %s: %s", $agw.chainobj.ethacc, $logs.gist)),
        :sharedobj($sharedobj)
    );

    if $logs && $logs.elems > 0 {
        my $latestlog = $logs.tail;

        if ($latestlog<transactionHash>:exists) &&
           ($latestlog<blockNumber>:exists) &&
           ($latestlog<address>:exists) {
            $ret = {
                scaddr => $latestlog<address>,
                txhash => $latestlog<transactionHash>
            };

        }
    }

    return $ret;
}
