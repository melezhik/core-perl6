unit class Pheix::Addons::Embedded::Admin;

use JSON::Fast;

use Pheix::Addons::Embedded::Admin::Blockchain;
use Pheix::Controller::API;
use Pheix::Model::JSON;
use Pheix::View::Web::Cookie;
use Pheix::Controller::Stats;

has $.ctrl;

has Str                $.confpath    = 'conf/addons';
has Str                $!ascookie    = 'pheixauth';
has Str                $!name        = 'EmbeddedAdmin';
has Str                $!tabname     = $!name.lc ~ '/login';
has Pheix::Model::JSON $.jsonobj     = self.init_json_config(:path($!confpath));
has Str                $.sesstimeout = ~$!jsonobj.get_conf_value($!name, 'sesstimeout');
has UInt               $.sealperiod  = +$!jsonobj.get_conf_value($!name, 'sealperiod');

has Pheix::View::Web::Cookie $!cookhelper = Pheix::View::Web::Cookie.new;
has Pheix::Model::JSON       $!jobjsys    = Pheix::Model::JSON.new.set_entire_config(:addon('Pheix'));
has Pheix::Controller::API   $!builtinapi = Pheix::Controller::API.new;
has Pheix::Controller::Stats $!statsobj   = Pheix::Controller::Stats.new;

has Pheix::Addons::Embedded::Admin::Blockchain $!authnode =
    Pheix::Addons::Embedded::Admin::Blockchain.new(
        :sesstime(+$!cookhelper.expire_calc($!sesstimeout)),
        :sealperiod($!sealperiod)
    );

method init_json_config(Str :$path) returns Pheix::Model::JSON {
    return Pheix::Model::JSON.new(:addonpath($path)).set_entire_config(:addon($!name));
}

method get(:$ctrl!) returns Pheix::Addons::Embedded::Admin {
    $!ctrl = $ctrl.clone;

    return self;
}

method get_class returns Str { return self.^name; }

method get_name returns Str { return $!name; }

method get_sm returns List { return List.new; }

method fill_seodata(:%match!) returns Hash {
    my %params;
    my %h =
        d => <title metadescr metakeywords header>,
        t => 'indexseotags',
        v => 'value';

    for @(%h<d>) -> $k {
        %params{$k} =
            $!jsonobj.get_group_setting($!name, %h<t>, $k, %h<v>) //
                q{};
    }

    return %params;
}

method get_render_data(:%match!) returns Hash {
    my %ret = table => %match<table> && %match<table> ne q{} ??
        %match<table> !!
            $!tabname;

    if %match<details><validate>:exists {
        for %match<details><validate>.kv -> $k, $v {
            %ret{$k} = $v if $v ne q{};
        }
    }

    return %ret;
}

method browse(UInt :$tick, :%match!) {
    my %m = %match;

    %m<details> = {path => '/api' ~ %match<details><path>};

    $!ctrl.index(
        :tick($tick),
        :match(%m),
        :addon($!name),
        :admin(True),
        :cookies([
            'fcgitick=' ~ $tick,
            'pheixaddon=' ~ $!name,
            'pheixadmin=' ~ True.Str
        ]),
    );
}

method browse_api(
    :%match!,
    Str  :$route,
    UInt :$tick!,
    Hash :$sharedobj!,
    Hash :$credentials = {},
    Hash :$payload = {},
    Hash :$header = {},
) returns Hash {
    my %rc;
    my $start = now;

    my %t = self.get_render_data(:match(%match));

    my Str $component_raw = $sharedobj<pageobj>.raw_pg(:table(%t<table>));

    if $component_raw {
        %t{$!ascookie} =
            self!get_token_from_cookies(:envcookies($sharedobj<fastcgi>.env<HTTP_COOKIE> // q{}));

        if %match<details> && (%match<details><validate>:exists) {
            my %validate = %match<details><validate>;

            if (%validate<status>:exists) && %validate<status> {
                %t<pheixlogs> = $!ctrl.sharedobj<logrobj>.get_all.reverse.map({
                    %(
                        date => sprintf(
                            "%s, %s",
                            DateTime.new($_<id>.UInt).yyyy-mm-dd,
                            DateTime.new($_<id>.UInt).hh-mm-ss
                        ),
                        dump => $_<log>
                    )
                });

                %t<stats> = $!statsobj.get_traffic;

                for $!ctrl.addons.values -> $addon {
                    if $addon<exten> {
                        my $addonobj = $addon<objct>.get(:ctrl($!ctrl));

                        if $addonobj.^find_method('extention_api_content').defined {
                            push(%t<extensions>, $addonobj.extention_api_content);
                        }
                    }
                }
            }
        }

        %t<tmpl_sesstoken> = $sharedobj<pageobj>.sesstoken;

        %rc =
            component => $sharedobj<mb64obj>.encode-str(
                $component_raw,
                :oneline
            ),
            tparams => %t,
        ;
    }
    else {
        %rc = self.error(
            :tick($tick),
            :match(%match),
            :code('400'),
            :message(sprintf("raw component for %s tab is missed", %t<table>)),
            :sharedobj($sharedobj)
        );
    }

    %rc<component_render> = (now - $start).Str;
    %rc<header> = $header if $header && $header.keys.elems;

    return %rc;
}

method auth_api(
         :%match!,
    Str  :$route,
    UInt :$tick!,
    Hash :$sharedobj!,
    Hash :$credentials = {},
    Hash :$payload = {},
    Hash :$header = {},
) returns Hash {
    my %m = %match;

    my %rc;

    my Str $ethaddr = $credentials<login> || q{};
    my Str $passwrd = $credentials<password> || q{};

    my %validate =
        $!authnode.auth_on_blockchain(:addr($ethaddr), :pwd($passwrd), :sharedobj($sharedobj));

    if (%validate<status>:exists) && %validate<status> {
        %m<table>   = $!name.lc ~ '/area';
        %m<details> = {validate => %validate};

        if $header && $header.keys && ($header<X-Request-ID>:exists) {
            $header<Set-Cookie> = [
                Pheix::View::Web::Cookie
                    .new(:name($!ascookie), :value(%validate<tx>), :expires_s($!sesstimeout)).cookie(),
                Pheix::View::Web::Cookie
                    .new(:name('pheixsender'), :value(%validate<addr>), :expires_s($!sesstimeout)).cookie()
            ];
        }
    }

    %rc = self.browse_api(
        :match(%m),
        :route($route),
        :tick($tick),
        :sharedobj($sharedobj),
        :credentials($credentials),
        :payload($payload),
        :header($header)
    );

    %rc<message> = %validate<message>
        if %validate && (%validate<message>:exists);

    return %rc;
}

method manage_session_api(
         :%match!,
    Str  :$route,
    UInt :$tick!,
    Hash :$sharedobj!,
    Hash :$credentials = {},
    Hash :$payload = {},
    Hash :$header = {},
) returns Hash {
    my %response =
        sesstatus => False,
        tryextend => False,
        tparams => {
            session => 0
        };

    my Str $token = self!get_token_from_cookies(:envcookies($sharedobj<fastcgi>.env<HTTP_COOKIE> // q{}));

    if $token && $token ~~ m:i/^ 0x<xdigit>**64 $/ {
        my %blockchain;

        if $route ~~ /validate/ || $route ~~ /refresh/ {
            %blockchain = $!authnode.validate_on_blockchain(:token($token));
        }
        elsif $route ~~ /extend/ {
            %blockchain = $!authnode.extend_on_blockchain(:token($token));
        }
        else {
            my Bool $rc = $!authnode.close_on_blockchain(:token($token));

            if $rc {
                %response = self.browse_api(
                    :match({t => $!tabname}),
                    :route($route),
                    :tick($tick),
                    :sharedobj($sharedobj)
                );
            }
            else {
                %response = self.error(
                    :tick($tick),
                    :match({}),
                    :code('400'),
                    :message('close session failure'),
                    :sharedobj($sharedobj)
                );
            }
        }

        if (%blockchain<status>:exists) && %blockchain<status> {
            my %tparams =
                session    => %blockchain<session>,
                addr       => %blockchain<addr>,
                pkey       => %blockchain<pkey>,
                tx         => %blockchain<tx>,
                $!ascookie => $token,
            ;

            if $route ~~ /refresh/ {
                my %refresh_response = self.browse_api(
                    :match({table => $!name.lc ~ '/area', details => {validate => %blockchain}}),
                    :route($route),
                    :tick($tick),
                    :sharedobj($sharedobj)
                );

                %response<component> = %refresh_response<component> if
                    %refresh_response && (%refresh_response<component>:exists);

                %tparams = %refresh_response<tparams> if
                    %refresh_response && (%refresh_response<tparams>:exists);
            }

            %response<sesstatus> = %blockchain<status>;
            %response<tryextend> = %blockchain<expired>;
            %response<tparams>   = %tparams;

            if $header && $header.keys && ($header<X-Request-ID>:exists) && $token ne %blockchain<tx> {
                $header<Set-Cookie> = [
                    Pheix::View::Web::Cookie.new(:name($!ascookie), :value(%blockchain<tx>), :expires_s($!sesstimeout)).cookie(),
                    Pheix::View::Web::Cookie.new(:name('pheixsender'), :value(%blockchain<addr>), :expires_s($!sesstimeout)).cookie()
                ];

                %response<header> = $header;
            }
        }
    }

    return %response;
}

method error(
         :%match!,
    UInt :$tick!,
    Hash :$sharedobj!,
    Str  :$code,
    Str  :$message
) returns Hash {
    $!builtinapi.error(
        :route(%match<details><path>),
        :code($code // '404'),
        :message($message // q{}),
        :tick($tick),
        :sharedobj($sharedobj),
    );
}

method !get_token_from_cookies(Str :$envcookies!) returns Str {
    return q{} unless $envcookies && $envcookies ne q{};

    my %cookies;

    $envcookies.split(/<[;\s]>+/, :skip-empty).map(
        {
            my @c = $_.split(q{=}, :skip-empty);
            %cookies{@c[0]} = @c[1];
        }
    );

    if %cookies && %cookies.keys && (%cookies{$!ascookie}:exists) {
        return %cookies{$!ascookie};
    }

    return q{};
}
