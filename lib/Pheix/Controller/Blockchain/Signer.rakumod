unit class Pheix::Controller::Blockchain::Signer;

use Pheix::Model::Database::Access;

has Bool $.test;
has UInt $.latestgas is default(0);
has Pheix::Model::Database::Access $.signerobj
    is default(Pheix::Model::Database::Access) is rw;
has Pheix::Model::Database::Access $.targetobj
    is default(Pheix::Model::Database::Access) is rw;

submethod BUILD(
    Str  :$signtab!,
    Bool :$test = False,
    Pheix::Model::Database::Access :$target!
) returns Pheix::Controller::Blockchain::Signer {
    $!test      = $test;
    $!targetobj = $target;
    $!signerobj = Pheix::Model::Database::Access.new(
        :table($signtab),
        :fields([]),
        :test($test)
    );

    return self;
}

method selfcheck returns Bool {
    return $!targetobj.dbswitch == 1 && $!signerobj.dbswitch == 1 ??
        True !!
            False;
}

method sign(Str :$marshalledtx!, Str :$blocktag) returns Str {
    my %sign;
    my UInt $gasqty = 0;

    my UInt $gp = $!targetobj.chainobj.ethobj.eth_gasPrice;

    my UInt $nc = $!targetobj.chainobj.ethobj.eth_getTransactionCount(
        :data($!targetobj.chainobj.ethacc),
        :tag($blocktag || 'pending')
    );

    $!signerobj.chainobj.ethobj.personal_unlockAccount(
        :account($!targetobj.chainobj.ethacc),
        :password($!targetobj.chainobj.ethobj.unlockpwd),
    );

    try {
        $gasqty = $!signerobj.chainobj.ethobj.eth_estimateGas({
            from => $!targetobj.chainobj.ethacc,
            to   => $!targetobj.chainobj.scaddr,
            data => $marshalledtx
        });

        CATCH {
            default {
                say 'Pheix::Controller::Blockchain::Signer estimate gas failure ' ~ .message;
            }
        };
    }

    if $gasqty > 0 {
        $!latestgas = $gasqty + $!signerobj.chainobj.gas_update(:gas($gasqty));

        # sign the transaction
        %sign = $!signerobj.chainobj.ethobj.eth_signTransaction(
            :from($!targetobj.chainobj.ethacc),
            :to($!targetobj.chainobj.scaddr),
            :gas($!latestgas),
            :gasprice($gp),
            :nonce($nc),
            :data($marshalledtx)
        );
    }

    return (%sign<raw>:exists && %sign<raw> ~~ m:i/^ 0x<xdigit>+ $/) ??
        %sign<raw> !!
            Str;
}

method send(Str :$rawtx!) returns Str {
    return $!targetobj.chainobj.ethobj.eth_sendRawTransaction(:data($rawtx));
}
