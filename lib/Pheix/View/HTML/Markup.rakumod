unit class Pheix::View::HTML::Markup;

has $.br = '<br>';

method uni_tag(
    Str $tag!,
    Cool $class,
    Str $content,
    Str :$href,
    Str :$each,
    Str :$if
) returns Str {
    my $c = $class ?? sprintf(" class=\"%s\"", $class) !! q{};
    my $h = $href ?? sprintf(" href=\"%s\"", $href) !! q{};
    my $e = $each ?? sprintf(" each=\"%s\"", $each) !! q{};
    my $i = $if ?? sprintf(" if=\"%s\"", $if) !! q{};

    return sprintf("\<%s%s%s%s%s\>%s\</%s\>", $tag, $c, $h, $e, $i, $content, $tag);
}
