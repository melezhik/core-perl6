unit class Pheix::View::Template;

use HTML::Template;

has Str $.uitempl is default('<TMPL_VAR tmpl_debug>') is rw;
has Str $.uitempl_indx is default('./conf/config/index.html');
has HTML::Template $!tobj is default(HTML::Template.new);

has $!indexparsed = $!uitempl_indx && $!uitempl_indx.IO.e ??
        $!tobj.from_string($!uitempl_indx.IO.slurp).parse !!
            Nil;

method render(Str :$type, :%vars) returns Str {
    my Str $rs;
    my $parsed;

    given $type {
        when 'debug' {
            if %vars.keys {
                $parsed = $!tobj.from_string($!uitempl // q{}).parse // Nil;
            }
            else {
                $rs = $!uitempl;
            }
        }
        default {
            $parsed = $!indexparsed;
        }
    }

    if $parsed {
        $rs = $!tobj.substitute($parsed, %vars);
    }

    $rs // q{};
}

method reset_uitmpl(Str :$to) returns Pheix::View::Template {
    if $to {
        $!uitempl = $to;
    }
    else {
        $!uitempl = '<TMPL_VAR tmpl_debug>';
    }

    self;
}
