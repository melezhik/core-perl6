unit class Pheix::View::Pages;

use P5quotemeta;
use MIME::Base64;
use Pheix::Datepack;
use Pheix::View::Template;
use Pheix::View::Web::Headers;
use Pheix::View::HTML::Markup;
use Pheix::View::Debug;
use Pheix::Model::Database::Compression;
use Pheix::Model::Resources;
use Pheix::Model::Version;
use Pheix::Model::JSON;
use Pheix::Utils;
use Pheix::Model::Database::Access;
use Pheix::Model::Database::Filechain;

has Pheix::Utils $.utilobj;
has Pheix::View::Template $.tobj;
has Pheix::View::Web::Headers $.headobj;
has Pheix::Model::JSON $.jsonobj;
has Pheix::View::HTML::Markup $.mkupobj;
has Mu $.rsrcobj;
has %.addons;

has Bool $.test     = False;
has Str $.indxcnt   = 'index';
has Str $.httperr   = 'http-error';
has Str $.sesstoken = $!utilobj.get_token_by_now;

has $.fchnobj = Pheix::Model::Database::Filechain.new;
has $!dateobj = Pheix::Datepack.new(:date(DateTime.now), :unixtime(time));
has $!dbugobj = Pheix::View::Debug.new;
has $!captcha = sprintf "%010d", (rand * 10000000000);
has $!mb64    = MIME::Base64.new;
has $!cmpobj  =
    Pheix::Model::Database::Compression.new(:dictsize($!utilobj.lzwsz));

has %!cparams =
    tmpl_modeclass   => q{}
;

has %!pparams =
    tmpl_contentcols => '8',
    tmpl_forcehide   => q{},
    tmpl_pagetitle   => q{},
    tmpl_metadesc    => q{},
    tmpl_metakeys    => q{},
    tmpl_content     => q{},
    tmpl_timestamp   => $!dateobj.hex_unixtime,
    tmpl_update_year => $!dateobj.year_update,
    tmpl_version     => Pheix::Model::Version.get_version,
    tmpl_scriptname  =>
        %*ENV<SCRIPT_NAME> || $*PROGRAM-NAME.IO.basename,
    tmpl_srvname     =>
        %*ENV<SERVER_NAME> ||
        $!jsonobj.get_setting('Pheix', 'servername', 'value'),
    tmpl_bigbrojs    =>
        $!jsonobj.get_setting('Pheix', 'bigbro_user', 'value') ??
            Pheix::View::HTML::Markup.uni_tag(
                'script',
                Nil,
                "doBigBroLook('" ~ (%*ENV<REQUEST_URI> || q{/}) ~ "')"
            ) !! q{},
;

has %!tparams =
    tmpl_sesstoken   => $!sesstoken,
    tmpl_decaptcha   => $!captcha,
    tmpl_encaptcha   => $!utilobj.do_encrypt($!captcha),
    tmpl_update_full => $!dateobj.format_update,
    tmpl_modname     => $?CLASS.^name,
    tmpl_pageheader  => q{},
    tmpl_debug       => q{},
    tmpl_extrametas  => q{},
    tmpl_modcode     => q{},
    tmpl_paragraph   => q{},
    tmpl_rakudo_ver  => q{},
    tmpl_env_vars    => q{},
;

method get_cparams returns Hash {
    return %!cparams;
}

method set_cparams(Str $k, Str $v) returns Bool {
    my Bool $ret = False;

    if %!cparams{$k}:exists {
        %!cparams{$k} = $v;
        $ret = True;
    }

    $ret;
}

method get_tparams returns Hash {
    return %!tparams;
}

method set_tparams(Str $k, Str $v) {
    if %!tparams{$k}:exists {
        %!tparams{$k} = $v;
    }
}

method get_pparams returns Hash {
    return %!pparams;
}

method set_pparams(Str $k, Str $v) {
    if %!pparams{$k}:exists {
        %!pparams{$k} = $v;
    }
}

method lazy_load(:%conf!) returns Str {
    my Str $jsloader = sprintf(
        "loadAPI_v2('%s', %s, '%s', '%s', '%s', '%s', null, false, null)",
        %conf<instance>,
        $!jsonobj.as-json(:data(%conf<credentials>)),
        %conf<method>,
        %conf<route>,
        %conf<httpstat> // 200,
        quotemeta(%conf<message> // q{})
    );

    $jsloader;
}

method raw_pg(
    Str  :$table!,
         :@fields = [],
    Bool :$test   = False,
    Pheix::Model::Database::Access :$database
) returns Str {
    my Str $pg = q{};

    my Pheix::Model::Database::Access $dbobj = $database //
        Pheix::Model::Database::Access.new(
            :table($table),
            :fields(@fields),
            :test($test)
        );

    if $dbobj {
        my @cols = $dbobj.fields;

        $pg = $dbobj.get_all(:fast(True))
            .map({
                ($_{@cols[2]} // q{0}).Int == 0 ?? $_{@cols[1]} // q{} !!
                    $dbobj.dbswitch == 1 ?? $_{@cols[1]} !!
                        $!cmpobj.decompress(
                            :data($!mb64.decode-str($_{@cols[1]}))
                        )
            })
            .join(q{}) // q{};

        if $dbobj.dbswitch == 1 && $dbobj.chainobj.is_tab_compressed {
            $pg = $!cmpobj.decompress(:data($pg));
        }
    }

    $pg;
}

method show_pg(
    Str :$pg_addon,
    Str :$pg_type!,
    Str :$pg_route!,
    Str :$pg_content,
        :%pg_env,
        :%pg_match
) returns Str {
    my Str $ret = q{};

    # $!utilobj.add_profile_data(:msg('enter show_pg method'), :initial(0));

    self.cookie_dependent(:env(%pg_env));
    self.fill_seodata($pg_type, $pg_addon, :pg_match(%pg_match));

    given $pg_type {
        when $_ ~~ / '40' <[0..5]> ** 1..1 / {
            my %c =
                method      => 'GET',
                credentials => { token => $!sesstoken },
                route       => $pg_route,
                instance    => 'page',
                httpstat    => $_,
                message     => $pg_content // q{}
            ;

            self.set_pparams('tmpl_content', self.lazy_load(:conf(%c)));

            $ret = $!tobj.render(:type(Str), :vars(%(%!pparams, %!cparams)));
        }
        when 'debug' {
            $ret = $!tobj.render(:type('debug'), :vars({tmpl_debug => $pg_content}));
        }
        default {
            my %c =
                method      => 'GET',
                credentials => { token => $!sesstoken },
                route       => $pg_route,
                instance    => 'page'
            ;

            self.set_pparams('tmpl_content', self.lazy_load(:conf(%c)));

            $ret = $!tobj.render(:type(Str), :vars(%(%!pparams, %!cparams)));
        }
    }

    # $!utilobj.add_profile_data(:msg('finish show_pg method'));
    # $!utilobj.print_profile_stats;

    $ret;
}

method cookie_dependent(:%env) returns Bool {
    my Bool $ret = False;
    my %defaults =
        tmpl_modeclass => 'daymode'
    ;

    for %!cparams.keys -> $cookie {
        if (
            %env<HTTP_COOKIE>:exists &&
            %env<HTTP_COOKIE> ~~ / $cookie '=' (<[a..z]>+) /
        ) {
            self.set_cparams($cookie, $/[0].Str);
            $ret = True;
        }
        else {
            self.set_cparams($cookie, %defaults{$cookie});
        }
    }

    $ret;
}

method fill_seodata(Str $ptype, Str $paddon, :%pg_match) {
    my %params;
    my %h = d => <title metadescr metakeywords header>, v => 'value';

    given $ptype {
        when 'debug' {
            ;
        }
        when '404' {
            %h.append({
                m => 'Pheix',
                t => '404seotags'
            });
        }
        default {
            %h.append({
                m => $paddon || 'Pheix',
                t => $ptype ~ 'seotags'
            });
        }
    }

    if (%h<m>:exists) {
        if %h<m> eq 'Pheix' {
            for @(%h<d>) -> $k {
                %params{$k} =
                    $!jsonobj.get_group_setting(%h<m>, %h<t>, $k, %h<v>) //
                        q{};
            }
        }
        else {
            my Str $addon;

            %params =
                title => q{},
                metadescr => q{},
                metakeywords => q{},
                header => q{}
            ;

            for %!addons.kv -> $class, %a {
                if %a<aname> eq %h<m> {
                    $addon = $class;
                    last;
                }
            }

            if $addon && (%!addons{$addon}:exists) {
                my $addonobj = %!addons{$addon}<objct>;
                if $addonobj.^find_method('fill_seodata').defined {
                    %params = $addonobj.fill_seodata(:match(%pg_match));
                }
            }
        }

        self.set_pparams('tmpl_pagetitle', %params<title>);
        self.set_pparams('tmpl_metadesc', %params<metadescr>);
        self.set_pparams('tmpl_metakeys', %params<metakeywords>);
        self.set_tparams('tmpl_pageheader', %params<header>);
    }
}

method show_rtm(Bool :$s, Instant :$time) returns Str {
    my Str $ret;
    my Instant $start = $time.defined ?? $time !! INIT now;

    if $s {
        $ret  = '<script type="text/javascript">';
        $ret ~= '$("#pheix_render_time").text("' ~ (now - $start);
        $ret ~= '");</script>';
    } else {
        my Str $rmsg =
            $!rsrcobj.render_mess_prefix ~ (now - $start) ~
            $!rsrcobj.render_mess_postfix;
        $ret = $!mkupobj.uni_tag(
            'p',
            '_phx-cntr _phx-ccc _phx-fnt10',
            $rmsg,
        );
    }

    $ret;
}

method show_sm returns Str {
    my $urltmpl = '<url><loc>%loc%</loc><lastmod>%mod%</lastmod></url>';
    my @sitemap = ('<?xml version="1.0" encoding="UTF-8"?>',
        '<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">',
        $urltmpl,
        '</urlset>',
    );

    my $dtobj = DateTime.new($!fchnobj.get_path(:tab($!indxcnt)).IO.modified || now );
    my $root  = $!headobj.proto_sn;
    my $rtup  = sprintf(
        "%04d-%02d-%02d",
        $dtobj.year,
        $dtobj.month,
        $dtobj.day
    );

    @sitemap[1] ~~ s:g/ <[\s]> ** 4..4 //;
    @sitemap[2] ~~ s:g/\%loc\%/$root/;
    @sitemap[2] ~~ s:g/\%mod\%/$rtup/;
    for %!addons.keys -> $member {
        next unless %!addons{$member}<objct>:exists;

        my $smdata;
        my @addon_sm = %!addons{$member}<objct>.get_sm;

        for @addon_sm -> %h {
            if %h {
                my $ut  = $urltmpl;
                my $url = $root ~ q{/} ~ %h<loc>;
                $ut ~~ s:g/\%loc\%/$url/;
                $ut ~~ s:g/\%mod\%/%h<lastmod>/;
                $smdata ~= $ut;
            }
        }
        if $smdata {
            @sitemap[2] ~= $smdata;
        }
    }

    @sitemap.join("\n");
}
