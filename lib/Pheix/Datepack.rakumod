unit class Pheix::Datepack;

use Pheix::Utils;
use Pheix::Model::Resources;

has DateTime $.date;
has UInt     $.unixtime;

method format_update returns Str {
    my Str $ret;
    if $!date {
        my $_robj = Pheix::Model::Resources.new.init;
        $ret =
            $!date.day ~ q{ } ~
            $_robj.months{$!date.month} ~
            ', ' ~ $!date.year;
    }
    $ret;
}

method year_update returns Str {
    my Str $ret;
    if $!date {
        $ret = $!date.year.Str;
    }
    $ret;
}

method hex_unixtime returns Str {
    my Str $ret;
    if $!unixtime && ( $!unixtime > 0 ) {
        $ret = Pheix::Utils.new.get_hex($!unixtime.Str);
    }
    $ret;
}

method get_http_response_date returns Str {
    my Str $ret;
    if $!unixtime and $!date {
        my $_robj = Pheix::Model::Resources.new.init;
        my $_dobj = DateTime.new($!unixtime);
        $ret = sprintf(
            "%s, %02d %s %04d %02d:%02d:%02d GMT",
            $_robj.weekd_reduced{$!date.day-of-week},
            $!date.day,
            $_robj.months_reduced{$!date.month},
            $!date.year,
            $_dobj.hour,
            $_dobj.minute,
            $_dobj.second,
        );
    }
    $ret;
}

method get_date_filename(Str :$prefix, Str :$extension) returns Str {
    my Str $ret;

    my $dobj = $!date // DateTime.new($!unixtime // now);
    my $robj = Pheix::Model::Resources.new.init;

    sprintf(
        "%s-%s-%02d-%s-%04d-%02d-%02d-%02d.%s",
        $prefix || 'dump',
        $robj.weekd_reduced{$dobj.day-of-week},
        $dobj.day,
        $robj.months_reduced{$dobj.month},
        $dobj.year,
        $dobj.hour,
        $dobj.minute,
        $dobj.second,
        $extension || 'log'
    );
}
