use v6.d;
use Test;
use lib 'lib';

plan 2;

use Pheix::Model::Resources;

subtest {
    plan 46;
    my Mu $obj =
        Pheix::Model::Resources.new(test => True, locale => 'En').init;
    is(
        $obj.parent,
        'Pheix::Model::Resources::En',
        'parent attr is ' ~ $obj.parent,
    );
    is(
        $obj.locale,
        'En',
        'locale attr is ' ~ $obj.locale,
    );
    is(
        $obj.render_mess_prefix,
        'render time: ',
        'render_mess_prefix()',
    );
    is(
        $obj.render_mess_postfix,
        ' seconds',
        'render_mess_postfix()',
    );
    is( $obj.welcome, 'Welcome to Pheix!', 'welcome' );
    is(
        $obj.helloworld,
        'Hello, World! It&apos;s works ;)',
        'helloworld',
    );
    is(
        $obj.error,
        'Something went wrong :(',
        'error',
    );
    is(
        $obj.vocabulary<error>,
        'Error',
        'vocabulary<error>',
    );
    is(
        $obj.vocabulary<usrcnterror>,
        'User content is missed',
        'vocabulary<usrcnterror>',
    );
    is( $obj.months{1},          'January',   'months{1}' );
    is( $obj.months{2},          'February',  'months{2}' );
    is( $obj.months{3},          'March',     'months{3}' );
    is( $obj.months{4},          'April',     'months{4}' );
    is( $obj.months{5},          'May',       'months{5}' );
    is( $obj.months{6},          'June',      'months{6}' );
    is( $obj.months{7},          'July',      'months{7}' );
    is( $obj.months{8},          'August',    'months{8}' );
    is( $obj.months{9},          'September', 'months{9}' );
    is( $obj.months{10},         'October',   'months{10}' );
    is( $obj.months{11},         'November',  'months{11}' );
    is( $obj.months{12},         'December',  'months{12}' );
    is( $obj.months_reduced{1},  'Jan', 'months_reduced{1}' );
    is( $obj.months_reduced{2},  'Feb', 'months_reduced{2}' );
    is( $obj.months_reduced{3},  'Mar', 'months_reduced{3}' );
    is( $obj.months_reduced{4},  'Apr', 'months_reduced{4}' );
    is( $obj.months_reduced{5},  'May', 'months_reduced{5}' );
    is( $obj.months_reduced{6},  'Jun', 'months_reduced{6}' );
    is( $obj.months_reduced{7},  'Jul', 'months_reduced{7}' );
    is( $obj.months_reduced{8},  'Aug', 'months_reduced{8}' );
    is( $obj.months_reduced{9},  'Sep', 'months_reduced{9}' );
    is( $obj.months_reduced{10}, 'Oct', 'months_reduced{10}' );
    is( $obj.months_reduced{11}, 'Nov', 'months_reduced{11}' );
    is( $obj.months_reduced{12}, 'Dec', 'months_reduced{12}' );
    is( $obj.weekd_reduced{1},   'Mon', 'weekd_reduced{6}' );
    is( $obj.weekd_reduced{2},   'Tue', 'weekd_reduced{7}' );
    is( $obj.weekd_reduced{3},   'Wed', 'weekd_reduced{8}' );
    is( $obj.weekd_reduced{4},   'Thu', 'weekd_reduced{9}' );
    is( $obj.weekd_reduced{5},   'Fri', 'weekd_reduced{10}' );
    is( $obj.weekd_reduced{6},   'Sat', 'weekd_reduced{11}' );
    is( $obj.weekd_reduced{7},   'Sun', 'weekd_reduced{12}' );
    is( $obj.http_codes<400>,    'Bad Request',        'http_codes<400>' );
    is( $obj.http_codes<401>,    'Unauthorized',       'http_codes<401>' );
    is( $obj.http_codes<402>,    'Payment Required',   'http_codes<402>' );
    is( $obj.http_codes<403>,    'Forbidden',          'http_codes<403>' );
    is( $obj.http_codes<404>,    'Not Found',          'http_codes<404>' );
    is( $obj.http_codes<405>,    'Method Not Allowed', 'http_codes<405>' );
}, 'Check attributes from EN locale';

subtest {
    plan 46;
    my Mu $obj =
        Pheix::Model::Resources.new(test => True, locale => 'Ru').init;
    is(
        $obj.parent,
        'Pheix::Model::Resources::Ru',
        'parent attr is ' ~ $obj.parent,
    );
    is(
        $obj.locale,
        'Ru',
        'locale attr is ' ~ $obj.locale,
    );
    is(
        $obj.render_mess_prefix,
        'время отрисовки: ',
        'render_mess_prefix()',
    );
    is(
        $obj.render_mess_postfix,
        ' сек.',
        'render_mess_postfix()',
    );
    is( $obj.welcome, 'Pheix приветсвует Вас!', 'welcome' );
    is(
        $obj.helloworld,
        'Здравствуй, Мир! Кажется я работаю ;)',
        'helloworld',
    );
    is(
        $obj.error,
        'Что-то пошло не так :(',
        'error',
    );
    is(
        $obj.vocabulary<error>,
        'Ошибка',
        'vocabulary<error>',
    );
    is(
        $obj.vocabulary<usrcnterror>,
        'Пользовательский контент пуст',
        'vocabulary<usrcnterror>',
    );
    is( $obj.months{1},           'Январь',   'months{1}'  );
    is( $obj.months{2},           'Февраль',  'months{2}'  );
    is( $obj.months{3},           'Март',     'months{3}'  );
    is( $obj.months{4},           'Апрель',   'months{4}'  );
    is( $obj.months{5},           'Май',      'months{5}'  );
    is( $obj.months{6},           'Июнь',     'months{6}'  );
    is( $obj.months{7},           'Июль',     'months{7}'  );
    is( $obj.months{8},           'Август',   'months{8}'  );
    is( $obj.months{9},           'Сентябрь', 'months{9}'  );
    is( $obj.months{10},          'Октябрь',  'months{10}' );
    is( $obj.months{11},          'Ноябрь',   'months{11}' );
    is( $obj.months{12},          'Декабрь',  'months{12}' );
    is( $obj.months_reduced{1},   'Янв',      'months_reduced{1}'  );
    is( $obj.months_reduced{2},   'Фев',      'months_reduced{2}'  );
    is( $obj.months_reduced{3},   'Мар',      'months_reduced{3}'  );
    is( $obj.months_reduced{4},   'Апр',      'months_reduced{4}'  );
    is( $obj.months_reduced{5},   'Май',      'months_reduced{5}'  );
    is( $obj.months_reduced{6},   'Июн',      'months_reduced{6}'  );
    is( $obj.months_reduced{7},   'Июл',      'months_reduced{7}'  );
    is( $obj.months_reduced{8},   'Авг',      'months_reduced{8}'  );
    is( $obj.months_reduced{9},   'Сен',      'months_reduced{9}'  );
    is( $obj.months_reduced{10},  'Окт',      'months_reduced{10}' );
    is( $obj.months_reduced{11},  'Ноя',      'months_reduced{11}' );
    is( $obj.months_reduced{12},  'Дек',      'months_reduced{12}' );
    is( $obj.weekd_reduced{1},    'Пн',       'weekd_reduced{1}'  );
    is( $obj.weekd_reduced{2},    'Вт',       'weekd_reduced{2}'  );
    is( $obj.weekd_reduced{3},    'Ср',       'weekd_reduced{3}'  );
    is( $obj.weekd_reduced{4},    'Чт',       'weekd_reduced{4}'  );
    is( $obj.weekd_reduced{5},    'Пт',       'weekd_reduced{5}'  );
    is( $obj.weekd_reduced{6},    'Сб',       'weekd_reduced{6}'  );
    is( $obj.weekd_reduced{7},    'Вс',       'weekd_reduced{7}'  );
    is( $obj.http_codes<400>,     'Плохой запрос',     'http_codes<400>' );
    is( $obj.http_codes<401>,     'Не авторизован',    'http_codes<401>' );
    is( $obj.http_codes<402>,     'Требуется оплата',  'http_codes<402>' );
    is( $obj.http_codes<403>,     'Запрещено',         'http_codes<403>' );
    is( $obj.http_codes<404>,     'Не найдено',        'http_codes<404>' );
    is( $obj.http_codes<405>,     'Метод не применим', 'http_codes<405>' );
}, 'Check attributes from RU locale';

done-testing;
