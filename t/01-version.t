use v6.d;
use Test;
use lib 'lib';

use Pheix::Model::Version;

plan 1;

constant gitver = @*ARGS[0] // q{};
constant curver = @*ARGS[1] // q{};

my $obj = Pheix::Model::Version.new;
my $mj = $obj.get_major;
my $mn = $obj.get_minor;
my $rl = $obj.get_release;
my $vn = $obj.get_version;

# Check version
subtest {
    plan 6;

    if curver && gitver {
        if curver ~~ / (<[\d]>+) '.' (<[\d]>+) '.' (<[\d]>+) / {
            my @gv = ( +($0 // 0), +($1 // 0), +($2 // 0) );

            ok(
                ($mj == @gv[0] && $mn == @gv[1] && $rl == @gv[2]),
                'curr git commit ver {' ~ curver ~ '} and Version.pm {' ~ $vn ~
                '} must be equal',
            );
        }
        else {
            skip 'no current version is given', 1;
        }

        if gitver ~~ / (<[\d]>+) '.' (<[\d]>+) '.' (<[\d]>+) / {
            my @gv = ( +($0 // 0), +($1 // 0), +($2 // 0) );

            ok(
                (
                    ($mj == @gv[0] && $mn == @gv[1] && $rl == ( @gv[2] + 1 )) ||
                    ($mj == @gv[0] && $mn == ( @gv[1] + 1 ) && $rl == 0) ||
                    ($mj == ( @gv[0] + 1 ) && $mn == 0 && $rl == 0)
                ),
                'prev git commit ver {' ~ gitver ~ '} and Version.pm {' ~ $vn ~
                '} must differ by 1.0.0 || x.1.0 || x.x.1',
            );
        }
        else {
            skip 'no latest git commit version is given', 1;
        }
    }
    else {
        skip 'no current version and latest git commit version are given', 2;
    }

    ok(
        $mj ~~ /<[\d]>/,
        'get_major() should be a digit: ' ~ $mj,
    );
    ok(
        $mn ~~ /<[\d]>/,
        'get_minor() should be a digit: ' ~ $mn,
    );
    ok(
        $rl ~~ /<[\d]>/,
        'get_release() should be a digit: ' ~ $rl,
    );
    ok(
        $vn ~~ / <[\d]>+ '.' <[\d]>+ '.' <[\d]>+ /,
        'get_version() should be a version str: ' ~ $vn,
    );
}, 'Check version';

done-testing;
