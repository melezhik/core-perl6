<TABLE WIDTH="100%" BORDER=0 CELLPADDING=7 CELLSPACING=0>
	<TR>
		<TD  class=standart_text>
			<h3>Купим срочно</h3>

<p>Ниже перечислены типы строений и конструкций, в покупке информации о которых мы <u>ЗАИНТЕРЕСОВАНЫ</u>. Если Вы владеете таковой информацией <a href="contact.html" class=main_menu>свяжитесь с нами</a>!</p>


<table border="0" cellpadding="0" cellspacing="10" width="400" align=center>
  <tr>
    <td colspan=2 class=standart_text><a name=balki></a>
    <p><strong>Балочные перекрытия</strong></p>
    <p>Стойкость профиля к коррозии и негорючесть позволяют применять фермы и металлоконструкции в условиях повышенной влажности и при предъявлении требований к огнестойкости конструкции.<br><br>Металлические фермы перекрывают пролеты до 14 метров без промежуточных опор. Низкий удельный вес легких стальных тонкостенных металлоконструкции в сочетании с их высокими несущими свойствами позволяет усилить межэтажные перекрытия без серьезных нагрузок на фундамент и цокольную часть здания.</p>
    </td>
  </tr>
  <tr>
    <td><a href="list4.html"><IMG SRC="images/prometal/buynow/small/photo00.jpg" border=0 WIDTH=200 ALT="Нажмите на фотографию для увеличения"></a></td>
    <td><a href="list4.html"><IMG SRC="images/prometal/buynow/small/photo02.jpg" border=0 WIDTH=200 ALT="Нажмите на фотографию для увеличения"></a></td>
  </tr>
  <tr>
    <td colspan=2><hr size=1></td>
  </tr>
  <tr>
    <td colspan=2 class=standart_text><a name=balki_fermi></a>
    <p><strong>Перекрытия из ферм</strong></p>
    <p>Металлическая ферма - это строительная несущая конструкция, выполненная из стержней, образующих не изменяемую с точки зрения геометрии фигуру. Наиболее прочная форма – треугольник. Сварная ферма представляет собой набор стержней, которые образуют треугольники. В случае если нагрузка на ферму прикладывается к ее узлам и действует в зоне оси фермы, то все стержни фермы испытывают только осевое усилие (усилие на сжатие или на разрыв). В связи с вышесказанным утверждением - общую работу сварной металлической фермы (на кручение, разрыв, изгиб) можно рассматривать как сопротивление каждого отдельного элемента фермы.</p>  
    </td>
  </tr>
  <tr>
    <td><a href="list1.html"><IMG SRC="images/prometal/buynow/small/photo45.jpg" border=0 WIDTH=200 ALT="Нажмите на фотографию для увеличения"></a></td>
    <td><a href="list1.html"><IMG SRC="images/prometal/buynow/small/photo53.jpg" border=0 WIDTH=200 ALT="Нажмите на фотографию для увеличения"></a></td>
  </tr>
  <tr>
    <td colspan=2><hr size=1></td>
  </tr> 
  <tr>
    <td colspan=2 class=standart_text><a name=drygoe></a>
    <p><strong>Здания из металлоконструкций типа «Канск»</strong></p>
    <p>Здание «Канск», размером 24х60 метров, имеет 18 оконных проемов и собрано из двух модулей рамной конструкции.  Здание состоит из ограждающих металлоконструкций из стеновых панелей со стальными обшивками утеплением типа «сэндвич», толщиной 130 мм. Количество панелей -  148 штук. Общий вес металлоконструкций – 112 тонн. <a href="library_1343032380.html" class=simple_link>Подробнее о зданиях «Канск»...</a></p>
    </td>
  </tr>
  <tr>
    <td><a href="list0.html"><IMG SRC="images/prometal/buynow/small/photo11.jpg" border=0 WIDTH=200 ALT="Здания из металлоконструкций типа «Канск»"></a></td>
    <td><a href="list0.html"><IMG SRC="images/prometal/buynow/small/photo13.jpg" border=0 WIDTH=200 ALT="Здания из металлоконструкций типа «Канск»"></a></td>
  </tr>
  <tr>
    <td colspan=2><hr size=1></td>
  </tr> 
  <tr>
    <td colspan=2 class=standart_text><a name=floors></a><p><strong>«Этажерки»</strong></p></td>
  </tr>
  <tr>
    <td><a href="list3.html"><IMG SRC="images/prometal/buynow/small/photo14.jpg" border=0 WIDTH=200 ALT="Нажмите на фотографию для увеличения"></a></td>
    <td><a href="list3.html"><IMG SRC="images/prometal/buynow/small/photo16.jpg" border=0 WIDTH=200 ALT="Нажмите на фотографию для увеличения"></a></td>
  </tr>
  <tr>
    <td colspan=2><hr size=1></td>
  </tr>
  <tr>
    <td colspan=2 class=standart_text><a name=molodechno></a><p><strong>«Молодечно»</strong></p>
    <p>Металлоконструкции покрытия предназначены для без прогонного решения кровли с непосредственным опиранием профилированного настила на верхние пояса стропильных ферм. В проектной документации предусмотрен единый сортамент ферм,используемый при шаге ферм 4 и 6 метров. Подробнее о типе конструкции «Молодечно» читайте в разделе <a href="library_1335430637.html" class=simple_link>Библиотека</a>.</p> 
    </td>
  </tr>
  <tr>
    <td><a href="list2.html"><IMG SRC="images/prometal/buynow/small/photo34.jpg" border=0 WIDTH=200 ALT="Нажмите на фотографию для увеличения"></a></td>
    <td><a href="list2.html"><IMG SRC="images/prometal/buynow/small/photo36.jpg" border=0 WIDTH=200 ALT="Нажмите на фотографию для увеличения"></a></td>
  </tr>
  <tr>
    <td colspan=2><hr size=1></td>
  </tr>  
  <tr>
    <td colspan=2 class=standart_text><p><strong>«Москва»</strong></p></td>
  </tr>
  <tr>
    <td><a href="list6.html"><IMG SRC="images/prometal/buynow/small/photo39.jpg" border=0 WIDTH=200 ALT="Нажмите на фотографию для увеличения"></a></td>
    <td><a href="list6.html"><IMG SRC="images/prometal/buynow/small/photo40.jpg" border=0 WIDTH=200 ALT="Нажмите на фотографию для увеличения"></a></td>
  </tr>
  <tr>
    <td colspan=2><hr size=1></td>
  </tr>
  <tr>
    <td colspan=2 class=standart_text><a name=orsk></a><p><strong>«Орск»</strong></p>
    <p>Металлоконструкции зданий типа «Орск» осуществляются системой двух шарнирных одно пролетных рам коробчатого сечения пролетом 24 метра, по которым уложены прогоны и профилированный лист (профнастил), выполняющий одновременно роль горизонтальных связей. Продольная жесткость здания обеспечивается системой вертикальных связей по колоннам. Рамы – замкнутого коробчатого сечения, образованные двумя швеллерами, соединенными по бокам на сварке листами с двумя продольными гофрами. Монтажные стыки рамы располагаются в карнизных и коньковых узлах и выполнены на фланцах с применением в соединении высокопрочных болтов. Подробнее конструкциях «Орск» читайте в разделе <a href="library_1336129929.html" class=main_menu>Библиотека</a>.</p>
    
    </td>
  </tr>
  <tr>
    <td><a href="list8.html"><IMG SRC="images/prometal/buynow/small/photo43.jpg" border=0 WIDTH=200 ALT="Нажмите на фотографию для увеличения"></a></td>
    <td><a href="list8.html"><IMG SRC="images/prometal/buynow/small/photo44.jpg" border=0 WIDTH=200 ALT="Нажмите на фотографию для увеличения"></a></td>
  </tr>
  <tr>
    <td colspan=2><hr size=1></td>
  </tr> 
  <tr>
    <td colspan=2 class=standart_text><a name=lezhalij></a><p><strong>Лежалый металл</strong></p></td>
  </tr>
  <tr>
    <td><a href="list5.html"><IMG SRC="images/prometal/buynow/small/photo26.jpg" border=0 WIDTH=200 ALT="Нажмите на фотографию для увеличения"></a></td>
    <td><a href="list5.html"><IMG SRC="images/prometal/buynow/small/photo28.jpg" border=0 WIDTH=200 ALT="Нажмите на фотографию для увеличения"></a></td>
  </tr>
  <tr>
    <td colspan=2><hr size=1></td>
  </tr> 

  
  <tr>
    <td colspan=2 class=standart_text><p><strong>Оборудование</strong></p></td>
  </tr>
  <tr>
    <td><a href="list7.html"><IMG SRC="images/prometal/buynow/small/photo41.jpg" border=0 WIDTH=200 ALT="Нажмите на фотографию для увеличения"></a></td>
    <td><a href="list7.html"><IMG SRC="images/prometal/buynow/small/photo42.jpg" border=0 WIDTH=200 ALT="Нажмите на фотографию для увеличения"></a></td>
  </tr>
  <tr>
    <td colspan=2><hr size=1></td>
  </tr>  


          
</table>

<p align=center>Если Вы владеете информацией о перечисленных типах<br>строений и конструкций - <a href="contact.html" class=main_menu>свяжитесь с нами</a>!</p>


<p>&nbsp;</p>
			
		</TD>
	</TR>
</TABLE>