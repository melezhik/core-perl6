<p>
	<b>Здания из металлоконструкций типа &laquo;Орск&raquo;</b></p>
<p>
	Здания типа &laquo;Орск&raquo; представляют систему каркаса из плоских рам, которые устанавливаются с шагом 6 метров. Каждая рама работает на один пролет. В многопролетные здания устанавливается требуемое число смежных одиночных рам, не связанных друг с другом конструктивно. Основные типы зданий&nbsp; имеют пролет шириной 12, 18, 24 метра и высотой 6,32 м для бескранового и 7,52 м для кранового варианта.</p>
<p>
	<a href="http://stroi-g.ru/images/orsk/pix_01.jpg" target="_blank"><img alt="Здания из металлоконструкций типа «Орск»" src="http://stroi-g.ru/images/orsk/small/pix_01.jpg" style="width: 449px; height: 275px;" /></a></p>
<div>
	<table align="left" cellpadding="0" cellspacing="0" class="simpletab" width="449">
		<tbody>
			<tr>
				<td>
					Тип здания</td>
				<td>
					Высота рамы &nbsp;h,м</td>
				<td>
					Пролет рамы L,м</td>
				<td>
					<p>
						Контрольной размер L<sub>1</sub>, мм</p>
				</td>
			</tr>
			<tr>
				<td rowspan="2">
					Бескрановое</td>
				<td rowspan="2">
					6,980</td>
				<td>
					18</td>
				<td>
					11365</td>
			</tr>
			<tr>
				<td>
					24</td>
				<td>
					13830</td>
			</tr>
			<tr>
				<td rowspan="2">
					Крановое</td>
				<td rowspan="2">
					8,180</td>
				<td>
					18</td>
				<td>
					12166</td>
			</tr>
			<tr>
				<td>
					24</td>
				<td>
					14500</td>
			</tr>
		</tbody>
	</table>
</div>
<p>
	&nbsp;</p>
<p>
	&nbsp;</p>
<p>
	&nbsp;</p>
<p>
	&nbsp;</p>
<p>
	&nbsp;</p>
<p>
	Разработаны следующие взаимозаменяемые виды каркасов типа &laquo;Орск&raquo; - &laquo;Орск-1&raquo; и &laquo;Орск-2&raquo;. По ТУ 5283-118-04614443-00 каркас рам типа &laquo;Орск-1&raquo; выполняется из металлопроката коробчатого сечения, каркас рам типа &laquo;Орск-2&raquo; по ТУ 5283-119-0461443-00 - из гнутосварных металлических профилей. Оба вида каркасов позволяют в крановом варианте установить в здании мостовые электрические краны грузоподъемностью от 5 до 8 тонн.</p>
<p>
	<a href="http://stroi-g.ru/images/orsk/pix_02.jpg" target="_blank"><img alt="Здания из металлоконструкций типа «Орск»" src="http://stroi-g.ru/images/orsk/small/pix_02.jpg" style="width: 449px; height: 510px;" /></a></p>
<p>
	<a href="http://stroi-g.ru/images/orsk/pix_03.jpg" target="_blank"><img alt="Здания из металлоконструкций типа «Орск»" src="http://stroi-g.ru/images/orsk/small/pix_03.jpg" style="width: 449px; height: 468px;" /></a></p>
<p>
	Все монтажные соединения выполняются исключительно на болтах. Благодаря надежной конструкции базового каркаса строительство зданий типа &laquo;Орск&raquo; отличается низкой трудоемкостью и выполняется в сжатые сроки с минимальным применением грузоподъемных механизмов (кранов, лебедок).<br />
	Стены зданий типа &laquo;Орск&raquo; состоят из трехслойных панелей со стальными обшивками иутеплителем из минеральной ваты. Обычно в базовый комплект поставки здания входят каркас, кровля, трехслойные стеновые панели, окна, ворота, двери и метизы.</p>
<p>
	Дополнительные чертежи элементов зданий из металлоконструкций типа &laquo;Орск&raquo; представлены ниже:</p>
<p>
	<a href="http://stroi-g.ru/images/orsk/pix_04.jpg" target="_blank"><img alt="Здания из металлоконструкций типа «Орск»" src="http://stroi-g.ru/images/orsk/small/pix_04.jpg" style="width: 449px; height: 418px;" /></a></p>
<p>
	<a href="http://stroi-g.ru/images/orsk/pix_05.jpg" target="_blank"><img alt="Здания из металлоконструкций типа «Орск»" src="http://stroi-g.ru/images/orsk/small/pix_05.jpg" style="width: 449px; height: 427px;" /></a></p>
<p>
	<a href="http://stroi-g.ru/images/orsk/small/pix_06.jpg" target="_blank"><img alt="Здания из металлоконструкций типа «Орск»" src="http://stroi-g.ru/images/orsk/small/pix_06.jpg" style="width: 449px; height: 526px;" /></a></p>
<p>
	<a href="http://stroi-g.ru/images/orsk/pix_07.jpg" target="_blank"><img alt="Здания из металлоконструкций типа «Орск»" src="http://stroi-g.ru/images/orsk/small/pix_07.jpg" style="width: 449px; height: 548px;" /></a></p>
<p>
	<a href="http://stroi-g.ru/images/orsk/pix_07.jpg" target="_blank"><img alt="Здания из металлоконструкций типа «Орск»" src="http://stroi-g.ru/images/orsk/small/pix_09.jpg" style="width: 449px; height: 507px;" /></a></p>
<p>
	<a href="http://stroi-g.ru/images/orsk/pix_10.jpg" target="_blank"><img alt="Здания из металлоконструкций типа «Орск»" src="http://stroi-g.ru/images/orsk/small/pix_10.jpg" style="width: 449px; height: 535px;" /></a></p>
<p>
	<a href="http://stroi-g.ru/images/orsk/pix_10.jpg" target="_blank"><img alt="Здания из металлоконструкций типа «Орск»" src="http://stroi-g.ru/images/orsk/small/pix_11.jpg" style="width: 449px; height: 350px;" /></a></p>
<p>
	Использование строительных конструкций зданий типа &laquo;Орск&raquo; возможно практически во всех климатических зонах России до IV снегового района и VII ветрового районов, в районах с сейсмичностью до 9 баллов и с расчетной температурой до - 65&deg;С.</p>
