<p class="standart_text_head">
	Цены на услуги. Прайс-лист</p>
<p class="standart_text" style="font-size:12pt;">
	Стоимость на монтаж потолка из гипсокартона указана на январь-декабрь 2014 года.</p>
<table border="0" cellpadding="0" cellspacing="0" class="table1" width="675">
	<tbody>
		<tr>
			<td class="standart_text" style="background-color:#F0F0F0" width="63%">
				Наименование работ</td>
			<td class="standart_text" style="background-color:#F0F0F0" width="14%">
				Ед. измер.</td>
			<td class="standart_text" style="background-color:#F0F0F0" width="23%">
				Стоимость работ, руб. <span style="color:#FF0000">*</span></td>
		</tr>
		<tr>
			<td class="standart_text" width="63%">
				Монтаж одноуровневого потолка с выставлением каркаса на 600 мм</td>
			<td class="standart_text" width="14%">
				1 м<sup>2</sup></td>
			<td class="standart_text" width="23%">
				420</td>
		</tr>
		<tr>
			<td class="standart_text" width="63%">
				Монтаж одноуровневого потолка с выставлением каркаса на 400 мм</td>
			<td class="standart_text" width="14%">
				1 м<sup>2</sup></td>
			<td class="standart_text" width="23%">
				480</td>
		</tr>
		<tr>
			<td class="standart_text" width="63%">
				Тепло/звукоизоляция, толщина 1 слоя до 50 мм</td>
			<td class="standart_text" width="14%">
				1 м<sup>2</sup></td>
			<td class="standart_text" width="23%">
				80</td>
		</tr>
		<tr>
			<td class="standart_text" width="63%">
				Монтаж криволинейных коробов</td>
			<td class="standart_text" width="14%">
				1 п.м.</td>
			<td class="standart_text" width="23%">
				500</td>
		</tr>
		<tr>
			<td class="standart_text" width="63%">
				Монтаж прямолинейных коробов</td>
			<td class="standart_text" width="14%">
				1 п.м.</td>
			<td class="standart_text" width="23%">
				350</td>
		</tr>
		<tr>
			<td class="standart_text" width="63%">
				Монтаж прямолинейных коробов с устройством ниш под скрытую подсветку</td>
			<td class="standart_text" width="14%">
				1 п.м.</td>
			<td class="standart_text" width="23%">
				600</td>
		</tr>
		<tr>
			<td class="standart_text" width="63%">
				Монтаж криволинейных коробов с устройством ниш под скрытую подсветку</td>
			<td class="standart_text" width="14%">
				1 п.м.</td>
			<td class="standart_text" width="23%">
				680</td>
		</tr>
	</tbody>
</table>
<p>
	<span style="color:#FF0000">*</span> - <span style="font-size:7pt;"><b>Цены указаны без учета стоимости материалов!</b></span></p>
<p>
	<strong>Примечание:</strong></p>
<ol class="standart_text">
	<li>
		При опускании потолка ниже 11 см (высота стандартного подвеса) от уровня существующего потолка, стоимость увеличивается на 80 руб. за 1 м<sup>2</sup>;</li>
	<li>
		При монтаже потолка на высоте более 3,3 м (высота средней квартиры), стоимость увеличивается на 80 руб. за 1 м<sup>2</sup> на каждый метр высоты;</li>
	<li>
		Cтоимость второго слоя гипсокартона, при монтаже одноуровневого потолка из гипсокартона, равна 80 руб. за 1 м<sup>2</sup>.</li>
</ol>
<p>
	&nbsp;</p>
<p>
	<img alt="" src="images/newsline.gif" style="width: 675px; height: 1px;" /></p>
<p>
	&nbsp;</p>
<p class="standart_text" style="font-size:12pt;">
	Стоимость на монтаж перегородок из гипсокартона указана на январь-декабрь 2014 года.</p>
<table border="0" cellpadding="0" cellspacing="0" class="table1" width="675">
	<tbody>
		<tr>
			<td class="standart_text" style="background-color:#F0F0F0" width="63%">
				Наименование работ</td>
			<td class="standart_text" style="background-color:#F0F0F0" width="14%">
				Ед. измер.</td>
			<td class="standart_text" style="background-color:#F0F0F0" width="23%">
				Стоимость работ, руб. <span style="color:#FF0000">*</span></td>
		</tr>
		<tr>
			<td class="standart_text" width="63%">
				Монтаж перегородок высотой до 3 метров с выставлением каркаса на 600 мм</td>
			<td class="standart_text" width="14%">
				1 м<sup>2</sup></td>
			<td class="standart_text" width="23%">
				450</td>
		</tr>
		<tr>
			<td class="standart_text" width="63%">
				Монтаж перегородок высотой до 3 метров с выставлением каркаса на 400 мм</td>
			<td class="standart_text" width="14%">
				1 м<sup>2</sup></td>
			<td class="standart_text" width="23%">
				520</td>
		</tr>
		<tr>
			<td class="standart_text" width="63%">
				Монтаж двухслойных перегородок высотой до 3 метров с выставлением каркаса на 600 мм (ГКЛ в 2 слоя с каждой стороны)</td>
			<td class="standart_text" width="14%">
				1 м<sup>2</sup></td>
			<td class="standart_text" width="23%">
				580</td>
		</tr>
		<tr>
			<td class="standart_text" width="63%">
				Монтаж двухслойных перегородок высотой до 3 метров с выставлением каркаса на 400 мм (ГКЛ в 2 слоя с каждой стороны)</td>
			<td class="standart_text" width="14%">
				1 м<sup>2</sup></td>
			<td class="standart_text" width="23%">
				640</td>
		</tr>
		<tr>
			<td class="standart_text" width="63%">
				Звуко/теплоизоляция, толщина 1 слоя до 50 мм</td>
			<td class="standart_text" width="14%">
				1 м<sup>2</sup></td>
			<td class="standart_text" width="23%">
				80</td>
		</tr>
		<tr>
			<td class="standart_text" width="63%">
				Врезка дверного проема с монтажом деревянных закладных</td>
			<td class="standart_text" width="14%">
				1 шт.</td>
			<td class="standart_text" width="23%">
				900</td>
		</tr>
	</tbody>
</table>
<p>
	<span style="color:#FF0000">*</span> - <span style="font-size:7pt;"><b>Цены указаны без учета стоимости материалов!</b></span></p>
<p>
	&nbsp;</p>
<p>
	<img alt="" src="images/newsline.gif" style="width: 675px; height: 1px;" /></p>
<p>
	&nbsp;</p>
<p class="standart_text" style="font-size:12pt;">
	Стоимость на монтаж стен из гипсокартона указана на январь-декабрь 2014 года.</p>
<table border="0" cellpadding="0" cellspacing="0" class="table1" width="675">
	<tbody>
		<tr>
			<td class="standart_text" style="background-color:#F0F0F0" width="63%">
				Наименование работ</td>
			<td class="standart_text" style="background-color:#F0F0F0" width="14%">
				Ед. измер.</td>
			<td class="standart_text" style="background-color:#F0F0F0" width="23%">
				Стоимость работ, руб. <span style="color:#FF0000">*</span></td>
		</tr>
		<tr>
			<td class="standart_text" width="63%">
				Стоимость монтажа стены с выставлением каркаса на 600 мм</td>
			<td class="standart_text" width="14%">
				1 м<sup>2</sup></td>
			<td class="standart_text" width="23%">
				320</td>
		</tr>
		<tr>
			<td class="standart_text" width="63%">
				Стоимость монтажа стены с выставлением каркаса на 400 мм</td>
			<td class="standart_text" width="14%">
				1 м<sup>2</sup></td>
			<td class="standart_text" width="23%">
				360</td>
		</tr>
		<tr>
			<td class="standart_text" width="63%">
				Стоимость монтажа стены в 2 слоя с выставлением каркаса на 600 мм</td>
			<td class="standart_text" width="14%">
				1 м<sup>2</sup></td>
			<td class="standart_text" width="23%">
				380</td>
		</tr>
		<tr>
			<td class="standart_text" width="63%">
				Стоимость монтажа стены в 2 слоя с выставлением каркаса на 400 мм</td>
			<td class="standart_text" width="14%">
				1 м<sup>2</sup></td>
			<td class="standart_text" width="23%">
				420</td>
		</tr>
		<tr>
			<td class="standart_text" width="63%">
				Звуко/теплоизоляция, толщина 1 слоя до 50 мм</td>
			<td class="standart_text" width="14%">
				1 м<sup>2</sup></td>
			<td class="standart_text" width="23%">
				80</td>
		</tr>
		<tr>
			<td class="standart_text" width="63%">
				Монтаж ниш в стенах</td>
			<td class="standart_text" width="14%">
				1 п.м.</td>
			<td class="standart_text" width="23%">
				350</td>
		</tr>
	</tbody>
</table>
<p>
	<span style="color:#FF0000">*</span> - <span style="font-size:7pt;"><b>Цены указаны без учета стоимости материалов!</b></span></p>
<p>
	<strong>Примечание:</strong></p>
<ol class="standart_text">
	<li>
		Стоимость монтажа стен из гипсокартона при отступе от существующих стен более 11 см (длина стандартного подвеса) увеличивается на 80 рублей за 1 м<sup>2</sup>;</li>
	<li>
		Стоимость монтажа стен из гипсокартона высотой более 3,3 метра (высота средней квартиры) увеличивается на 80 рублей за 1 м<sup>2</sup> на каждый метр высоты;</li>
	<li>
		Cтоимость сложных конструкций из гипсокартона оценивается отдельно 80 рублей за 1 м<sup>2</sup>.</li>
</ol>
<p>
	&nbsp;</p>
<p>
	<img alt="" src="images/newsline.gif" style="width: 675px; height: 1px;" /></p>
<p>
	&nbsp;</p>
<p class="standart_text" style="font-size:12pt;">
	Стоимость малярных работ указана на январь-декабрь 2014 года.</p>
<table border="0" cellpadding="0" cellspacing="0" class="table1" width="675">
	<tbody>
		<tr>
			<td class="standart_text" style="background-color:#F0F0F0" width="63%">
				Наименование работ</td>
			<td class="standart_text" style="background-color:#F0F0F0" width="14%">
				Ед. измер.</td>
			<td class="standart_text" style="background-color:#F0F0F0" width="23%">
				Стоимость работ, руб. <span style="color:#FF0000">*</span></td>
		</tr>
		<tr>
			<td class="standart_text" width="63%">
				Грунтовка</td>
			<td class="standart_text" width="14%">
				1 м<sup>2</sup></td>
			<td class="standart_text" width="23%">
				15</td>
		</tr>
		<tr>
			<td class="standart_text" width="63%">
				Заделка швов и саморезов</td>
			<td class="standart_text" width="14%">
				1 м<sup>2</sup></td>
			<td class="standart_text" width="23%">
				40</td>
		</tr>
		<tr>
			<td class="standart_text" width="63%">
				Шпатлевка Ветонит LR+ в два слоя</td>
			<td class="standart_text" width="14%">
				1 м<sup>2</sup></td>
			<td class="standart_text" width="23%">
				120</td>
		</tr>
		<tr>
			<td class="standart_text" width="63%">
				Шлифовка</td>
			<td class="standart_text" width="14%">
				1 м<sup>2</sup></td>
			<td class="standart_text" width="23%">
				60</td>
		</tr>
		<tr>
			<td class="standart_text" width="63%">
				Проклейка стеклохолста</td>
			<td class="standart_text" width="14%">
				1 м<sup>2</sup></td>
			<td class="standart_text" width="23%">
				80</td>
		</tr>
		<tr>
			<td class="standart_text" width="63%">
				Шпатлевка Ветонит LR+ в один слой</td>
			<td class="standart_text" width="14%">
				1 м<sup>2</sup></td>
			<td class="standart_text" width="23%">
				60</td>
		</tr>
		<tr>
			<td class="standart_text" width="63%">
				Шпатлевка финишная в два слоя</td>
			<td class="standart_text" width="14%">
				1 м<sup>2</sup></td>
			<td class="standart_text" width="23%">
				170</td>
		</tr>
		<tr>
			<td class="standart_text" width="63%">
				Покраска в два слоя</td>
			<td class="standart_text" width="14%">
				1 м<sup>2</sup></td>
			<td class="standart_text" width="23%">
				160</td>
		</tr>
		<tr>
			<td class="standart_text" width="63%">
				Шпатлевка и покраска откосов</td>
			<td class="standart_text" width="14%">
				1 п.м.</td>
			<td class="standart_text" width="23%">
				500</td>
		</tr>
		<tr>
			<td class="standart_text" width="63%">
				Монтаж потолочных багет с полочкой до 10 см</td>
			<td class="standart_text" width="14%">
				1 п.м.</td>
			<td class="standart_text" width="23%">
				80</td>
		</tr>
		<tr>
			<td class="standart_text" width="63%">
				Монтаж потолочных багет с полочкой более 10 см</td>
			<td class="standart_text" width="14%">
				1 п.м.</td>
			<td class="standart_text" width="23%">
				140</td>
		</tr>
		<tr>
			<td class="standart_text" width="63%">
				<b>ВЕСЬ КОМПЛЕКС РАБОТ ПО ПОТОЛКАМ</b></td>
			<td class="standart_text" width="14%">
				1 м<sup>2</sup></td>
			<td class="standart_text" width="23%">
				<b>765</b></td>
		</tr>
		<tr>
			<td class="standart_text" width="63%">
				<b>ВЕСЬ КОМПЛЕКС РАБОТ ПО СТЕНАМ</b></td>
			<td class="standart_text" width="14%">
				1 м<sup>2</sup></td>
			<td class="standart_text" width="23%">
				<b>650</b></td>
		</tr>
		<tr>
			<td class="standart_text" width="63%">
				<b>СТОИМОСТЬ ПОКЛЕЙКИ ОБОЕВ</b></td>
			<td class="standart_text" width="14%">
				от</td>
			<td class="standart_text" width="23%">
				<b>200</b></td>
		</tr>
	</tbody>
</table>
<p>
	&nbsp;</p>
<p class="standart_text">
	Если у вас возникли вопросы по ценам или объемам выполняемых работ - <a class="simplelink" href="contacts.html">свяжитесь с нами!</a></p>
