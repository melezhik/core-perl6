<h1>Велопробег с заездами на заброшенные военные объекты: Кубинка-Дорохово 75 км. ( 13 апреля 2014 г.)</h1>

<p><strong>Участники: </strong>Павел Ляхов, Никита Чебаков.</p>

<p><strong>Трэк:</strong>&nbsp;<a href="http://www.gpsies.com/map.do?fileId=wubpgopdagoxpbys"><img alt="GPSies - tanki" src="http://www.gpsies.com/images/linkus.png" /></a></p>

<p>Изначально планировался более насыщенный достопримечательностями маршрут на два дня, но было решено разделить путешествие на две отдельных части. Цивильная часть предусматривала посещение танкового музея в Кубинке (&laquo;Центрального музея бронетанкового вооружения и техники&raquo; Министерства обороны Российской Федерации&raquo;). &laquo;Не цивильная&raquo; часть включала заезды на территории заброшенных военных объектах, затерянных в лесах подмосковных.&nbsp;<br />
<br />
&laquo;Не цивильная&raquo; часть знакомства с окрестностями Кубинки прошла в виде вело заезда. Погода была отличная, большая часть пути пыла пройдена по твердым дорожным покрытиям. В лесах грязь еще не развезлась, в ельниках и вовсе дороги были заморожены. Так что фоток велосипедов и велосипедистов, вымазанных в грязи, тут не будет.&nbsp;<br />
<br />
Ограничимся публикацией фотографий заброшенных военных объектов.&nbsp;<br />
<br />
Я думал, стоит ли выкладывать трэк, и, учитывая, что данные объекты давно заброшены и регулярно посещаются любителями приключений &ndash; то есть давно открыты, решил всетаки выложить. Маршрут интересный, желающие могут повторить.&nbsp;</p>

<p><br />
<br />
Остальные комментарии под фото.</p>

<p>Руины южнее Обухово</p>

<p><img src="http://drojj-taigi.narod.ru/kubinka2014/k01.jpg" style="height:533px; width:800px" /></p>

<p><img src="http://drojj-taigi.narod.ru/kubinka2014/k02.jpg" /></p>

<p><img src="http://drojj-taigi.narod.ru/kubinka2014/k03.jpg" /></p>

<p><img src="http://drojj-taigi.narod.ru/kubinka2014/k04.jpg" /></p>

<p><img src="http://drojj-taigi.narod.ru/kubinka2014/k05.jpg" /></p>

<p><img src="http://drojj-taigi.narod.ru/kubinka2014/k06.jpg" /></p>

<p><img src="http://drojj-taigi.narod.ru/kubinka2014/k07.jpg" /></p>

<p><img src="http://drojj-taigi.narod.ru/kubinka2014/k08.jpg" /></p>

<p><img src="http://drojj-taigi.narod.ru/kubinka2014/k09.jpg" /></p>

<p><img src="http://drojj-taigi.narod.ru/kubinka2014/k10.jpg" /></p>

<p><img src="http://drojj-taigi.narod.ru/kubinka2014/k11.jpg" /></p>

<p><img src="http://drojj-taigi.narod.ru/kubinka2014/k12.jpg" /></p>

<p><a href="http://drojj-taigi.narod.ru/kubinka2014/k131.jpg"><img src="http://drojj-taigi.narod.ru/kubinka2014/k13.jpg" /></a></p>

<p><img src="http://drojj-taigi.narod.ru/kubinka2014/k14.jpg" /></p>

<p><img src="http://drojj-taigi.narod.ru/kubinka2014/k15.jpg" /></p>

<p><img src="http://drojj-taigi.narod.ru/kubinka2014/k16.jpg" /></p>

<p><img src="http://drojj-taigi.narod.ru/kubinka2014/k17.jpg" /></p>

<p><img src="http://drojj-taigi.narod.ru/kubinka2014/k18.jpg" /></p>

<p><img src="http://drojj-taigi.narod.ru/kubinka2014/k19.jpg" /></p>

<p><img src="http://drojj-taigi.narod.ru/kubinka2014/k20.jpg" /></p>

<p><img src="http://drojj-taigi.narod.ru/kubinka2014/k21.jpg" /></p>

<p><img src="http://drojj-taigi.narod.ru/kubinka2014/k22.jpg" /></p>

<p><img src="http://drojj-taigi.narod.ru/kubinka2014/k23.jpg" /></p>

<p><img src="http://drojj-taigi.narod.ru/kubinka2014/k24.jpg" /></p>

<p><img src="http://drojj-taigi.narod.ru/kubinka2014/k25.jpg" /></p>

<p><img src="http://drojj-taigi.narod.ru/kubinka2014/k26.jpg" /></p>

<p><img src="http://drojj-taigi.narod.ru/kubinka2014/k27.jpg" /></p>

<p><img src="http://drojj-taigi.narod.ru/kubinka2014/k28.jpg" /></p>

<p><img src="http://drojj-taigi.narod.ru/kubinka2014/k29.jpg" /></p>

<p><img src="http://drojj-taigi.narod.ru/kubinka2014/k30.jpg" /></p>

<p><img src="http://drojj-taigi.narod.ru/kubinka2014/k31.jpg" /></p>

<p><img src="http://drojj-taigi.narod.ru/kubinka2014/k32.jpg" /></p>

<p><img src="http://drojj-taigi.narod.ru/kubinka2014/k33.jpg" /></p>

<p><img src="http://drojj-taigi.narod.ru/kubinka2014/k34.jpg" /></p>

<p><img src="http://drojj-taigi.narod.ru/kubinka2014/k35.jpg" /></p>

<p><img src="http://drojj-taigi.narod.ru/kubinka2014/k36.jpg" /></p>

<p><img src="http://drojj-taigi.narod.ru/kubinka2014/k37.jpg" /></p>

<p><img src="http://drojj-taigi.narod.ru/kubinka2014/k38.jpg" /></p>

<p><img src="http://drojj-taigi.narod.ru/kubinka2014/k39.jpg" /></p>

<p><img src="http://drojj-taigi.narod.ru/kubinka2014/k40.jpg" /></p>

<p><img src="http://drojj-taigi.narod.ru/kubinka2014/k41.jpg" /></p>

<p>&nbsp;</p>

<p>Руины южнее Ястребово</p>

<p><img src="http://drojj-taigi.narod.ru/kubinka2014/k42.jpg" /></p>

<p><img src="http://drojj-taigi.narod.ru/kubinka2014/k43.jpg" /></p>

<p><img src="http://drojj-taigi.narod.ru/kubinka2014/k44.jpg" /></p>

<p><img src="http://drojj-taigi.narod.ru/kubinka2014/k45.jpg" /></p>

<p><img src="http://drojj-taigi.narod.ru/kubinka2014/k46.jpg" /></p>

<p><img src="http://drojj-taigi.narod.ru/kubinka2014/k47.jpg" /></p>

<p><img src="http://drojj-taigi.narod.ru/kubinka2014/k48.jpg" /></p>

<p><a href="http://drojj-taigi.narod.ru/kubinka2014/k491.jpg"><img src="http://drojj-taigi.narod.ru/kubinka2014/k49.jpg" /></a></p>

<p><img src="http://drojj-taigi.narod.ru/kubinka2014/k50.jpg" /></p>

<p><a href="http://drojj-taigi.narod.ru/kubinka2014/k511.jpg"><img src="http://drojj-taigi.narod.ru/kubinka2014/k51.jpg" /></a></p>

<p><img src="http://drojj-taigi.narod.ru/kubinka2014/k52.jpg" /></p>

<p><img src="http://drojj-taigi.narod.ru/kubinka2014/k53.jpg" /></p>

<p><img src="http://drojj-taigi.narod.ru/kubinka2014/k54.jpg" /></p>

<p><img src="http://drojj-taigi.narod.ru/kubinka2014/k55.jpg" /></p>

<p><img src="http://drojj-taigi.narod.ru/kubinka2014/k56.jpg" /></p>

<p>&nbsp;</p>

<p>Руины южнее с. Архангельское</p>

<p><img src="http://drojj-taigi.narod.ru/kubinka2014/k57.jpg" /></p>

<p><img src="http://drojj-taigi.narod.ru/kubinka2014/k58.jpg" /></p>

<p><img src="http://drojj-taigi.narod.ru/kubinka2014/k59.jpg" /></p>

<p><img src="http://drojj-taigi.narod.ru/kubinka2014/k60.jpg" /></p>

<p><img src="http://drojj-taigi.narod.ru/kubinka2014/k61.jpg" /></p>

<p><img src="http://drojj-taigi.narod.ru/kubinka2014/k62.jpg" /></p>

<p><img src="http://drojj-taigi.narod.ru/kubinka2014/k63.jpg" /></p>

<p><img src="http://drojj-taigi.narod.ru/kubinka2014/k64.jpg" /></p>

<p><img src="http://drojj-taigi.narod.ru/kubinka2014/k65.jpg" /></p>

<p><img src="http://drojj-taigi.narod.ru/kubinka2014/k66.jpg" /></p>

<p><img src="http://drojj-taigi.narod.ru/kubinka2014/k67.jpg" /></p>

<p><img src="http://drojj-taigi.narod.ru/kubinka2014/k68.jpg" /></p>

<p>&nbsp;</p>

<p>Село Архангельское</p>

<p><img src="http://drojj-taigi.narod.ru/kubinka2014/k69.jpg" /></p>

<p><img src="http://drojj-taigi.narod.ru/kubinka2014/k70.jpg" /></p>

<p><img src="http://drojj-taigi.narod.ru/kubinka2014/k71.jpg" /></p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;&nbsp;<var>23.04.2014. Павел Ляхов</var></p>
