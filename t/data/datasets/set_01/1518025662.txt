<h1>Выставка DRTG [живопись и др.]. Павел Ляхов, Никита Чебаков. 16-17 фев. 2018. Откр. 16 февр. в 19:00</h1>

<p>&nbsp;</p>

<p>В Городке художников на Верхней Масловке состоится выставка &laquo;DRTG&raquo;.&nbsp;Художники Павел Ляхов и Никита Чебаков.</p>

<p>Только один день! Открытие состоится 16 февраля в 19:00<br />
<br />
<img alt="" src="https://ic.pics.livejournal.com/drtg3/84211443/5049/5049_800.jpg" /><br />
&nbsp;</p>

<p>Реалистичная живопись как инструмент более других подходит для оставления зарубок в памяти. Мы пишем уходящую Россию. Точнее вторично, повторно уходящую. Из упомянутых уходов мы говорим о смерти русской деревни - она стала увядать в условиях тотальной урбанизации рождая новую форму, не населенную людьми и завораживающую своей красотой и атмосферой. Но и эти места уходят, превращаясь в урочища которые зарастают лесами и затопляются болотами. Это нормальный цикл жизни. Особая грусть и романтика этих мест притягивает путешественников и художников.</p>

<p>Проект DRTG включает в себя как приключенческо-туристическую так и художественную часть, в которую входят пленэры в походных условиях.<br />
&nbsp;</p>

<p>Выставка будет проходить в Доме художников по адресу Москва, ул. Верхняя Масловка дом. 1. 1 подьезд 5 этаж.<br />
Только один день! 16 февраля 2018.</p>

<p>Время работы выставки:</p>

<p>Открытие состоится 16 февраля в 19:00.</p>

<p>16 февраля 2018: с 19:00 до 23:00</p>

<p>Телефоны для справок 8 (929) 647-98-69 (Павел) 8 (901) 599-35-61 (Никита). О своем посещении выставки просьба сообщить заранее (можно написать СМС на один из вышеуказанных номеров), для того чтобы мы могли подготовить списки для входа в здание.</p>

<p>Источник:&nbsp;<a href="www.pavel-lyakhov.ru/rus/drtg-vistavka-fevral-2018.html">http://www.pavel-lyakhov.ru/rus/drtg-vistavka-fevral-2018.html&nbsp;</a></p>
