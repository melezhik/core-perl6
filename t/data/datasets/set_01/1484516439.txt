<h1>Лыжня Радищево (Лен. напр) - пл 50 км (Риж. напр.). [великая тропа пенсионеров] 24 км. (14.01.2017)</h1>

<p><br />
<br />
Прокатились по этой замечательной лыжне. 24 километра отличной лыжни с великолепными видами. В результате правильной пропилки завалов, состоящих из навалов стволов диаметром от полуметра до метра, доступ квадроциклов и снегоходов на лыжню закрыт, отсюда и отличного качества лыжня, не разбитая ни колесами ни гусеницами. Еще один плюс этой лыжни - практически весь трек идет с понижением высоты. Этот маршрут называют также &quot;Великая тропа пенсионеров&quot;, вероятно по тому, что он не сильно протяженный и идет большую часть времени под горку. Есть еще один из вариант этого маршрута: Радищево - Снегири, но мы решили финишировать на платформе 50-й километр, так как лыжня подходит здесь прямо к платформе. Если финишировать в снегирях, то придется идти через поселок, тем самым сглаживая впечатление от катания по лесу привкусом цивилизации :-)</p>

<p>.<br />
<br />
<br />
<a href="http://ic.pics.livejournal.com/pavel_lyakhov/14975806/209770/209770_original.jpg" target="_blank"><img alt="" src="http://ic.pics.livejournal.com/pavel_lyakhov/14975806/209770/209770_800.jpg" style="border-style:initial; border-width:0px; height:auto !important; margin:0px" /></a></p>

<p>&nbsp;</p>

<p>Грамотные пропилы завалов :-)<br />
<a href="http://ic.pics.livejournal.com/pavel_lyakhov/14975806/210156/210156_original.jpg" target="_blank"><img alt="" src="http://ic.pics.livejournal.com/pavel_lyakhov/14975806/210156/210156_800.jpg" style="border-style:initial; border-width:0px; height:auto !important; margin:0px" /></a></p>

<p>&nbsp;</p>

<p>Сарайчик всетаки не выдержал ледяного дождя.<br />
<a href="http://ic.pics.livejournal.com/pavel_lyakhov/14975806/210235/210235_original.jpg" target="_blank"><img alt="" src="http://ic.pics.livejournal.com/pavel_lyakhov/14975806/210235/210235_800.jpg" style="border-style:initial; border-width:0px; height:auto !important; margin:0px" /></a></p>

<p>&nbsp;</p>

<p>Да, кстати со старым новым годом !<br />
<a href="http://ic.pics.livejournal.com/pavel_lyakhov/14975806/210556/210556_original.jpg" target="_blank"><img alt="" src="http://ic.pics.livejournal.com/pavel_lyakhov/14975806/210556/210556_800.jpg" style="border-style:initial; border-width:0px; height:auto !important; margin:0px" /></a></p>

<p>&nbsp;</p>

<p>На этот раз ложку забыл я :-)<br />
<a href="http://ic.pics.livejournal.com/pavel_lyakhov/14975806/210795/210795_original.jpg" target="_blank"><img alt="" src="http://ic.pics.livejournal.com/pavel_lyakhov/14975806/210795/210795_800.jpg" style="border-style:initial; border-width:0px; height:auto !important; margin:0px" /></a></p>

<p>&nbsp;</p>

<p>Ну как же без костра! [Кстати,&nbsp;по причине того, что я люблю пожечь костер в походе я не покупаю себе новую подную куртку, так как если я закуплюсь новой мембранкой, мне придется ныкаться от огня и вообще использовать горелку.]&nbsp;<br />
<a href="http://ic.pics.livejournal.com/pavel_lyakhov/14975806/211133/211133_original.jpg" target="_blank"><img alt="" src="http://ic.pics.livejournal.com/pavel_lyakhov/14975806/211133/211133_800.jpg" style="border-style:initial; border-width:0px; height:auto !important; margin:0px" /></a></p>

<p>&nbsp;</p>

<p>ПВО-шный бугор с НУПом всетаки завалило - теперь его не только с самолета, но и с земли не разглядишь :-)<br />
<a href="http://ic.pics.livejournal.com/pavel_lyakhov/14975806/211275/211275_original.jpg" target="_blank"><img alt="" src="http://ic.pics.livejournal.com/pavel_lyakhov/14975806/211275/211275_800.jpg" style="border-style:initial; border-width:0px; height:auto !important; margin:0px" /></a><br />
<br />
<br />
<a href="http://ic.pics.livejournal.com/pavel_lyakhov/14975806/211663/211663_original.jpg" target="_blank"><img alt="" src="http://ic.pics.livejournal.com/pavel_lyakhov/14975806/211663/211663_800.jpg" style="border-style:initial; border-width:0px; height:auto !important; margin:0px" /></a><br />
<br />
<br />
<a href="http://ic.pics.livejournal.com/pavel_lyakhov/14975806/211724/211724_original.jpg" target="_blank"><img alt="" src="http://ic.pics.livejournal.com/pavel_lyakhov/14975806/211724/211724_800.jpg" style="border-style:initial; border-width:0px; height:auto !important; margin:0px" /></a></p>

<p>&nbsp;</p>

<p>Триумфальная арка!<br />
<a href="http://ic.pics.livejournal.com/pavel_lyakhov/14975806/211999/211999_original.jpg" target="_blank"><img alt="" src="http://ic.pics.livejournal.com/pavel_lyakhov/14975806/211999/211999_800.jpg" style="border-style:initial; border-width:0px; height:auto !important; margin:0px" /></a><br />
<br />
<a href="http://ic.pics.livejournal.com/pavel_lyakhov/14975806/212433/212433_original.jpg" target="_blank"><img alt="" src="http://ic.pics.livejournal.com/pavel_lyakhov/14975806/212433/212433_800.jpg" style="border-style:initial; border-width:0px; height:auto !important; margin:0px" /></a></p>

<p>&nbsp;</p>

<p>Мост!<br />
<a href="http://ic.pics.livejournal.com/pavel_lyakhov/14975806/212515/212515_original.jpg" target="_blank"><img alt="" src="http://ic.pics.livejournal.com/pavel_lyakhov/14975806/212515/212515_800.jpg" style="border-style:initial; border-width:0px; height:auto !important; margin:0px" /></a><br />
&nbsp;</p>

<p>Лестница в небо!</p>

<p><a href="http://ic.pics.livejournal.com/pavel_lyakhov/14975806/214664/214664_original.jpg" target="_blank"><img alt="" src="http://ic.pics.livejournal.com/pavel_lyakhov/14975806/214664/214664_800.jpg" style="border-style:initial; border-width:0px; height:auto !important; margin:0px" /></a></p>

<p>&nbsp;</p>

<p>Новогодняя красавица!<br />
<a href="http://ic.pics.livejournal.com/pavel_lyakhov/14975806/212809/212809_original.jpg" target="_blank"><img alt="" src="http://ic.pics.livejournal.com/pavel_lyakhov/14975806/212809/212809_800.jpg" style="border-style:initial; border-width:0px; height:auto !important; margin:0px" /></a></p>

<p>&nbsp;</p>

<p>Лэнд арт!<br />
<a href="http://ic.pics.livejournal.com/pavel_lyakhov/14975806/213240/213240_original.jpg" target="_blank"><img alt="" src="http://ic.pics.livejournal.com/pavel_lyakhov/14975806/213240/213240_800.jpg" style="border-style:initial; border-width:0px; height:auto !important; margin:0px" /></a></p>

<p>&nbsp;</p>

<p>Адская еловая лапища!!!<br />
<a href="http://ic.pics.livejournal.com/pavel_lyakhov/14975806/213494/213494_original.jpg" target="_blank"><img alt="" src="http://ic.pics.livejournal.com/pavel_lyakhov/14975806/213494/213494_800.jpg" style="border-style:initial; border-width:0px; height:auto !important; margin:0px" /></a></p>

<p>&nbsp;</p>

<p>Вот такой непролазный лес практически везде. Азимутом не пройдешь, или пролезешь но очень очень медленно.<br />
<a href="http://ic.pics.livejournal.com/pavel_lyakhov/14975806/213711/213711_original.jpg" target="_blank"><img alt="" src="http://ic.pics.livejournal.com/pavel_lyakhov/14975806/213711/213711_800.jpg" style="border-style:initial; border-width:0px; height:auto !important; margin:0px" /></a><br />
<br />
<br />
<a href="http://ic.pics.livejournal.com/pavel_lyakhov/14975806/213940/213940_original.jpg" target="_blank"><img alt="" src="http://ic.pics.livejournal.com/pavel_lyakhov/14975806/213940/213940_800.jpg" style="border-style:initial; border-width:0px; height:auto !important; margin:0px" /></a><br />
<br />
<br />
<a href="http://ic.pics.livejournal.com/pavel_lyakhov/14975806/214139/214139_original.jpg" target="_blank"><img alt="" src="http://ic.pics.livejournal.com/pavel_lyakhov/14975806/214139/214139_800.jpg" style="border-style:initial; border-width:0px; height:auto !important; margin:0px" /></a><br />
<br />
Лыжная инсталляция!<br />
<a href="http://ic.pics.livejournal.com/pavel_lyakhov/14975806/214299/214299_original.jpg" target="_blank"><img alt="" src="http://ic.pics.livejournal.com/pavel_lyakhov/14975806/214299/214299_800.jpg" style="border-style:initial; border-width:0px; height:auto !important; margin:0px" /></a><br />
&nbsp;</p>

<p>&nbsp;</p>

<h2>Трэк лыжни Радищево - пл 50 км (январь 2017)</h2>

<p>Трэк можно скачать здесь (gpx):&nbsp;<a href="https://yadi.sk/d/PacyXRfhSWfzzg">https://yadi.sk/d/PacyXRfhSWfzzg&nbsp;</a></p>

<p><iframe frameborder="0" height="600" scrolling="no" src="https://www.strava.com/activities/831666826/embed/b365d170463204a705704742dd711400445e5104" width="800"></iframe></p>
