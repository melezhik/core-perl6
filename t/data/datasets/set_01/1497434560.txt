<h1>Майский трип на ММБ с заходом в Рязань.</h1>

<p>Так как на майские праздники в связи с затянувшейся зимой и навалившимися делами и финансовым кризисом мне никуда не удалось по серьезному сходить в поход -&nbsp; на предстоящую деловую командировку в Рязань на конференцию RUS-Lasa 2017 у меня сформировались некоторые авантюристические планы.<br />
<br />
Это история о том как можно одновременно успеть быть двумя личностями и успеть на три уходящих поезда :-)))<br />
Итак по порядку: я обычно стараюсь не пропускать ежегодные ММБ (Московский Марш Бросок, который проходит весной и осенью. Соревнования по спортивному ориентированию, с ночным этапом с пятницы на субботу. Примерная дистанция чуть больше 100 км за вечер пятницы + сб. и вскр.), но в этом году старт ММБ совпал с моей командировкой в Рязань.<br />
<br />
Естественно, побывать я хотел и в Рязани и на ММБ - но как разорваться? Расклад был такой: конференция идет с 18 по 20 мая, старт ММБ вечером 19 мая. С первого взгляда понятно, что надо выбрать что-то одно, но по счастливому стечению обстоятельств решили меня командировать до 19 мая. Но билеты заказывали с работы и заказывали с запасом, так что я бы оказался в Москве в 11 вечера - естественно при таком раскладе я бы не успел на старт. На старт мы заявились командой и если бы я не успел, то был бы дисквалифицирован на старте с последующим уменьшением моего рейтинга. А заявка с моей кандидатурой на участие в марш-броске была уже подана. В итоге я решил не заказывать заранее обратный билет в Москву а купить его в Рязани по факту завершения моих командировочных задач. Но об этом позже.<br />
<br />
<br />
<br />
<a href="http://ic.pics.livejournal.com/drtg_ru/81658786/9583/9583_original.jpg" target="_blank"><img alt="" src="http://ic.pics.livejournal.com/drtg_ru/81658786/9583/9583_800.jpg" style="border-style:none; height:auto !important" /></a><br />
<br />
До Рязани я доехал на комфортабельном двухэтажном поезде с вайфаем и едой как в самолете. Доехал одним словом козырно и с музыкой :-)<br />
<br />
<a href="http://ic.pics.livejournal.com/drtg_ru/81658786/9752/9752_original.jpg" target="_blank"><img alt="" src="http://ic.pics.livejournal.com/drtg_ru/81658786/9752/9752_800.jpg" style="border-style:none; height:auto !important" /></a><br />
<br />
Сойдя с поезда, вооружившись компасом и Гармином я без труда по Первомайскому проспекту добрался до гостиницы, сэкономив на такси, осмотревшись, приметив по пути магазины, где можно затариться едой.<br />
<br />
<a href="http://ic.pics.livejournal.com/drtg_ru/81658786/10189/10189_original.jpg" target="_blank"><img alt="" src="http://ic.pics.livejournal.com/drtg_ru/81658786/10189/10189_800.jpg" style="border-style:none; height:auto !important" /></a><br />
<br />
В гостинице я минут 15 повалялся на огромной кровати, отдыхая от домашнего шума и беспорядка. После ломанулся в РязГУ организовывать выставку оборудования для работы с лабораторными животными. В этот раз организационные вопросы были решены превосходно и около 2 часов дня я освободился.<br />
<br />
<a href="http://ic.pics.livejournal.com/drtg_ru/81658786/10253/10253_original.jpg" target="_blank"><img alt="" src="http://ic.pics.livejournal.com/drtg_ru/81658786/10253/10253_800.jpg" style="border-style:none; height:auto !important" /></a><br />
<br />
Не теряя ни минуты я отправился на обзорную прогулку по Рязани. Маршрут я простроил заранее и залил в вышеупомянутый Гармин Дакота 20. Знакомстово с городом я обычно начинаю с длинного маршрута по улицам, заканчивающимся в каком нибудь знаковом месте. В Рязани этим местом по моему плану был &nbsp;Рязанский кремль.<br />
<br />
Город мне понравился чистотой и не многолюдностью. Вероятно, отсутствие людей было спровоцировано моросящим весь день дождем.<br />
<br />
<a href="http://ic.pics.livejournal.com/drtg_ru/81658786/10668/10668_original.jpg" target="_blank"><img alt="" src="http://ic.pics.livejournal.com/drtg_ru/81658786/10668/10668_800.jpg" style="border-style:none; height:auto !important" /></a><br />
<br />
При малоэтажной застройке, улицы были довольно широкие, что безусловно радовало.<br />
<br />
<a href="http://ic.pics.livejournal.com/drtg_ru/81658786/10974/10974_original.jpg" target="_blank"><img alt="" src="http://ic.pics.livejournal.com/drtg_ru/81658786/10974/10974_800.jpg" style="border-style:none; height:auto !important" /></a><br />
<br />
К сожалению я забыл сфотографировать знаменитый рязанский троллейбус, выкрашенный оливковой краской. Не знаю, только я ли это приметил, но мне определенно понравилась эта фишка с троллейбусами цвета хаки.<br />
<br />
<a href="http://ic.pics.livejournal.com/drtg_ru/81658786/11247/11247_original.jpg" target="_blank"><img alt="" src="http://ic.pics.livejournal.com/drtg_ru/81658786/11247/11247_800.jpg" style="border-style:none; height:auto !important" /></a><br />
<br />
Церковь иконы Божией Матери &quot;Всецарица&quot; - фотографировал из за забора.<br />
<a href="http://ic.pics.livejournal.com/drtg_ru/81658786/11312/11312_original.jpg" target="_blank"><img alt="" src="http://ic.pics.livejournal.com/drtg_ru/81658786/11312/11312_800.jpg" style="border-style:none; height:auto !important" /></a><br />
<br />
<br />
Городские многоэтажки тут иногда перемежаются с отживающими деревянными анахронизмами, что впрочем довольно мило.<br />
<a href="http://ic.pics.livejournal.com/drtg_ru/81658786/11611/11611_original.jpg" target="_blank"><img alt="" src="http://ic.pics.livejournal.com/drtg_ru/81658786/11611/11611_800.jpg" style="border-style:none; height:auto !important" /></a><br />
<br />
Стрит арт н развалинах старого дома в центре города. Далее по ходу моей импровизированной экскурсии я поднял из заросших илом глубин моей памяти ряд выдающихся личностей, таких как Павлов, Есенин, Солженицын.<br />
<a href="http://ic.pics.livejournal.com/drtg_ru/81658786/11958/11958_original.jpg" target="_blank"><img alt="" src="http://ic.pics.livejournal.com/drtg_ru/81658786/11958/11958_800.jpg" style="border-style:none; height:auto !important" /></a><br />
<br />
Первым по пути мне встретился памятник Ивану Петровичу Павлову, родившемуся в Рязани. Да, тому самому Павлову&nbsp;первому русскому нобелевскому лауреату (в области медицины и физиологии 1904 года &laquo;за работу по физиологии пищеварения&raquo;), физиологу, создателю науки о высшей нервной деятельности, физиологической школы. Короче: тому самому Павлову с собаками и рефлексами из школьного курса биологии :-)<br />
<a href="http://ic.pics.livejournal.com/drtg_ru/81658786/12603/12603_original.jpg" target="_blank"><img alt="" src="http://ic.pics.livejournal.com/drtg_ru/81658786/12603/12603_800.jpg" style="border-style:none; height:auto !important" /></a><br />
<br />
Потом еще один памятник - богатырю Евпатию Коловрату. Я заинтересовался этой личностью увидев памятник. Позже я нашел о Коловрате информацию в интернете. Привожу краткую историю из Википедии:</p>

<p><em>Евпа́тий Коловра́т (около 1200 &mdash; до 11 января 1238) &mdash; рязанский боярин, воевода и русский богатырь.</em></p>

<p><em>Родился, по преданию, в селе Фролово Шиловской волости. Находясь в Чернигове, с посольством с просьбой о помощи Рязанскому княжеству против монголов и узнав об их вторжении в Рязанское княжество, Евпатий Коловрат с &laquo;малою дружиною&raquo; спешно двинулся в Рязань. Но застал город уже разорённым. Тут к нему присоединились уцелевшие, и с отрядом в 1700 человек Евпатий пустился в погоню за монголами. Настигнув их в Суздальских землях, внезапной атакой полностью истребил их арьергард. Изумлённый Батый послал против Евпатия богатыря Хостоврула, брата своей жены. Хостоврул обещал Батыю привести Евпатия Коловрата живым, но погиб в поединке с ним. Несмотря на огромный численный перевес татар, в ходе ожесточённой битвы Евпатий Коловрат.</em></p>

<p><em>Есть предание, что посланец Батыя, отправленный на переговоры, спросил у Евпатия: &laquo;Чего вы хотите?&raquo;. И получил ответ &mdash; &laquo;Только умереть!&raquo;. Согласно некоторым преданиям, монголам удалось уничтожить отряд Евпатия только с помощью камнемётных орудий, предназначенных для разрушения укреплений: &laquo;И навадиша на него множество пороков, и нача бити по нем ис тмочисленых пороков, и едва убиша его&raquo;. Поражённый отчаянной смелостью, мужеством и воинским искусством рязанского богатыря, Батый, сказав &laquo;О, Евпатий! Если б ты у меня служил, я держал тебя у самого сердца!&raquo;, отдал тело убитого Евпатия Коловрата оставшимся в живых рязанским воинам и, в знак уважения к их мужеству, повелел отпустить их, не причиняя им никакого вреда.</em></p>

<p>.<br />
<br />
<a href="http://ic.pics.livejournal.com/drtg_ru/81658786/12979/12979_original.jpg" target="_blank"><img alt="" src="http://ic.pics.livejournal.com/drtg_ru/81658786/12979/12979_800.jpg" style="border-style:none; height:auto !important" /></a><br />
<br />
Следующая мемориальная табличка на фасаде рязанской школы №2 дала возможность мне вспомнить о другом лауреате Нобелевской премии - Александре Исаевиче Солженицыне, который жил там с 1957 года.<br />
<br />
<a href="http://ic.pics.livejournal.com/drtg_ru/81658786/13219/13219_original.jpg" target="_blank"><img alt="" src="http://ic.pics.livejournal.com/drtg_ru/81658786/13219/13219_800.jpg" style="border-style:none; height:auto !important" /></a><br />
<br />
Еще немного и я дошел до кремля и принялся осматривать архитектурные достопримечательности.<br />
<br />
<a href="http://ic.pics.livejournal.com/drtg_ru/81658786/13378/13378_original.jpg" target="_blank"><img alt="" src="http://ic.pics.livejournal.com/drtg_ru/81658786/13378/13378_800.jpg" style="border-style:none; height:auto !important" /></a><br />
<br />
Ландшафтно-архитектурный ансамбль Рязанского кремля занимает довольно большую территорию. Привожу здесь схему музея-заповедника. &quot;Рязанский кремль&quot;.<br />
<a href="http://ic.pics.livejournal.com/drtg_ru/81658786/22377/22377_original.jpg" target="_blank"><img alt="" src="http://ic.pics.livejournal.com/drtg_ru/81658786/22377/22377_800.jpg" style="border-style:none; height:auto !important" /></a><br />
<br />
<a href="http://ic.pics.livejournal.com/drtg_ru/81658786/22902/22902_original.jpg" target="_blank"><img alt="" src="http://ic.pics.livejournal.com/drtg_ru/81658786/22902/22902_800.jpg" style="border-style:none; height:auto !important" /></a><br />
<br />
Дальше мое повествование будет в виде тезисного фотоотчета:<br />
<br />
Успенский собор<br />
<a href="http://ic.pics.livejournal.com/drtg_ru/81658786/13778/13778_original.jpg" target="_blank"><img alt="" src="http://ic.pics.livejournal.com/drtg_ru/81658786/13778/13778_800.jpg" style="border-style:none; height:auto !important" /></a><br />
<br />
Христорождественский собор.<br />
<a href="http://ic.pics.livejournal.com/drtg_ru/81658786/14032/14032_original.jpg" target="_blank"><img alt="" src="http://ic.pics.livejournal.com/drtg_ru/81658786/14032/14032_800.jpg" style="border-style:none; height:auto !important" /></a><br />
<br />
Гостиница знати.<br />
<a href="http://ic.pics.livejournal.com/drtg_ru/81658786/14220/14220_original.jpg" target="_blank"><img alt="" src="http://ic.pics.livejournal.com/drtg_ru/81658786/14220/14220_800.jpg" style="border-style:none; height:auto !important" /></a><br />
<br />
Гостиница черни.<br />
<a href="http://ic.pics.livejournal.com/drtg_ru/81658786/16528/16528_original.jpg" target="_blank"><img alt="" src="http://ic.pics.livejournal.com/drtg_ru/81658786/16528/16528_800.jpg" style="border-style:none; height:auto !important" /></a><br />
<br />
На крепостной насыпи.<br />
<a href="http://ic.pics.livejournal.com/drtg_ru/81658786/14476/14476_original.jpg" target="_blank"><img alt="" src="http://ic.pics.livejournal.com/drtg_ru/81658786/14476/14476_800.jpg" style="border-style:none; height:auto !important" /></a><br />
<br />
Вид на Рязань с кремлевской насыпи.<br />
<a href="http://ic.pics.livejournal.com/drtg_ru/81658786/14837/14837_original.jpg" target="_blank"><img alt="" src="http://ic.pics.livejournal.com/drtg_ru/81658786/14837/14837_800.jpg" style="border-style:none; height:auto !important" /></a><br />
<br />
<a href="http://ic.pics.livejournal.com/drtg_ru/81658786/15261/15261_original.jpg" target="_blank"><img alt="" src="http://ic.pics.livejournal.com/drtg_ru/81658786/15261/15261_800.jpg" style="border-style:none; height:auto !important" /></a><br />
<br />
Кремль с другого ракурса. Также с тегом <a href="http://www.livejournal.com/rsearch/?tags=%23nopeope" target="_self">#nopeope</a><br />
<a href="http://ic.pics.livejournal.com/drtg_ru/81658786/15077/15077_original.jpg" target="_blank"><img alt="" src="http://ic.pics.livejournal.com/drtg_ru/81658786/15077/15077_800.jpg" style="border-style:none; height:auto !important" /></a><br />
<br />
<br />
<br />
<br />
Раскопки<br />
<a href="http://ic.pics.livejournal.com/drtg_ru/81658786/15377/15377_original.jpg" target="_blank"><img alt="" src="http://ic.pics.livejournal.com/drtg_ru/81658786/15377/15377_800.jpg" style="border-style:none; height:auto !important" /></a><br />
<br />
Далее куча фото с территории Рязанского кремля. Люблю фотоотчеты с большим количеством фото и делаю так же :-)<br />
<a href="http://ic.pics.livejournal.com/drtg_ru/81658786/15854/15854_original.jpg" target="_blank"><img alt="" src="http://ic.pics.livejournal.com/drtg_ru/81658786/15854/15854_800.jpg" style="border-style:none; height:auto !important" /></a><br />
<br />
<a href="http://ic.pics.livejournal.com/drtg_ru/81658786/16013/16013_original.jpg" target="_blank"><img alt="" src="http://ic.pics.livejournal.com/drtg_ru/81658786/16013/16013_800.jpg" style="border-style:none; height:auto !important" /></a><br />
<br />
<a href="http://ic.pics.livejournal.com/drtg_ru/81658786/16261/16261_original.jpg" target="_blank"><img alt="" src="http://ic.pics.livejournal.com/drtg_ru/81658786/16261/16261_800.jpg" style="border-style:none; height:auto !important" /></a><br />
<br />
<br />
<br />
<a href="http://ic.pics.livejournal.com/drtg_ru/81658786/16858/16858_original.jpg" target="_blank"><img alt="" src="http://ic.pics.livejournal.com/drtg_ru/81658786/16858/16858_800.jpg" style="border-style:none; height:auto !important" /></a><br />
<br />
<a href="http://ic.pics.livejournal.com/drtg_ru/81658786/17000/17000_original.jpg" target="_blank"><img alt="" src="http://ic.pics.livejournal.com/drtg_ru/81658786/17000/17000_800.jpg" style="border-style:none; height:auto !important" /></a><br />
<br />
<a href="http://ic.pics.livejournal.com/drtg_ru/81658786/17220/17220_original.jpg" target="_blank"><img alt="" src="http://ic.pics.livejournal.com/drtg_ru/81658786/17220/17220_800.jpg" style="border-style:none; height:auto !important" /></a><br />
<br />
<a href="http://ic.pics.livejournal.com/drtg_ru/81658786/17442/17442_original.jpg" target="_blank"><img alt="" src="http://ic.pics.livejournal.com/drtg_ru/81658786/17442/17442_800.jpg" style="border-style:none; height:auto !important" /></a><br />
<br />
<br />
<br />
<br />
<br />
<br />
Памятник Сергею Есенину<br />
<br />
<a href="http://ic.pics.livejournal.com/drtg_ru/81658786/18995/18995_original.jpg" target="_blank"><img alt="" src="http://ic.pics.livejournal.com/drtg_ru/81658786/18995/18995_800.jpg" style="border-style:none; height:auto !important" /></a><br />
<br />
<br />
<br />
И в заключении нашего хит-парада памятников - Рязанский Ленин!<br />
<a href="http://ic.pics.livejournal.com/drtg_ru/81658786/19531/19531_original.jpg" target="_blank"><img alt="" src="http://ic.pics.livejournal.com/drtg_ru/81658786/19531/19531_800.jpg" style="border-style:none; height:auto !important" /></a><br />
<br />
<br />
Гостиница, где я останавливался.<br />
<a href="http://ic.pics.livejournal.com/drtg_ru/81658786/20215/20215_original.jpg" target="_blank"><img alt="" src="http://ic.pics.livejournal.com/drtg_ru/81658786/20215/20215_800.jpg" style="border-style:none; height:auto !important" /></a><br />
<br />
<br />
Ночная прогулка по безлюдным улицам.<br />
<a href="http://ic.pics.livejournal.com/drtg_ru/81658786/20944/20944_original.jpg" target="_blank"><img alt="" src="http://ic.pics.livejournal.com/drtg_ru/81658786/20944/20944_800.jpg" style="border-style:none; height:auto !important" /></a><br />
<br />
Мимо этой вывески я не смог пройти равнодушно. Почему Чебурашка? Секрет здесь: <a href="https://youtu.be/ZcWGnszLp20" target="_self">https://youtu.be/ZcWGnszLp20</a><br />
<a href="http://ic.pics.livejournal.com/drtg_ru/81658786/21170/21170_original.jpg" target="_blank"><img alt="" src="http://ic.pics.livejournal.com/drtg_ru/81658786/21170/21170_800.jpg" style="border-style:none; height:auto !important" /></a><br />
В заключительный день командировки произошел сбой в работе мобильного оператора Мегафон, в результате чего телефонной связи не было. В результате этого были некоторые накладки. Но находчивость и смекалка помогли мне наилучшим образом завершить дела. До вокзала я бежал бегом от здания РязГМУ, так как по причине сбоев на мегафоне мобильного интернета не было, а времени на поиски машины не оставалось. В итоге я успел добежать до вокзала Рязань-2 и купил билет на поезд за 10 минут до его отъезда в Москву. Костюм, ноутбук, цивильные туфли я отправил транспортной компанией в офис, так как таскать всё это в рюкзаке по болотам и буеракам было бы тяжко.<br />
<br />
<br />
<br />
Еду в Москву на ММБ!!!!<br />
<a href="http://ic.pics.livejournal.com/drtg_ru/81658786/21706/21706_original.jpg" target="_blank"><img alt="" src="http://ic.pics.livejournal.com/drtg_ru/81658786/21706/21706_800.jpg" style="border-style:none; height:auto !important" /></a><br />
В Москву на Казанский вокзал я прибыл за сорок минут до отправления электрики до Акри. Бег с препятствиями в час-пик с рюкзаком - и опа! - я на Павелецком вокзале - запыхавшись, с дикими глазами спрашивая &quot;услугами какого мобильного оператора вы пользуетесь??!!&quot; - &nbsp;прошу у первого встречного телефон с НеМегафоном, созваниваюсь с товарищами, и успеваю на электричку, которая ушла на пять минут позже, чем их электричка. Они ждут меня на платформе Акри минут пять. Я приезжаю на следующей электричке. и Ура!!! мы в полном составе идем на старт!!!!<br />
<br />
<br />
<br />
<strong>ММБ осень 2017!</strong><br />
Дальше все пошло своим чередом - блуждание по ночным лесам в поисках КП, бег за светящимися &quot;паровозами&quot; лидеров.<br />
<br />
<a href="http://ic.pics.livejournal.com/drtg_ru/81658786/31846/31846_original.jpg" target="_blank"><img alt="" src="http://ic.pics.livejournal.com/drtg_ru/81658786/31846/31846_800.jpg" style="border-style:none; height:auto !important" /></a><br />
<br />
Этот ММБ был менее брутальный чем осенний. Тем не менее было классно! Вдоволь нагулялись по лесу, помесили грязь, поплавали в лужах.<br />
<br />
Первый этап - ночное ориентирование. Погода отличная.<br />
<a href="http://ic.pics.livejournal.com/drtg_ru/81658786/23651/23651_original.jpg" target="_blank"><img alt="" src="http://ic.pics.livejournal.com/drtg_ru/81658786/23651/23651_800.jpg" style="border-style:none; height:auto !important" /></a><br />
<br />
Часть нашего пути пролегала вдоль закрытой территории, обнесенной колючей проволокой. В лесу много кабанов. Видели пару раз выводок молодых кабанят с огромной мамашей. Самая жуткая встреча с кабанами была в ночном лесу, когда стадо этих огромных тварей пронеслась буквально перед нашими носами. Поняв, что дело может обернуться не лучшим для нас образом - если мы все таки столкнемся с ними лицом к лицу, решено было петь песни, чтобы отпугнуть животных. Пришлось напрячь мозги, вспоминая песни приличные и не очень.<br />
<a href="http://ic.pics.livejournal.com/drtg_ru/81658786/23962/23962_original.jpg" target="_blank"><img alt="" src="http://ic.pics.livejournal.com/drtg_ru/81658786/23962/23962_800.jpg" style="border-style:none; height:auto !important" /></a><br />
<br />
<br />
<a href="http://ic.pics.livejournal.com/drtg_ru/81658786/24252/24252_original.jpg" target="_blank"><img alt="" src="http://ic.pics.livejournal.com/drtg_ru/81658786/24252/24252_800.jpg" style="border-style:none; height:auto !important" /></a><br />
<br />
<br />
<a href="http://ic.pics.livejournal.com/drtg_ru/81658786/24358/24358_original.jpg" target="_blank"><img alt="" src="http://ic.pics.livejournal.com/drtg_ru/81658786/24358/24358_800.jpg" style="border-style:none; height:auto !important" /></a><br />
<br />
По пути встречалось много живописных мест.<br />
<a href="http://ic.pics.livejournal.com/drtg_ru/81658786/24675/24675_original.jpg" target="_blank"><img alt="" src="http://ic.pics.livejournal.com/drtg_ru/81658786/24675/24675_800.jpg" style="border-style:none; height:auto !important" /></a><br />
<br />
<br />
<a href="http://ic.pics.livejournal.com/drtg_ru/81658786/25032/25032_original.jpg" target="_blank"><img alt="" src="http://ic.pics.livejournal.com/drtg_ru/81658786/25032/25032_800.jpg" style="border-style:none; height:auto !important" /></a><br />
<br />
Коллекция фото корявых деревьев с ММБ весна 2017:<br />
<a href="http://ic.pics.livejournal.com/drtg_ru/81658786/25140/25140_original.jpg" target="_blank"><img alt="" src="http://ic.pics.livejournal.com/drtg_ru/81658786/25140/25140_800.jpg" style="border-style:none; height:auto !important" /></a><br />
<br />
Каких только форм не встретишь за сто километровую прогулку по лесу!<br />
<a href="http://ic.pics.livejournal.com/drtg_ru/81658786/25719/25719_original.jpg" target="_blank"><img alt="" src="http://ic.pics.livejournal.com/drtg_ru/81658786/25719/25719_800.jpg" style="border-style:none; height:auto !important" /></a><br />
<br />
<a href="http://ic.pics.livejournal.com/drtg_ru/81658786/25957/25957_original.jpg" target="_blank"><img alt="" src="http://ic.pics.livejournal.com/drtg_ru/81658786/25957/25957_800.jpg" style="border-style:none; height:auto !important" /></a></p>

<table>
	<tbody>
		<tr>
			<td><a href="http://ic.pics.livejournal.com/drtg_ru/81658786/28070/28070_original.jpg" target="_blank"><img alt="" src="http://ic.pics.livejournal.com/drtg_ru/81658786/28070/28070_800.jpg" style="border-style:none; height:auto !important; width:392px" /></a></td>
			<td><a href="http://ic.pics.livejournal.com/drtg_ru/81658786/28332/28332_original.jpg" target="_blank"><img alt="" src="http://ic.pics.livejournal.com/drtg_ru/81658786/28332/28332_800.jpg" style="border-style:none; height:auto !important; width:392px" /></a></td>
		</tr>
	</tbody>
</table>

<p><br />
<br />
Смена карт. Обед. Все происходящее:погода, время, приготовление пищи, ландшафт было &nbsp;похоже как &nbsp;на смене карт ММБ весной 2016-м. ДеЖаВю!!!<br />
<a href="http://ic.pics.livejournal.com/drtg_ru/81658786/26272/26272_original.jpg" target="_blank"><img alt="" src="http://ic.pics.livejournal.com/drtg_ru/81658786/26272/26272_800.jpg" style="border-style:none; height:auto !important" /></a><br />
<br />
Русские березки.<br />
<a href="http://ic.pics.livejournal.com/drtg_ru/81658786/26423/26423_original.jpg" target="_blank"><img alt="" src="http://ic.pics.livejournal.com/drtg_ru/81658786/26423/26423_800.jpg" style="border-style:none; height:auto !important" /></a><br />
<br />
Грязь - наше все!!!! Без нее ММБ не ММБ :-)<br />
<a href="http://ic.pics.livejournal.com/drtg_ru/81658786/26686/26686_original.jpg" target="_blank"><img alt="" src="http://ic.pics.livejournal.com/drtg_ru/81658786/26686/26686_800.jpg" style="border-style:none; height:auto !important" /></a><br />
<br />
<br />
<a href="http://ic.pics.livejournal.com/drtg_ru/81658786/26946/26946_original.jpg" target="_blank"><img alt="" src="http://ic.pics.livejournal.com/drtg_ru/81658786/26946/26946_800.jpg" style="border-style:none; height:auto !important" /></a><br />
<br />
<br />
<br />
Ящерица веретенка. О том что это ящерица я узнал уже после мероприятия. А при встрече с ней мы были уверены, что это ядовитая змея.<br />
<a href="http://ic.pics.livejournal.com/drtg_ru/81658786/27418/27418_original.jpg" target="_blank"><img alt="" src="http://ic.pics.livejournal.com/drtg_ru/81658786/27418/27418_800.jpg" style="border-style:none; height:auto !important" /></a><br />
<br />
<br />
<a href="http://ic.pics.livejournal.com/drtg_ru/81658786/27782/27782_original.jpg" target="_blank"><img alt="" src="http://ic.pics.livejournal.com/drtg_ru/81658786/27782/27782_800.jpg" style="border-style:none; height:auto !important" /></a><br />
<br />
<br />
<br />
<a href="http://ic.pics.livejournal.com/drtg_ru/81658786/28457/28457_original.jpg" target="_blank"><img alt="" src="http://ic.pics.livejournal.com/drtg_ru/81658786/28457/28457_800.jpg" style="border-style:none; height:auto !important" /></a><br />
<br />
<br />
<a href="http://ic.pics.livejournal.com/drtg_ru/81658786/28720/28720_original.jpg" target="_blank"><img alt="" src="http://ic.pics.livejournal.com/drtg_ru/81658786/28720/28720_800.jpg" style="border-style:none; height:auto !important" /></a><br />
<br />
<br />
<a href="http://ic.pics.livejournal.com/drtg_ru/81658786/28970/28970_original.jpg" target="_blank"><img alt="" src="http://ic.pics.livejournal.com/drtg_ru/81658786/28970/28970_800.jpg" style="border-style:none; height:auto !important" /></a><br />
<br />
<br />
<br />
<a href="http://ic.pics.livejournal.com/drtg_ru/81658786/29277/29277_original.jpg" target="_blank"><img alt="" src="http://ic.pics.livejournal.com/drtg_ru/81658786/29277/29277_800.jpg" style="border-style:none; height:auto !important" /></a><br />
<br />
<br />
<a href="http://ic.pics.livejournal.com/drtg_ru/81658786/29647/29647_original.jpg" target="_blank"><img alt="" src="http://ic.pics.livejournal.com/drtg_ru/81658786/29647/29647_800.jpg" style="border-style:none; height:auto !important" /></a><br />
<br />
<br />
<a href="http://ic.pics.livejournal.com/drtg_ru/81658786/29853/29853_original.jpg" target="_blank"><img alt="" src="http://ic.pics.livejournal.com/drtg_ru/81658786/29853/29853_800.jpg" style="border-style:none; height:auto !important" /></a><br />
<br />
Дорога, неумолимо идет через болото.<br />
<a href="http://ic.pics.livejournal.com/drtg_ru/81658786/30181/30181_original.jpg" target="_blank"><img alt="" src="http://ic.pics.livejournal.com/drtg_ru/81658786/30181/30181_800.jpg" style="border-style:none; height:auto !important" /></a><br />
<br />
У одного из КП я соорудил инсталляцию из подобранных неподалеку костей.<br />
<a href="http://ic.pics.livejournal.com/drtg_ru/81658786/30422/30422_original.jpg" target="_blank"><img alt="" src="http://ic.pics.livejournal.com/drtg_ru/81658786/30422/30422_800.jpg" style="border-style:none; height:auto !important" /></a><br />
<br />
<br />
<a href="http://ic.pics.livejournal.com/drtg_ru/81658786/30688/30688_original.jpg" target="_blank"><img alt="" src="http://ic.pics.livejournal.com/drtg_ru/81658786/30688/30688_800.jpg" style="border-style:none; height:auto !important" /></a><br />
<br />
<br />
<a href="http://ic.pics.livejournal.com/drtg_ru/81658786/30888/30888_original.jpg" target="_blank"><img alt="" src="http://ic.pics.livejournal.com/drtg_ru/81658786/30888/30888_800.jpg" style="border-style:none; height:auto !important" /></a><br />
<br />
<br />
<a href="http://ic.pics.livejournal.com/drtg_ru/81658786/31192/31192_original.jpg" target="_blank"><img alt="" src="http://ic.pics.livejournal.com/drtg_ru/81658786/31192/31192_800.jpg" style="border-style:none; height:auto !important" /></a><br />
<br />
<br />
<a href="http://ic.pics.livejournal.com/drtg_ru/81658786/31300/31300_original.jpg" target="_blank"><img alt="" src="http://ic.pics.livejournal.com/drtg_ru/81658786/31300/31300_800.jpg" style="border-style:none; height:auto !important" /></a><br />
<br />
<br />
<a href="http://ic.pics.livejournal.com/drtg_ru/81658786/31740/31740_original.jpg" target="_blank"><img alt="" src="http://ic.pics.livejournal.com/drtg_ru/81658786/31740/31740_800.jpg" style="border-style:none; height:auto !important" /></a><br />
<br />
<br />
<a href="http://ic.pics.livejournal.com/drtg_ru/81658786/32142/32142_original.jpg" target="_blank"><img alt="" src="http://ic.pics.livejournal.com/drtg_ru/81658786/32142/32142_800.jpg" style="border-style:none; height:auto !important" /></a><br />
<br />
Если первый день был относительно сухой, то на второй день выкупались по самые бубенцы!<br />
<img alt="" src="http://ic.pics.livejournal.com/drtg_ru/81658786/32614/32614_900.jpg" style="border-style:none; height:auto !important; width:800px" /><br />
<br />
<iframe frameborder="0" height="450" src="https://www.youtube.com/embed/kCLnzk1sWtw?rel=0&amp;controls=0&amp;showinfo=0" width="800"></iframe><br />
<img alt="" src="http://ic.pics.livejournal.com/drtg_ru/81658786/32999/32999_900.jpg" style="border-style:none; height:auto !important; width:800px" /><br />
<br />
<a href="http://ic.pics.livejournal.com/drtg_ru/81658786/27384/27384_original.jpg" target="_blank"><img alt="" src="http://ic.pics.livejournal.com/drtg_ru/81658786/27384/27384_800.jpg" style="border-style:none; height:auto !important" /></a><br />
<br />
Взятие последнего КП было фееричным апрофеозом грязеориентирования, грязешлнпанья и грязекупания.<br />
<br />
<iframe frameborder="0" height="450" src="https://www.youtube.com/embed/sWRPCCy8MjE?rel=0&amp;controls=0&amp;showinfo=0" width="800"></iframe><br />
<a href="http://ic.pics.livejournal.com/drtg_ru/81658786/32273/32273_original.jpg" target="_blank"><img alt="" src="http://ic.pics.livejournal.com/drtg_ru/81658786/32273/32273_800.jpg" style="border-style:none; height:auto !important" /></a><br />
Вот так и погуляли!<br />
Отличное мероприятие - рекомендую всем кто хочет испытать свои силы и способности в ориентировании на местности!<br />
<br />
&nbsp;</p>

<h2>Ссылка на gps трэк ММБ-весна 2017</h2>

<p><br />
Трэк можно скачать здесь (gpx): <a href="https://yadi.sk/d/rHl-Inm13KBrLX" target="_self">https://yadi.sk/d/LCZb6F2K3GqaGQ</a></p>

<p><iframe frameborder="0" height="405" scrolling="no" src="https://www.strava.com/activities/999468562/embed/08f57e1f39c1b6a34aa754c591461d712962f4d2" width="800"></iframe></p>

<p><strong>МЕТКИ: </strong><a href="http://drtg-ru.livejournal.com/tag/%23nopeope" target="_self">#nopeope</a>, <a href="http://drtg-ru.livejournal.com/tag/%23nopeople" target="_self">#nopeople</a>, <a href="http://drtg-ru.livejournal.com/tag/%23%D0%BC%D0%BC%D0%B1" target="_self">#ммб</a>, <a href="http://drtg-ru.livejournal.com/tag/%23%D0%BC%D0%BC%D0%B1%D0%B2%D0%B5%D1%81%D0%BD%D0%B02017" target="_self">#ммбвесна2017</a>, <a href="http://drtg-ru.livejournal.com/tag/%D1%80%D1%8F%D0%B7%D0%B0%D0%BD%D1%8C" target="_self">рязань</a></p>
