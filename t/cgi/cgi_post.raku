#!/usr/bin/env raku

use lib './lib';

use Test;
use Pheix::Addons::November::CGI;

my $cgi = Pheix::Addons::November::CGI.new;

sub MAIN($t_param) {
    print is-deeply( $cgi.params, EVAL( %*ENV<TEST_RESULT> ), $t_param )
        ?? "success "
        !! "failure \n" ~ "got: " ~ $cgi.param.perl ~
            "\nexpected: " ~ %*ENV<TEST_RESULT> ~ "\n";
    say "- " ~ %*ENV<TEST_NAME>;
}
