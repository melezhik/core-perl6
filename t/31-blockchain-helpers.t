use v6.d;
use Test;
use Test::Mock;
use lib 'lib';

use Net::Ethereum;
use Pheix::Test::Blockchain;
use Pheix::Test::BlockchainComp::Helpers;

plan 1;

constant blank    = '0x0000000000000000000000000000000000000000000000000000000000000000';
constant excptn   = '0xffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff';
constant trx      = '0x97e92abe8de919f6e7f79105b534690f88704d8dc7c81b805cc3410baad30e1a';
constant sc       = '0xe6df2905589db3c09091462f771ed0a3b820017c';
constant acc      = '0x9dc4c65c12d81f741ae57ac7abc3a02a342625a4';

class MockedBlockchain {
    has Net::Ethereum $.ethobj;
}

class MockedDatabase {
    has MockedBlockchain $.chainobj;
}

my $ne = mocked(
    Net::Ethereum,
    returning => {
        contract_method_call_estimate_gas => 3_000_000,
        sendTransaction => trx,
        retrieve_contract => sc,
        eth_accounts => [acc],
        eth_getTransactionByHash => {blockNumber => 1},
        eth_getTransactionReceipt => {status => 1},
        debug_traceTransaction => {failure => False}
    },
    overriding => {
        debug_traceTransaction => -> :$dbobj, :$trx, :$diag = True {
            die 'emulate exception for ' ~ $trx if $trx eq excptn;

            $trx eq blank ?? Any !! {failure => False};
        }
    }
);

my MockedDatabase $dbobj =
    MockedDatabase.new(:chainobj(MockedBlockchain.new(:ethobj($ne))));

my Pheix::Test::BlockchainComp::Helpers $helpobj =
    Pheix::Test::BlockchainComp::Helpers.new(
        :testnet(Str),
        :localtab(Str),
        :debug(False),
        :tstobj(Pheix::Test::Blockchain.new)
    );

subtest {
    plan 5;

    my $trace = $helpobj.trace_transaction(:dbobj($dbobj), :trx(trx), :diag(True));

    ok ($trace<trx>:exists) && ($trace<receipt>:exists) && ($trace<trace>:exists), 'basic keys in trace';

    is ($helpobj.trace_transaction(:dbobj($dbobj), :trx(Str), :diag(False)))<error>,
        'invalid trx hash: ',
            'error on null transaction hash';

    is ($helpobj.trace_transaction(:dbobj($dbobj), :trx('not hex trx hash'), :diag(False)))<error>,
        'invalid trx hash: not hex trx hash',
            'error on invalid transaction hash';

    is ($helpobj.trace_transaction(:dbobj($dbobj), :trx(blank), :diag(False)))<trace><status>,
        'null trace',
            'status null trace';

    is ($helpobj.trace_transaction(:dbobj($dbobj), :trx(excptn), :diag(False)))<trace><error>,
        'emulate exception for ' ~ excptn,
            'exception message in trace';
}, 'Check trace transaction method';

done-testing;
