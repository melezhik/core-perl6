use v6.d;
use Test;
use lib 'lib';

plan 1;

use Pheix::View::Web::Headers;

# Extended check for proto-sn method
subtest {
    plan 1;
    my Any $obj = Pheix::View::Web::Headers.new(
        addon => 'FooBar'
    );
    my Str $p = $obj.proto_sn;
    if %*ENV<SERVER_NAME> {
        is(
            $p,
            'http://foo.bar',
            'proto-sn with referer=' ~
                %*ENV<SERVER_NAME> ~ ' for unknown addon',
        );
    }
    else {
        skip '%*ENV<SERVER_NAME> is not set', 1;
    }
}, 'Extended check for proto-sn method';

done-testing;
