use v6.d;
use Test;
use lib 'lib';

plan 1;

use Pheix::View::Template;

my Str $t_cont = qq~
Sed ut perspiciatis, unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam eaque ipsa, qua
e ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt, explicabo. Nemo enim ipsam voluptatem, quia voluptas sit,
aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos, qui ratione voluptatem sequi nesciunt, neque porro quisq uam
est, qui dolorem ipsum, quia dolor sit, amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt, ut labor
e et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laborio
sam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit, qui in ea voluptate velit esse, quam nihil mo
lestiae consequatur, vel illum, qui dolorem eum fugiat, quo voluptas nulla pariatur? At vero eos et accusamus et iusto odio dignissi
mos ducimus, qui blanditiis praesentium voluptatum deleniti atque corrupti, quos dolores et quas molestias excepturi sint, obcaecati
cupiditate non provident, similique sunt in culpa, qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum qui
dem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi optio, cumque nihil impedit, quo min
us id, quod maxime placeat, facere possimus, omnis voluptas assumenda est, omnis dolor repellendus. Temporibus autem quibusdam et au
t officiis debitis aut rerum necessitatibus saepe eveniet, ut et voluptates repudiandae sint et molestiae non recusandae. Itaque ear
um rerum hic tenetur a sapiente delectus, ut aut reiciendis voluptatibus maiores alias consequatur aut perferendis doloribus asperio
res repellat.
~;

my Pheix::View::Template $obj =
    Pheix::View::Template.new(:uitempl_indx('./t/tmp.txt'));

# Check render method
subtest {
    plan 5;
    is(
        $obj.render(:type(Str), :vars(%(Nil))),
        Empty,
        'render() with not existed template file',
    );
    ok(
        _create_test_file($obj.uitempl_indx),
        'create template file ' ~ $obj.uitempl_indx,
    );
    is(
        $obj.render(:type(Str), :vars(%(Nil))),
        $t_cont,
        'render() with template file ' ~ $obj.uitempl_indx,
    );
    ok(
        _delete_test_file($obj.uitempl_indx),
        'delete template file ' ~ $obj.uitempl_indx,
    );

    $obj = Pheix::View::Template.new(:uitempl_indx(Str));
    is(
        $obj.render(:type(Str), :vars(%(Nil))),
        Empty,
        'render() without template file (Str) and empty message',
    );
}, 'Check render method';

sub _create_test_file returns Bool {
    my ($fname) = @_;
    my Bool $ret = False;
    if $fname {
        if $fname.IO ~~ :e {
           unlink $fname;
        }
        else {
            my $fh = open $fname, :w;
            $fh.print($t_cont);
            $fh.close;
        }
        if $fname.IO.e {
            $ret = True;
            $obj = Pheix::View::Template.new(
                :uitempl_indx('./t/tmp.txt'),
            );
        }
    }
    $ret;
}

sub _delete_test_file returns Bool {
    my ($fname) = @_;
    my Bool $ret = True;
    if $fname {
        if $fname.IO ~~ :e {
            unlink $fname;
        } else {
            $ret = False;
        }
    } else {
        $ret = False;
    }
    $ret;
}

done-testing;
