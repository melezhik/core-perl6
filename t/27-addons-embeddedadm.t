use v6.d;
use Test;
use lib 'lib';

use Pheix::Test::Content;
use Pheix::Test::FastCGI;
use Pheix::Test::Helpers;
use Pheix::Controller::Basic;
use Pheix::Addons::Embedded::Admin;
use Pheix::Test::Blockchain;

constant localtab = 'blockchain';
constant tstobj   = Pheix::Test::Blockchain.new(:locstorage(localtab), :genwords(200));

my $tcnt = Pheix::Test::Content.new;
my $fcgi = Pheix::Test::FastCGI.new;
my $thlp = Pheix::Test::Helpers.new;
my $ctrl = Pheix::Controller::Basic.new(
    :apirobj(Nil),
    :mockedfcgi($fcgi),
    :test(True),
);

plan 7;

use-ok 'Pheix::Addons::Embedded::Admin';

# Check browse method for login
subtest {
    plan 4;

    my $addon = Pheix::Addons::Embedded::Admin.new(:ctrl($ctrl), :confpath('conf/addons/custom_path'));

    my %match =
        details    => %(path => '/admin'),
        controller => $addon.get_class,
        action     => 'browse',
    ;

    is $ctrl.sharedobj<fastcgi>.reset, 0, 'reset content array';

    $addon.browse(:tick(1), :match(%match));

    my ($hdr, $cnt) = $ctrl.sharedobj<fastcgi>.fetch.split("\n\n", 2);

    ok $cnt !~~ / '<TMPL_VAR tmpl_' <[a..z_]>+ '>' /, 'no tmpl vars';

    ok $tcnt.checkheader(
        :header($hdr),
        :status(200),
        :cookies([
            'fcgitick=' ~ 1,
            'pheixaddon=' ~ $addon.get_name,
            'pheixadmin=' ~ True.Str
        ])
    ), 'HTTP header';

    ok $tcnt.checkcontent(
        :cnt($cnt),
        :params(
            %(
                $ctrl.sharedobj<pageobj>.get_pparams,
                $ctrl.sharedobj<pageobj>.get_tparams
            )
        )
    ), 'check content for login page';
}, 'Check browse method for login';

# Check trivial auth failure - no node available
subtest {
    plan 2;

    my $addon = Pheix::Addons::Embedded::Admin.new(:ctrl($ctrl), :confpath('conf/addons/custom_path'));

    my %auth_details =
        $addon.auth_api(:match({}), :tick(1), :sharedobj($ctrl.sharedobj));

    nok %auth_details<tparams><pheixauth>, 'pheixauth is undefined';
    is %auth_details<tparams><table>, 'embeddedadmin/login', 'login table';
}, 'Check trivial auth failure - no node available';

# Ethereum node is required for the next tests
if !tstobj.pte {
    diag('PHEIXTESTENGINE was not set');
    skip-rest('Blockchain test should be run via Pheix test engine');
    done-testing;

    exit;
}

# Check trivial auth, exit and validation
subtest {
    plan 14;

    my $addon = Pheix::Addons::Embedded::Admin.new(:ctrl($ctrl), :confpath('conf/addons/custom_path'));
    my $dbobj = Pheix::Model::Database::Access.new(
        :table(localtab),
        :fields([]),
        :test(True)
    );

    if $dbobj {
        if $dbobj.dbswitch {
            ok $dbobj.dbswitch, 'blockchain tab connected';

            my @accounts = $dbobj.chainobj.ethobj.eth_accounts;

            ok @accounts.elems, 'ethereum accounts';

            my %hdr = $ctrl.sharedobj<headobj>.header(
                %(Status => 200, X-Request-ID => 1),
                List.new,
            );

            my %auth_details = $addon.auth_api(
                :match({}),
                :tick(1),
                :sharedobj($ctrl.sharedobj),
                :credentials({login => @accounts.head, password => $dbobj.chainobj.ethobj.unlockpwd}),
                :header(%hdr),
            );

            ok %auth_details<tparams><pkey> ~~ m:i/^ 0x<xdigit>**64 $/ , 'pkey found';
            ok %auth_details<tparams><tx> ~~ m:i/^ 0x<xdigit>**64 $/ , 'tx found';
            is %auth_details<tparams><table>, 'embeddedadmin/area', 'area table';
            is %auth_details<tparams><session>, 60, 'one minute per session';
            is %auth_details<header><Set-Cookie>.elems, 2, 'cookies found';
            ok %auth_details<header><Set-Cookie>.head ~~ m:i/ <{%auth_details<tparams><tx>}> /, 'pkey cookie';
            ok %auth_details<header><Set-Cookie>.tail ~~ m:i/ <{@accounts.head}> /, 'sender cookie';
            ok $ctrl.sharedobj<logrobj>.chainobj.table_drop, 'clear log';
            ok %auth_details<tparams><status>, 'session is authenticated';

            $ctrl.sharedobj<fastcgi>.set_env(
                :key('HTTP_COOKIE'),
                :value(sprintf("pheixauth=%s; pheixsender=%s", %auth_details<tparams><tx>, @accounts.head)),
            );

            my %exit_details = $addon.manage_session_api(
                :match({}),
                :route('/api/session/close'),
                :tick(1),
                :sharedobj($ctrl.sharedobj),
            );

            ok %exit_details.keys.elems, 'exit response';

            my %validate_details = $addon.manage_session_api(
                :match({}),
                :route('/api/session/validate'),
                :tick(1),
                :sharedobj($ctrl.sharedobj),
            );

            ok %validate_details<sesstatus>:exists, 'sesstatus presented';
            ok %validate_details<sesstatus> == False, 'sesstatus is false';

            # diag(%validate_details.gist);
        }
        else {
            skip-rest('ethereum node is not available');
        }
    }
    else {
        skip-rest('database object is not available');
    }
}, 'Check trivial auth, exit and validation';

# Check auth, validation, extending and exit
subtest {
    plan 16;

    my $addon = Pheix::Addons::Embedded::Admin.new(:ctrl($ctrl), :confpath('conf/addons/custom_path'));
    my $dbobj = Pheix::Model::Database::Access.new(
        :table(localtab),
        :fields([]),
        :test(True)
    );

    if $dbobj {
        if $dbobj.dbswitch {
            my %details;

            ok $dbobj.dbswitch, 'blockchain tab connected';

            my @accounts = $dbobj.chainobj.ethobj.eth_accounts;

            ok @accounts.elems, 'ethereum accounts';

            %details<auth> = $addon.auth_api(
                :match({}),
                :tick(1),
                :sharedobj($ctrl.sharedobj),
                :credentials({login => @accounts.head, password => $dbobj.chainobj.ethobj.unlockpwd}),
            );

            ok %details<auth><tparams><pkey> ~~ m:i/^ 0x<xdigit>**64 $/ , 'pkey found';
            ok %details<auth><tparams><tx> ~~ m:i/^ 0x<xdigit>**64 $/ , 'tx found';
            ok %details<auth><tparams><status>, 'session is authenticated';

            $ctrl.sharedobj<fastcgi>.set_env(
                :key('HTTP_COOKIE'),
                :value(sprintf("pheixauth=%s; pheixsender=%s", %details<auth><tparams><tx>, @accounts.head)),
            );

            react {
                whenever Supply.interval(10) -> $step {
                    %details<validate>.push: $addon.manage_session_api(
                        :match({}),
                        :route('/api/session/validate'),
                        :tick(1),
                        :sharedobj($ctrl.sharedobj),
                    );

                    diag(sprintf("validate session at step %d: %s", $step, (%details<validate>.tail)<sesstatus>))
                        if tstobj.debug;

                    if (%details<validate>.tail)<tryextend> {
                        %details<extend> = $addon.manage_session_api(
                            :match({}),
                            :route('/api/session/extend'),
                            :tick(1),
                            :sharedobj($ctrl.sharedobj),
                        );

                        $ctrl.sharedobj<fastcgi>.set_env(
                            :key('HTTP_COOKIE'),
                            :value(sprintf("pheixauth=%s; pheixsender=%s", %details<extend><tparams><tx>, @accounts.head)),
                        );

                        diag(sprintf("extend session at step %d: %s ", $step, %details<extend><sesstatus>))
                            if tstobj.debug;
                    }

                    done() if $step == 8;
                }
            }

            %details<close> = $addon.manage_session_api(
                :match({}),
                :route('/api/session/close'),
                :tick(1),
                :sharedobj($ctrl.sharedobj),
            );

            %details<validate>.push: $addon.manage_session_api(
                :match({}),
                :route('/api/session/validate'),
                :tick(1),
                :sharedobj($ctrl.sharedobj),
            );

            for %details<validate>.kv -> $index, $data {
                last if $index == %details<validate>.elems - 1;

                ok $data<sesstatus>, 'session is valid for step ' ~ $index;
            }

            ok ((%details<validate>.tail)<sesstatus>:exists) && (%details<validate>.tail)<sesstatus> == False, 'session is closed';
            ok $ctrl.sharedobj<logrobj>.chainobj.table_drop, 'clear log';
        }
        else {
            skip-rest('ethereum node is not available');
        }
    }
    else {
        skip-rest('database object is not available');
    }
}, 'Check auth, validation, extending and exit';

# Check session validation
subtest {
    plan 9;

    my $addon = Pheix::Addons::Embedded::Admin.new(:ctrl($ctrl), :confpath('conf/addons/custom_path'));
    my $dbobj = Pheix::Model::Database::Access.new(
        :table(localtab),
        :fields([]),
        :test(True)
    );

    if $dbobj {
        if $dbobj.dbswitch {
            my %details = stats => {valid => 0, invalid => 0};

            ok $dbobj.dbswitch, 'blockchain tab connected';

            my @accounts = $dbobj.chainobj.ethobj.eth_accounts;

            ok @accounts.elems, 'ethereum accounts';

            %details<auth> = $addon.auth_api(
                :match({}),
                :tick(1),
                :sharedobj($ctrl.sharedobj),
                :credentials({login => @accounts.head, password => $dbobj.chainobj.ethobj.unlockpwd}),
            );

            ok %details<auth><tparams><pkey> ~~ m:i/^ 0x<xdigit>**64 $/ , 'pkey found';
            ok %details<auth><tparams><tx> ~~ m:i/^ 0x<xdigit>**64 $/ , 'tx found';
            ok %details<auth><tparams><status>, 'session is authenticated';

            $ctrl.sharedobj<fastcgi>.set_env(
                :key('HTTP_COOKIE'),
                :value(sprintf("pheixauth=%s; pheixsender=%s", %details<auth><tparams><tx>, @accounts.head)),
            );

            react {
                whenever Supply.interval(10) -> $step {
                    %details<validate>.push: $addon.manage_session_api(
                        :match({}),
                        :route('/api/session/validate'),
                        :tick(1),
                        :sharedobj($ctrl.sharedobj),
                    );

                    diag(sprintf("validate session at step %d: %s", $step, (%details<validate>.tail)<sesstatus>))
                        if tstobj.debug;

                    done() if $step == 8;
                }
            }

            for @(%details<validate>) -> $data {
                $data<sesstatus> ??
                    %details<stats><valid>++ !!
                        %details<stats><invalid>++;
            }

            %details<close> = $addon.manage_session_api(
                :match({}),
                :route('/api/session/close'),
                :tick(1),
                :sharedobj($ctrl.sharedobj),
            );

            ok %details<close>.keys.elems, 'exit response';
            is %details<stats><valid>, 6, sprintf("session is valid for %s", $addon.sesstimeout);
            is %details<stats><invalid>, 3, sprintf("session is lost after %s", $addon.sesstimeout);
            ok $ctrl.sharedobj<logrobj>.chainobj.table_drop, 'clear log';
        }
        else {
            skip-rest('ethereum node is not available');
        }
    }
    else {
        skip-rest('database object is not available');
    }
}, 'Check session validation';

# Check catch contract feature
subtest {
    plan 8;

    my $addon = Pheix::Addons::Embedded::Admin.new(:ctrl($ctrl), :confpath('conf/addons/custom_path'));
    my $dbobj = Pheix::Model::Database::Access.new(
        :table(localtab),
        :fields([]),
        :test(True)
    );

    if $dbobj {
        if $dbobj.dbswitch {
            my %details = stats => {valid => 0, invalid => 0};

            ok $dbobj.dbswitch, 'blockchain tab connected';

            my @accounts = $dbobj.chainobj.ethobj.eth_accounts;

            ok @accounts.elems, 'ethereum accounts';

            %details<auth>.push: $addon.auth_api(
                :match({}),
                :tick(1),
                :sharedobj($ctrl.sharedobj),
                :credentials({login => @accounts.head, password => $dbobj.chainobj.ethobj.unlockpwd}),
            );

            ok %details<auth>.tail<tparams><pkey> ~~ m:i/^ 0x<xdigit>**64 $/ , 'pkey found';
            ok %details<auth>.tail<tparams><tx> ~~ m:i/^ 0x<xdigit>**64 $/ , 'tx found';
            ok %details<auth>.tail<tparams><status>, 'session is authenticated';

            $ctrl.sharedobj<fastcgi>.set_env(
                :key('HTTP_COOKIE'),
                :value(sprintf("pheixauth=%s; pheixsender=%s", %details<auth>.tail<tparams><tx>, @accounts.head)),
            );

            %details<auth>.pop;

            react {
                whenever Supply.interval(10) -> $step {
                    %details<auth>.push: $addon.auth_api(
                        :match({}),
                        :tick(1),
                        :sharedobj($ctrl.sharedobj),
                        :credentials({login => @accounts.head, password => $dbobj.chainobj.ethobj.unlockpwd}),
                    );

                    diag(sprintf("catch contract at step %d for %d", $step, (%details<auth>.tail<tparams><session>)))
                        if tstobj.debug;

                    done() if $step == 5;
                }
            }

            for @(%details<auth>) -> $data {
                 %details<stats><catchseconds> += $data<tparams><session>
                    if $data<tparams><status>;
            }

            %details<close> = $addon.manage_session_api(
                :match({}),
                :route('/api/session/close'),
                :tick(1),
                :sharedobj($ctrl.sharedobj),
            );

            ok %details<close>.keys.elems, 'exit response';
            is %details<stats><catchseconds>, (1..6).map({$_ * 10}).sum, 'catch valid num of seconds';
            ok $ctrl.sharedobj<logrobj>.chainobj.table_drop, 'clear log';
        }
        else {
            skip-rest('ethereum node is not available');
        }
    }
    else {
        skip-rest('database object is not available');
    }
}, 'Check catch contract feature';

done-testing;
