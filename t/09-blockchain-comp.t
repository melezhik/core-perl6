# Specific dataset deployment
# raku ./t/06-blockchain-comp.t local deploy ./t/data/datasets/set_08/1598179320.txt embedded/

use v6.d;
use Test;
use lib 'lib';

use Net::Ethereum;
use Pheix::Model::Database::Access;
use Pheix::Test::Blockchain;
use Pheix::Test::BlockchainComp::Helpers;
use Pheix::Utils;

plan 3;

constant localtab   = 'tst_table';
constant tstobj     = Pheix::Test::Blockchain.new(:locstorage(localtab));
constant debug      = tstobj.debug;
constant debuglevel = tstobj.debuglevel;
constant fulltest   = tstobj.fulltest;

constant logfname = 'tx-sign.06-blockchain-comp.log';
constant testnet  =
    ((@*ARGS[0].defined && @*ARGS[0] (elem) <ropsten rinkeby goerli>) || tstobj.testnet) ??
        @*ARGS[0] // tstobj.testnet !!
            localtab;

constant dsdeploy = ((@*ARGS[1] // q{}) eq 'deploy') ?? True !! False;
constant ds       = (@*ARGS[2] && @*ARGS[2].IO.e) ?? @*ARGS[2] !! Str;
constant tabprefx = ((@*ARGS[3] // q{}) ne q{}) ?? @*ARGS[3] !! Str;
constant skipurge = ((@*ARGS[4] // q{}) ~~ m:i/true/) ?? True !! False;
constant target   = ((@*ARGS[5] // q{}) ~~ m:i/true/) ?? True !! False;
constant dictsz   = ((@*ARGS[6] // q{}) ~~ m:i/^<[\d]>+$/) ?? @*ARGS[6].Int !! 200000;
constant helpobj  = Pheix::Test::BlockchainComp::Helpers.new(
    :testnet(testnet),
    :localtab(localtab),
    :debuglevel(debuglevel),
    :tstobj(tstobj),
    :dictsz(dictsz)
);

my Str  $targettb = dsdeploy ?? target_tab_name(:name(ds), :pfx(tabprefx)) !! localtab;

my UInt $frlen    = (testnet ne localtab && !dsdeploy) ?? 2048 !! 4096;
my UInt $tab_size = 10;
my Int  $tbot_inx = fulltest ?? -1 !! 0;
my Int  $ttop_inx = fulltest ?? -1 !! (testnet eq localtab ?? 10 !! 2);
my Str  $dspath   = './t/data/datasets';
my Str  $utf8smpl = './t/data/utf8demo.txt';
my Str  $diagnet  = testnet eq localtab ?? 'local PoA net' !! testnet ~ ' net';
my Str  $tab      = testnet eq localtab ?? localtab !! testnet ~ '_storage';
my      @farr     = <domains ip_addrs browsers resolut pages countries payload>;
my      @tabs     = <table_1 table_2 table_3>;

my Any  $dbobj    = Pheix::Model::Database::Access.new(
    :table($tab),
    :fields(@farr),
    :test(True),
    :utils(Pheix::Utils.new(:test(True), :lzwsz(helpobj.dictsz))),
);

if !tstobj.pte {
    diag('PHEIXTESTENGINE was not set');
    skip-rest('Blockchain test should be run via Pheix test engine');
    exit;
}

if $dbobj && $dbobj.dbswitch == 1 {
    $dbobj.chainobj.ethobj.tx_wait_iters = 60;

    $dbobj.chainobj.ethobj.tx_wait_sec =
        tstobj.is_local(:storage(testnet)) ??
            tstobj.local_wait !!
                tstobj.public_wait;

    $dbobj.chainobj.nounlk = True if tstobj.is_public(:storage(testnet));

    helpobj.diag(:l(tstobj.logwrn), :m('Run tests on ' ~ $diagnet));
    helpobj.diag(:l(tstobj.logwrn), :m('Compression dict size ' ~ (helpobj.dictsz)));
}

# Compression flag setter and getter loop test
my $subtest_1 = subtest(
    'Compression flag setter and getter loop test on ' ~ $diagnet ~ ', tab: ' ~ $tab,
    sub {
        plan 4;
        if $dbobj && !dsdeploy {
            if $dbobj.dbswitch == 1 {
                is $dbobj.chainobj.unlock_account, True, tstobj.bm(
                    :ts('s1: account unlock'),
                    :return(True)
                );

                my $tabs_on_blockchain = $dbobj.chainobj.count_tables;

                if $tabs_on_blockchain {
                    is tstobj.drop_all(:dbobj($dbobj)), True, tstobj.bm(
                        :ts('s1: drop ' ~ $tabs_on_blockchain ~ ' tabs'),
                        :return(True)
                    );
                }
                else {
                    ok $tabs_on_blockchain == 0,
                        tstobj.bm(:ts('s1: empty blockchain'), :return(True));
                }

                is ($dbobj.chainobj.table_debug(:waittx(True)))<status>,
                    True,
                        tstobj.bm(:ts('s1: init internal db'), :return(True));

                is(
                    random_set_get_cmp_test(
                        :dbobj($dbobj),
                        :iters(5),
                        :debug(debug)
                    ),
                    True,
                    tstobj.bm(:ts('s1: random cmp set/get'), :return(True)),
                );
            }
            else {
                skip-rest('ethereum node is not available');
            }
        }
        else {
           skip-rest(
            dsdeploy ??
                'cmp flag setter/getter test on deploy' !!
                    'database object is not available'
           );
        }
    }
).Bool;

# Tables with independent records test
my $subtest_2 = subtest(
    'Tables with independent records test on ' ~ $diagnet ~ ', tab: ' ~ $tab,
    sub {
        plan 24;
        if $dbobj && !dsdeploy {
            if $dbobj.dbswitch == 1 {
                is $dbobj.chainobj.unlock_account, True,
                    tstobj.bm(:ts('s2: account unlock'), :return(True));

                my $tabs_on_blockchain = $dbobj.chainobj.count_tables;

                if $tabs_on_blockchain {
                    is tstobj.drop_all(:dbobj($dbobj)), True, tstobj.bm(
                        :ts('s1: drop ' ~ $tabs_on_blockchain ~ ' tabs'),
                        :return(True)
                    );
                }
                else {
                    ok $tabs_on_blockchain == 0,
                        tstobj.bm(:ts('s1: empty blockchain'), :return(True));
                }

                for ^10 {
                    my Str $f = helpobj.generate_content(:words(tstobj.genwords));

                    ok $f.chars > 100, tstobj.bm(
                        :ts('s2: generate utf8 data ' ~ $_),
                        :return(True)
                    );

                    helpobj.cmpobj.compress(:data($f));

                    my int $r = helpobj.cmpobj.get_ratio;

                    ok $r >= 0, tstobj.bm(
                        :ts('s2: utf8 data ' ~ $_ ~ ' cmp ratio ' ~  $r ~ q{%}),
                        :return(True)
                    );
                }

                my %database  = helpobj.create_database(
                    :dbobj($dbobj),
                    :tabnames(@tabs),
                    :fixedrows($tab_size)
                );

                helpobj.debug_database(:database(%database));

                is %database<status>, True, tstobj.bm(
                    :ts('s2: create database (' ~ %database<db>.elems ~ ' tabs)'),
                    :return(True)
                );

                my @blockchaindb = helpobj.validate_database(:dbobj($dbobj));

                is-deeply @(%database<db>), @blockchaindb, tstobj.bm(
                    :ts('s2: validate database'),
                    :return(True)
                );
            }
            else {
                skip-rest('ethereum node is not available');
            }
        }
        else {
            skip-rest(
             dsdeploy ??
                 'tabs with independent records test on deploy' !!
                     'database object is not available'
            );
        }
    }
).Bool;

# Tables with content datasets test
my $subtest_3 = subtest(
    'Tables with content datasets test on ' ~ $diagnet ~ ', tab: ' ~ $tab,
    sub {
        plan 4;
        my Any $dbobj_ = Pheix::Model::Database::Access.new(
            table  => $tab,
            fields => @('rowdata'),
            test   => True
        );

        if $dbobj_ {
            if $dbobj_.dbswitch == 1 {

                $dbobj_.chainobj.ethobj.tx_wait_sec =
                    tstobj.is_local(:storage(testnet)) ??
                        tstobj.local_wait !!
                            tstobj.public_wait;

                $dbobj_.chainobj.nounlk =
                    True if tstobj.is_public(:storage(testnet));

                is $dbobj_.chainobj.unlock_account, True, tstobj.bm(
                    :ts('s3: account unlock'),
                    :return(True)
                );

                my $tabs_on_blockchain = $dbobj_.chainobj.count_tables;

                if $tabs_on_blockchain {
                    if skipurge {
                        ok skipurge, 'do not clean blockchain';
                    }
                    else {
                        is tstobj.drop_all(:dbobj($dbobj_)), True, tstobj.bm(
                            :ts('s3: drop ' ~ $tabs_on_blockchain ~ ' tabs'),
                            :return(True)
                        );
                    }
                }
                else {
                    ok $tabs_on_blockchain == 0,
                        tstobj.bm(:ts('s3: empty blockchain'), :return(True));
                }

                my $cmp_ds_details = compress_datasets(
                    :dbobj($dbobj_),
                    :cmpobj(helpobj.cmpobj),
                    :comp(True),
                );

                ok $cmp_ds_details<status>,
                    tstobj.bm(:ts('s3: store datasets'), :return(True));

                if skipurge {
                    ok skipurge, 'do not validate datasets';
                }
                else {
                    is(
                        decompress_datasets(
                            :dbobj($dbobj_),
                            :cmpobj(helpobj.cmpobj),
                            :datasets($cmp_ds_details<processed_data>)
                        ), True,
                        tstobj.bm(:ts('s3: validate datasets'), :return(True)),
                    );
                }

            $dbobj.chainobj.sgnlog.push($dbobj_.chainobj.sgnlog.Slip);
            }
            else {
                skip-rest('ethereum node is not available');
            }
        }
        else {
           skip-rest('database object is not available');
        }
    }
).Bool;

logfname.IO.spurt: $dbobj.chainobj.sgnlog.join("\n")
    if $dbobj && $dbobj.dbswitch == 1;

done-testing;

# Datasets test subroutines
sub compress_datasets(
         :$dbobj!,
         :$cmpobj!,
    Bool :$comp!,
) returns Hash {
    my @ds = (!ds && $ttop_inx >= 0 && $tbot_inx >= 0 && $tbot_inx <= $ttop_inx) ??
                 get_datasets[$tbot_inx..$ttop_inx] !!
                     get_datasets(:ds(ds));

    my @actual_processed_data;

    for @ds.kv -> $inx, $ds {
        my Str $cnt = $ds.IO.slurp if $ds.IO.f;
        if $cnt {
            my Str  $data;

            my Str  $tnam = $ds.IO.basename;

            if (tabprefx && dsdeploy && ds) {
                (my $truefname = $ds.IO.basename) ~~ s:g:i/ \. <{$ds.IO.extension}> //;
                $tnam = tabprefx ~ $truefname;
            }
            else {
                $tnam ~~ s:g:i/\./_/;
            }

            my Bool $actcmp = False;

            if $comp {
                $data     = $cmpobj.compress(:data($cnt));
                my int $r = $cmpobj.get_ratio;

                my Str $dcmp = $cmpobj.decompress(:data($data));
                if $dcmp ne $cnt {
                    helpobj.diag(:l(tstobj.logerr), :m('comp<->decomp failure ' ~ $ds));

                    return {status => False};
                }
                else {
                    helpobj.diag(:l(tstobj.loginf), :m('comp<->decomp ok ' ~ $ds));
                }
                if $r < 0 {
                    $data = $cnt;
                }
                else {
                    $actcmp = True;

                    helpobj.diag(:l(tstobj.loginf), :m('{' ~ $inx ~ '} ' ~ $tnam ~ ' comp ratio ' ~ $r ~ '%'))
                }
            }
            else {
                $data = $cnt;
            }

            my @frms = dataset_to_frames(:dataset($data));

            if @frms {
                $dbobj.chainobj.unlock_account;
                $dbobj.chainobj.table_create(:t($tnam),:comp($actcmp));

                if $dbobj.chainobj.table_exists(:t($tnam)) {
                    my @processed;
                    my @skipped;
                    my @txs;

                    helpobj.diag(:l(tstobj.loginf), :m('tab ' ~ $tnam ~ ' is created'));

                    for @frms.kv -> $finx, $f {
                        my %data = rowdata => $f;

                        my %rhash = $dbobj.chainobj.row_insert(
                            :t($tnam),
                            :data(%data),
                            :comp(False),
                            :waittx(False),
                        );

                        if %rhash<status> && %rhash<error> eq q{} {
                            @txs.push(%rhash<txhash>);
                            @processed.push($f);
                        }
                        else {
                            @skipped.push(sprintf("%d/%d: %s", $finx + 1, @frms.elems, %rhash<error>));
                        }
                    }

                    if @frms.elems != @txs.elems && debug {
                        helpobj.diag(:l(tstobj.logwrn), :m(sprintf("frames for %s \{%d, %s\} were skipped:", $tnam, $inx, $ds)));

                        for @skipped -> $skipped_dataset {
                            helpobj.diag(:l(tstobj.logmsg), :m(sprintf("\t%s", $skipped_dataset)));
                        }
                    }

                    @actual_processed_data.push({
                        file    => $ds,
                        frames  => @processed,
                        skipped => @frms.elems != @txs.elems ?? 1 !! 0
                    });

                    if !$dbobj.chainobj.wait_for_transactions(:hashes(@txs)) && @txs {
                        for @txs.kv -> $index, $txhash {
                            my %h =
                                $dbobj.chainobj.ethobj.
                                    eth_getTransactionReceipt($txhash);

                            if %h<status>:!exists || %h<status> == 0 {
                                helpobj.diag(:l(tstobj.logerr), :m(sprintf("%s trx %s failure", &?ROUTINE.name, $txhash)));

                                helpobj.trace_transaction(:dbobj($dbobj), :trx($txhash));
                            }
                        }

                        return {status => False};
                    }
                    else {
                        if tabprefx && dsdeploy && ds {
                            helpobj.diag(:l(tstobj.logdpl), :m(@txs.join(q{,})));
                        }
                    }
                }
                else {
                    helpobj.diag(:l(tstobj.logerr), :m($tnam ~ ' create is failed!'));

                    return {status => False};
                }
            }
            else {
                helpobj.diag(:l(tstobj.logerr), :m('split to frames is failed!'));

                return {status => False};
            }
        }
        else {
            helpobj.diag(:l(tstobj.logerr), :m('dataset is empty!'));

            return {status => False};
        }
    }

    return {status => True, processed_data => @actual_processed_data};
}

# Datasets test subroutines
sub decompress_datasets(
         :$dbobj!,
         :$cmpobj!,
         :$datasets!
) returns Bool {
    for @$datasets -> $dataset_details {
        my Int $r   = 0;
        my $file    = $dataset_details<file>;
        my $frames  = $dataset_details<frames>;
        my $skipped = $dataset_details<skipped>;
        my Str $cnt = $frames.join(q{});

        my Str $tnam = $file.IO.basename;

        if (tabprefx && dsdeploy && ds) {
            (my $truefname = $file.IO.basename) ~~ s:g:i/ \. <{$file.IO.extension}> //;
            $tnam = tabprefx ~ $truefname;
        }
        else {
            $tnam ~~ s:g:i/\./_/;
        }

        $dbobj.chainobj.unlock_account;

        if $dbobj.chainobj.table_exists(:t($tnam)) {
            my @data;
            my UInt $rows = $dbobj.chainobj.count_rows(:t($tnam));

            for ^$rows -> $rinx {
                my $rowid =
                    $dbobj.chainobj.get_id_byindex(:t($tnam),:index($rinx));
                if $rowid > 0 {
                    my %row = $dbobj.chainobj.select(
                        :t($tnam),
                        :id($rowid),
                        :comp(False)
                    );
                    my $ts = $dbobj.fields.map({ %row{$_} }) if %row;

                    @data.push($ts);
                }
            }

            if @data {
                my Str  $chck;
                my Bool $ic = $dbobj.chainobj.is_tab_compressed(:t($tnam));

                if $ic && !$skipped {
                    my Str $data = @data.join(q{});

                    $chck = $cmpobj.decompress(:data($data));
                    $cnt  = $cmpobj.decompress(:data($cnt));
                }
                else {
                    $chck = @data.join(q{});
                }

                if $chck eq $cnt {
                     helpobj.diag(:l(tstobj.loginf), :m('{' ~ $file ~ '} ' ~ ($ic ?? 'compressed' !! 'native') ~ ' tab ' ~ $tnam ~ ' content looks good'));
                }
                else {
                    helpobj.diag(:l(tstobj.logerr), :m($tnam  ~ ' content failure'));

                    return False;
                }
            }
            else {
                if $cnt {
                    helpobj.diag(:l(tstobj.logerr), :m('table is empty!'));

                    return False;
                }
            }
        }
        else {
            helpobj.diag(:l(tstobj.logerr), :m($tnam ~ ' is not existed!'));

            return False;
        }
    }

    return True;
}

sub dataset_to_frames(Str :$dataset) returns List {
    my @frames;
    if $dataset.defined && $dataset ne q{} {
        my $ff  = ($dataset.chars / $frlen).ceiling;
        my $hf  = $dataset.chars % $frlen;

        for ^$ff -> $index {
            if $index < $ff {
                @frames.push($dataset.substr( $index * $frlen, $frlen));
            } else {
                @frames.push($dataset.substr( $index * $frlen, $hf));
            }
        }
    }

    return @frames;
}

sub get_datasets(Str :$ds) returns List {
    my @files;

    return [$ds] if target && $ds.IO.e && $ds.IO.f;

    my $dir  = $dspath;
    my @todo = $dir.IO;

    while @todo {
        for @todo.pop.dir -> $path {
            if $ds {
                if $path.f && $path.Str eq $ds {
                    @files.push($path.Str);
                }
            }
            else {
                @files.push($path.Str) if $path.f;
            }

            @todo.push: $path if $path.d;
        }
    }

    return @files
}

sub random_set_get_cmp_test(
         :$dbobj!,
    UInt :$iters!,
    Bool :$debug
) returns Bool {
    my Bool $rc   = True;
    my Bool $cmp  = True;
    my UInt $tabs = $dbobj.chainobj.count_tables;

    for ^$iters -> $iter {
        helpobj.diag(:l(tstobj.logdbg), :m('random_set_get_cmp_test: ' ~ ($iter + 1) ~ q{/} ~ $iters));

        my @states = (
            {
                t0 => False,
                r0 => False,
                r1 => False
            },
            {
                t1 => False,
                r0 => False,
                r1 => False
            },
            {
                t2 => False,
                r0 => False,
            }
        );

        my $rti = $tabs.rand.Int;

        for ^$tabs -> $tinx {
            $dbobj.chainobj.unlock_account;
            my Str $tnam = $dbobj.chainobj.get_tabname_byindex(:index($tinx));

            if $tinx == $rti {
                if $dbobj.chainobj.set_tab_cmp(
                   :t($tnam),
                   :comp($cmp),
                   :waittx(True)
                ) {
                    my Str $tkey = 't' ~ $tinx;
                    @states[$tinx]{$tkey} = True;
                }
                else {
                    return False;
                }
            }

            my UInt $rows = $dbobj.chainobj.count_rows(:t($tnam));
            my $rtri      = $rows.rand.Int;

            for ^$rows -> $rinx {
                my $rowid =
                    $dbobj.chainobj.get_id_byindex(:t($tnam), :index($rinx));

                if $rowid > 0 && $rinx == $rtri {
                    if $dbobj.chainobj.set_row_cmp(
                       :t($tnam),
                       :id($rowid),
                       :comp($cmp),
                       :waittx(True)
                    ) {
                        my Str $rkey = 'r' ~ $rinx;
                        @states[$tinx]{$rkey} = True;
                    }
                    else {
                        return False;
                    }
                }
            }
        }

        @states.gist.say if debug;

        for @states.kv -> $index, $state {
            my Str  $tkey = 't' ~ $index;
            my Bool $scmp = $state{$tkey};
            my Str  $tnam = $dbobj.chainobj.get_tabname_byindex(:index($index));
            my Bool $tcmp = $dbobj.chainobj.is_tab_compressed(:t($tnam));

            if $tcmp != $scmp {
                return False;
            }
            else {
                my UInt $rows = $dbobj.chainobj.count_rows(:t($tnam));

                for ^$rows -> $rinx {
                    my $rowid =
                        $dbobj.chainobj.get_id_byindex(:t($tnam),:index($rinx));

                    if $rowid > 0 {
                        my Str  $rkey = 'r' ~ $rinx;

                        my Bool $rcmp = $dbobj.chainobj.is_row_compressed(
                            :t($tnam),
                            :id($rowid)
                        );
                        my Bool $srcmp = $state{$rkey};

                        if $rcmp != $srcmp {
                            return False;
                        }
                    }
                }
            }
        }

        if !reset_compression(:cmp(False),:debug($debug)) {
            return False;
        }
    }
    $rc;
}

sub reset_compression(Bool :$cmp, Bool :$debug) returns Bool {
    helpobj.diag(:l(tstobj.logdbg), :m('reset compression to ' ~ $cmp ~ ': application level'));

    for @tabs.kv -> $index, $tnam {
        if $dbobj.chainobj.table_exists(:t($tnam)) {
            if $dbobj.chainobj.is_tab_compressed(:t($tnam)) {
                $dbobj.chainobj.unlock_account;

                if !$dbobj.chainobj.set_tab_cmp(
                   :t($tnam),
                   :comp($cmp),
                   :waittx(True)
                ) {
                    return False;
                }
                else {
                    helpobj.diag(:l(tstobj.logdbg), :m('reset cmp ' ~ $tnam));
                }
            }

            my UInt $rows = $dbobj.chainobj.count_rows(:t($tnam));

            for ^$rows -> $rinx {
                my $rowid =
                    $dbobj.chainobj.get_id_byindex(:t($tnam),:index($rinx));

                if $rowid > 0 {
                    if $dbobj.chainobj.is_row_compressed(
                        :t($tnam),
                        :id($rowid)
                    ) {
                        if !$dbobj.chainobj.set_row_cmp(
                           :t($tnam),
                           :id($rowid),
                           :comp($cmp),
                           :waittx(True)
                        ) {
                            return False;
                        }
                        else {
                            helpobj.diag(:l(tstobj.logdbg), :m('reset cmp ' ~ $tnam ~ ' row ' ~ $rowid));
                        }
                    }
                }
            }
        }
    }

    return True;
}

sub target_tab_name(Str :$name, Str :$pfx) returns Str {
    my Str $rs;

    if ($name ~~ m:i/ '/' (<[\da..z\_\-]>+) '.' <[a .. z]>/) {
        $rs = $pfx ~ $0;
        helpobj.diag(:l(tstobj.logdpl), :m('Target table name to deploy: ' ~ $rs));
    }
    else {
        die 'Target table name construct failed';
    }

    $rs;
}

if $dbobj {
    if $dbobj.dbswitch == 1 {
        tstobj.print_statistics if $subtest_1 && $subtest_2 && $subtest_3;
    }
}
