# Pheix CMS implemented in Raku

**Pheix CMS** is the Artistic License 2.0 compliant, [Raku](https://raku.org) driven Content Management System. It is extremely lightweight, simple and customizable.

## Borrowings

Pheix uses [CGI](https://github.com/viklund/november/blob/master/lib/November/CGI.pm) module from [November Wiki engine](https://github.com/viklund/november). Also CGI tests are added to Pheix test bundle.

## Dependencies

Check the list [dependencies](https://gitlab.com/pheix-pool/core-perl6/wikis/module-dependencies) at our wiki.

## License information

Pheix is free and opensource software, so you can redistribute it and/or modify it under the terms of the [The Artistic License 2.0](https://opensource.org/licenses/Artistic-2.0).

## Changelog

All notable changes to this project are documented [in this file](https://pheix.org/changelog.txt).

## Author

Please contact me via [LinkedIn](https://www.linkedin.com/in/knarkhov/) or [Twitter](https://twitter.com/CondemnedCell). Your feedback is welcome at [narkhov.pro](https://narkhov.pro/contact-information.html).
